sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device",
	"jquery.sap.global",
], function (JSONModel, Device,jQuery) {
	"use strict";

	return {
		createDeviceModel: function () {
			var oModel = new JSONModel(jQuery.sap.getModulePath(""));//Model para hacerlo Local
			//var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		}

	};
});