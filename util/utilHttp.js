sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"./utilResponse",
	"../constantes",
	"./util",
	"./utilPopUps"
], function (Controller, JSONModel, utilResponse, constantes, util, utilPopUps) {
	"use strict";
	return {
		getToken: function (callback) {
			$.ajax({
				url: "/JavaService/TokenSession",
				method: "GET",
				success: function (result) {
					callback(result);
				},
				error: function (xhr, status, error) {
					callback(null);
				}
			});
		},
		HandlerErrorRed: function (e, textStatus, errorThrown) {
			var codigoError = "Error: " + e.status;
			utilPopUps.onMessageErrorDialogPress(codigoError);
			sap.ui.core.BusyIndicator.hide();
		},
		HandlerErrorToken: function () {
			var codigoError = "Error al obtener el token de acceso";
			utilPopUps.onMessageErrorDialogPress(codigoError);
			sap.ui.core.BusyIndicator.hide();
		},
		getoData: function (self, tabla, oFilter, callback, urlParameters) {
			var oDataModel = self.getView().getModel(constantes.modelOdata);
			var onSuccess = function (oDataModel) {
				var results = oDataModel.results;
				callback(utilResponse.success('0000000', '', results));
			};
			var onError = function (error) {
				sap.ui.core.BusyIndicator.hide();
				callback(utilResponse.exception('0000000', 'Error al consultar los datos '));
			};
			var reqHeaders = {
				context: this, // mention the context you want
				success: onSuccess, // success call back method
				error: onError, // error call back method
				async: true // flage for async true
			};
			if (urlParameters) {
				reqHeaders.urlParameters = urlParameters;
			}
			if (oFilter.length !== 0) {
				reqHeaders.filters = [oFilter];
			}

			try {
				oDataModel.read(tabla, reqHeaders);
			} catch (ex) {}

		},
		encode: function (input) {
			var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

			function utf8_encode(string) {
				string = string.replace(/\r\n/g, "\n");
				var utftext = "";
				for (var n = 0; n < string.length; n++) {
					var c = string.charCodeAt(n);
					if (c < 128) {
						utftext += String.fromCharCode(c);
					} else if ((c > 127) && (c < 2048)) {
						utftext += String.fromCharCode((c >> 6) | 192);
						utftext += String.fromCharCode((c & 63) | 128);
					} else {
						utftext += String.fromCharCode((c >> 12) | 224);
						utftext += String.fromCharCode(((c >> 6) & 63) | 128);
						utftext += String.fromCharCode((c & 63) | 128);
					}
				}
				return utftext;
			}

			var output = "";
			var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
			var i = 0;

			input = utf8_encode(input);

			while (i < input.length) {

				chr1 = input.charCodeAt(i++);
				chr2 = input.charCodeAt(i++);
				chr3 = input.charCodeAt(i++);

				enc1 = chr1 >> 2;
				enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
				enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
				enc4 = chr3 & 63;

				if (isNaN(chr2)) {
					enc3 = enc4 = 64;
				} else if (isNaN(chr3)) {
					enc4 = 64;
				}

				output = output +
					keyStr.charAt(enc1) + keyStr.charAt(enc2) +
					keyStr.charAt(enc3) + keyStr.charAt(enc4);

			}

			return output;
		},
		tagAleatorio: function () {
			var tad1 = new Array("M=Ga", "VCaK", "OP=Q", "NNOA", "McXs", "ZZ=Q", "LJGS", "PNk=", "qswf", "ZVs2", "qaSa", "LNms", "CzSQ", "Nlsd",
				"SSd=", "HGas", "MRTG", "KhRt", "lGtR", "BvRt", "AzsF", "JkTr", "HGas", "QRXG", "KhRt", "PGtR", "Avtt ", "AzsF", "Fkxr");

			var aleat = Math.random() * tad1.length;
			aleat = Math.floor(aleat);
			return tad1[aleat];
		},

		generarIdTransaccion: function () {
			var fecha = new Date();
			var fechaIso = fecha.toISOString();
			var fechaString = fechaIso.toString().replace(/:/g, "").replace(/-/g, "").replace(".", "").replace("Z", "").replace("T", "");
			var randon = Math.floor((Math.random() * 1000000) + 1);
			var idTransaccion = fechaString + "" + randon;
			return idTransaccion;
		},

		validarRespuestaServicio: function (oAuditResponse, mensaje2) {
			if (oAuditResponse.iCode === 1) {
				utilPopUps.onMessageSuccessDialogPress(oAuditResponse.sIdTransaction, mensaje2);
			} else if (oAuditResponse.iCode === 200) {
				utilPopUps.onMessageWarningDialogPressExit(oAuditResponse.sIdTransaction, mensaje2);
			} else if (oAuditResponse.iCode > 1) {
				//var mensaje = oAuditResponse.sMessage;
				utilPopUps.onMessageWarningDialogPress2(oAuditResponse.sIdTransaction, mensaje2);
			} else if (oAuditResponse.iCode === 0) {
				utilPopUps.onMessageErrorDialogPress(oAuditResponse.sIdTransaction);
			} else {
				utilPopUps.onMessageErrorDialogPress(oAuditResponse.sIdTransaction);
			}
		},
		serviceRootpath: function () {
			var rootPath = "/SaasaService";
			//var rootPath = "https://saasan708c0830.br1.hana.ondemand.com/SaasaSeguridadDev/core";
			return rootPath;
		},
		obtenerFechaIso: function () {
			var d = new Date();
			var fechaIso = d.toISOString();
			return fechaIso.toString();
		},
		generarHeaders: function (self,token) {
			var userapi = self.getView().getModel("userapi");
			var request = {};
			request.sIdTransaccion = this.generarIdTransaccion();
			request.sAplicacion = self.getOwnerComponent().getMetadata().getComponentName();
			request.sToken = this.tagAleatorio() + this.encode(token);
			//request.sToken = this.tagAleatorio() + this.encode(userapi.getData().loginName.toLowerCase());
			return request;
		},
		Post2: function (service, oResults, success, context) {
			var self = this;
			$.ajax({
				//url: this.serviceRootpath() +service,
				url: service,
				timeout: 0,
				headers: this.generarHeaders(context),
				data: JSON.stringify(oResults),
				contentType: "application/json; charset=utf-8",
				context: context,
				method: "POST",
				async: true,
				success: function (data) {
					sap.ui.core.BusyIndicator.hide();
					return data;
					if (data.oAuditResponse.iCode === 1) {
						util.ui.messageBox("El usuario ha sido registrado", 's', function () {});
					} else {
						util.http.validarRespuestaServicio(data.oAuditResponse, data.oAuditResponse.sMessage);
					}
				},
				error: self.HandlerErrorRed,
				complete: function () {
					console.log("--Se ejecutó llamada de red--");
				}
			});
		},
		PostSync: function (service, oResults, success, context) {
			var self = this;
			$.ajax({
				url: this.serviceRootpath() + service,
				timeout: 0,
				headers: this.generarHeaders(context),
				data: JSON.stringify(oResults),
				contentType: "application/json; charset=utf-8",
				context: context,
				method: "POST",
				async: false,
				success: success,
				error: self.HandlerErrorRed,
				complete: function () {
					console.log("--Se ejecutó llamada de red--");
				}
			});
		},
		Post: function (service, oResults, success, context) {

			var self = this;
			self.getToken(function (token) {
				if (token) {
					$.ajax({
						url: self.serviceRootpath() + service,
						timeout: 0,
						headers: self.generarHeaders(context,token),
						data: JSON.stringify(oResults),
						contentType: "application/json; charset=utf-8",
						context: context,
						method: "POST",
						async: true,
						success: success,
						error: self.HandlerErrorRed,
						complete: function () {
							console.log("--Se ejecutó llamada de red--");
						}
					});
				} else {
					self.HandlerErrorToken();
				}
			});

		},
		Get: function (service, oResults, success, context) {
			$.ajax({
				url: this.serviceRootPruebaPath() + service,
				timeout: 0,
				contentType: "application/json",
				context: context,
				method: "GET",
				async: true,
				success: success,
				error: this.HandlerErrorRed,
				complete: function () {
					jQuery.sap.log.info("--Se ejecutó llamada de red--");
				}
			});
		},
		Put: function (service, oData, success, context) {
			$.ajax({
				url: this.serviceRootpath() + service,
				timeout: 0,
				headers: this.generarHeaders(context),
				data: JSON.stringify(oData),
				contentType: "application/json; charset=utf-8",
				context: context,
				method: "PUT",
				async: true,
				success: success,
				error: this.HandlerErrorRed,
				complete: function () {
					console.log("--Se ejecutó llamada de red--");
				}
			});
		},
		Delete: function (service, oData, success, context) {
			$.ajax({
				url: this.serviceRootpath() + service,
				timeout: 0,
				headers: this.generarHeaders(context),
				data: JSON.stringify(oData),
				contentType: "application/json; charset=utf-8",
				context: context,
				method: "DELETE",
				async: true,
				success: success,
				error: this.HandlerErrorRed,
				complete: function () {
					console.log("--Se ejecutó llamada de red--");
				}
			});
		},
		restGet: function (url, callback) {
			$.ajax({
				url: url,
				method: "GET",
				async: false,
				contentType: 'application/json',
				dataType: 'json',
				success: function (result) {
					return callback(result);
				},
				error: function (xhr, status, error) {
					var respuestaService = {
						c: "ex",
						m: "Error HTTP - GET",
						data: error
					};
					return callback(respuestaService);
				}
			});
		},
		restPost: function (url, data, callback) {
			$.ajax({
				url: url,
				method: "POST",
				async: false,
				contentType: 'application/json',
				dataType: 'json',
				data: JSON.stringify(data),
				success: function (result) {
					return callback(result);
				},
				error: function (xhr, status, error) {
					var respuestaService = {
						c: "ex",
						m: "Error HTTP - POST",
						data: error
					};
					return callback(respuestaService);
				}
			});
		},
		httpPost: function (path, content, callback, data) {
			$.ajax({
				url: "/DocumentService" + path,
				method: "POST",
				cache: false,
				async: true,
				data: JSON.stringify({
					content: content
				}),
				timeout: 5000000,
				contentType: "application/json charset=utf-8",
				success: function (result) {
					try {
						return callback(JSON.parse(result), data);
					} catch (e) {
						return callback(result, data);
					}
				},
				error: function (error) {
					var respuestaError = utilResponse.error("Error del sistema", error);
					return callback(respuestaError);
				}
			});
		},

		ftp: function (NroEntrega, objRequest) {
			var pathService = constantes.URLFTPService;
			var resultFTP = {};
			$.ajax({
				url: pathService,
				type: "POST",
				dataType: 'json',
				async: false,
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(objRequest),
				success: function (result) {
					var respuestaService = {
						c: "s",
						m: "",
						data: result
					};
					resultFTP = respuestaService;
				},
				error: function (xhr, resp, text) {
					var respuestaService = {
						c: "ex",
						m: "Exepcion del sistema",
						data: error
					};
					resultFTP = respuestaService;
				}
			});

			return resultFTP;
		}
	};
});