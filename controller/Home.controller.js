sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"../model/models",
	"sap/ui/core/UIComponent",
	"../util/util",
	'sap/ui/model/Filter',
	"../services/Services",
	"../constantes",
	"../viewPass",
	"../estructuras/Estructura",
	"./BaseController"
], function (Controller,models,UIComponent,util,Filter,Services,constantes,viewPass,Estructura,BaseController) {
	"use strict";
	return BaseController.extend("saasa.armadoUA.controller.Home", {
		onInit: function () {
			
		},
		onRouteMatched: function (oEvent) {
			if (oEvent.getParameter("name") === "appHome") {
				this.getView().getModel("localModel").setSizeLimit(50000);
				this.getView().getModel("localModel").refresh(true);
			}
		},
		onAfterRendering:function(){
			///////////////////////////////////////////////////////////////////////////////
			var userModel = this.getView().getModel(constantes.userApi);
			userModel.attachRequestCompleted(function() {
			}.bind(this));
			viewPass.Home = this;
			////////////////////////////////////////////////////////////////////////////////
		},
		
		
	});
});