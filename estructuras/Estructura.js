
jQuery.sap.require("sap.ui.core.format.DateFormat");
sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"../util/utilController"
], function (JSONModel, controller) {
	"use strict";
	return {
		maestro: {
			mapVGenericaCampoComboBox: function (self, datos) {
				var servDatos = datos;
				self.getView().getModel("maestroModel").setData({});
				for (var i in servDatos) {
					if(servDatos[i].CodigoTabla == "T_BALANZA"){
						var model = self.getView().getModel("maestroModel").getProperty("/" + servDatos[i].CodigoTabla);
						var modelSet = model ? model : [];
						var modDatos = {
							//Codigo: servDatos[i].CodigoSap,
							Codigo: servDatos[i].DescripcionCampo,
							Descripcion: servDatos[i].DescripcionCampo,
							CodigoSap: servDatos[i].CodigoSap,
							IdPadre: servDatos[i].IdPadre 
						};
						modelSet.push(modDatos);
						self.getView().getModel("maestroModel").setProperty("/" + servDatos[i].CodigoTabla, modelSet);
					}else{
						var model = self.getView().getModel("maestroModel").getProperty("/" + servDatos[i].CodigoTabla);
						var modelSet = model ? model : [];
						var modDatos = {
							Codigo: servDatos[i].Id,
							Descripcion: servDatos[i].DescripcionCampo,
							IdPadre: servDatos[i].IdPadre,
							CodigoSap: servDatos[i].CodigoSap,
							Campo: servDatos[i].Campo 
						};
						modelSet.push(modDatos);
						self.getView().getModel("maestroModel").setProperty("/" + servDatos[i].CodigoTabla, modelSet);
					}
						
					} 
				}
		},
		master: {
			mapoDataVItinerario: function (datos) {
				var servDatos = datos;
				var arrayDatos = [];
				for (var i in servDatos) {
					var modDatos = {
						Id: servDatos[i].Id,
						IdEstado: servDatos[i].IdEstado,
						EstadoCodigo: servDatos[i].EstadoCodigo,
						EstadoDescripcion: servDatos[i].EstadoDescripcion,
						UsuarioCreador: servDatos[i].UsuarioCreador,
						FechaCreacion: servDatos[i].FechaCreacion,
						TerminalCreacion: servDatos[i].TerminalCreacion,
						UsuarioModificador: servDatos[i].UsuarioModificador,
						FechaModificacion: servDatos[i].FechaModificacion,
						TerminalModificacion: servDatos[i].TerminalModificacion,
						CodigoTransportista: servDatos[i].CodigoTransportista,
						IdTransportista: servDatos[i].IdTransportista,
						DescripcionTransportista: servDatos[i].DescripcionTransportista,
						IdVia: servDatos[i].IdVia,
						CodVia: servDatos[i].CodVia,
						DescVia: servDatos[i].DescVia,
						ClienteSap: servDatos[i].ClienteSap,
						GHA: servDatos[i].GHA,
						CodTerminal: servDatos[i].CodTerminal,
						IdTipoTransportista: servDatos[i].IdTipoTransportista,
						CodTipoTransportista: servDatos[i].CodTipoTransportista,
						DescTipoTransportista: servDatos[i].DescTipoTransportista,
						CodSapTipoTransportista: servDatos[i].CodSapTipoTransportista,
						NroViaje: servDatos[i].NroViaje,
						AnioViaje: servDatos[i].AnioViaje,
						Matricula: servDatos[i].Matricula,
						ModeloTransporte: servDatos[i].ModeloTransporte,
						ModeloTransporteDescripcion: servDatos[i].ModeloTransporteDescripcion,
						TipoTransporte: servDatos[i].TipoTransporte,
						TipoTransporteDescripcion: servDatos[i].TipoTransporteDescripcion,
						Movimiento: servDatos[i].Movimiento,
						MovimientoDescripcion: servDatos[i].MovimientoDescripcion,
						Origen: servDatos[i].Origen,
						OrigenDescripcion: servDatos[i].OrigenDescripcion,
						Destino: servDatos[i].Destino,
						DestinoDescripcion: servDatos[i].DestinoDescripcion,
						Dias: servDatos[i].Dias,
						Hora: servDatos[i].Hora
					};
					arrayDatos.push(modDatos);
				}
				return arrayDatos;
			},
			mapoDataVManifiesto: function (datos) {
				var servDatos = datos;
				var arrayDatos = [];
				for (var i in servDatos) {
					var modDatos = {
						Id: servDatos[i].Id,
						Via: servDatos[i].Via,
						Viaje: servDatos[i].Viaje,
						TipoManifiesto: servDatos[i].TipoManifiesto,
						NumeroManifiesto: servDatos[i].NumeroManifiesto,
						AnioManifiesto: servDatos[i].AnioManifiesto,
						FechaLlegada: servDatos[i].FechaLlegada,
						FechaTerminoDescarga: controller.formatFechaDDMMAAAAHora(servDatos[i].FechaTerminoDescarga),
						CodItinerario: servDatos[i].CodItinerario,
						AnioItinerario: servDatos[i].AnioItinerario,
						IdItinerario: servDatos[i].IdItinerario,
						IdItinerarioDetalle: servDatos[i].IdItinerarioDetalle,
						FechaHoraLlegada: controller.formatFechaDDMMAAAAHora(servDatos[i].FechaHoraLlegada),
						Movimiento: servDatos[i].Movimiento,
						Contenedores: servDatos[i].Contenedores,
						DocumentosTransporte: servDatos[i].DocumentosTransporte,
						Paquetes: servDatos[i].Paquetes,
						Precintos: servDatos[i].Precintos,
						Adjuntos: servDatos[i].Adjuntos,
						IdTransportista: servDatos[i].IdTransportista,
						DescripcionTransportista: servDatos[i].DescripcionTransportista,
						IdVia: servDatos[i].IdVia,
						CodVia: servDatos[i].CodVia,
						DescVia: servDatos[i].DescVia,
						ClienteSap: servDatos[i].ClienteSap,
						GHA: servDatos[i].GHA,
						CodTerminal: servDatos[i].CodTerminal,
						IdTipoTransportista: servDatos[i].IdTipoTransportista,
						CodTipoTransportista: servDatos[i].CodTipoTransportista,
						DescTipoTransportista: servDatos[i].DescTipoTransportista,
						CodSapTipoTransportista: servDatos[i].CodSapTipoTransportista,
						NroViaje: servDatos[i].NroViaje,
						AnioViaje: servDatos[i].AnioViaje,
						Matricula: servDatos[i].Matricula,
						ModeloTransporte: servDatos[i].ModeloTransporte,
						ModeloTransporteDescripcion: servDatos[i].ModeloTransporteDescripcion,
						TipoTransporte: servDatos[i].TipoTransporte,
						TipoTransporteDescripcion: servDatos[i].TipoTransporteDescripcion,
						MovimientoItinerario: servDatos[i].MovimientoItinerario,
						MovimientoDescripcion: servDatos[i].MovimientoDescripcion,
						MovimientoDescripcionItinerario: servDatos[i].MovimientoDescripcionItinerario,
						Origen: servDatos[i].Origen,
						OrigenDescripcion: servDatos[i].OrigenDescripcion,
						Destino: servDatos[i].Destino,
						DestinoDescripcion: servDatos[i].DestinoDescripcion,
						Dias: servDatos[i].Dias,
						Hora: servDatos[i].Hora,
						DescTipoManifiesto: servDatos[i].DescTipoManifiesto,
						Estacionamiento: servDatos[i].Estacionamiento
					};
					arrayDatos.push(modDatos);
				}
				return arrayDatos;
			},
		},
		detail: {
			mapoDataDocumentosTransporte: function (self, datos) {
				var servDatos = datos;
				var arrayDatos = [];
				var totalBultoManifestado = 0;
				var totalPesoManifestado = 0;
				for (var i in servDatos) {
					totalBultoManifestado = totalBultoManifestado + parseInt(servDatos[i].BultosManifestados);
					totalPesoManifestado = totalPesoManifestado + parseInt(servDatos[i].PesoManifestado);
					var modDatos = {
						Id: servDatos[i].Id,
						/*EsMaster: servDatos[i].EsMaster,
						DocumentoTransporteMadreId: servDatos[i].DocumentoTransporteMadreId,
						DocumentoTransporteCodigo: servDatos[i].DocumentoTransporteCodigo,
						Descripcion: servDatos[i].Descripcion,
						BultosManifestados: servDatos[i].BultosManifestados,
						PesoManifestado: servDatos[i].PesoManifestado,
						Origen: servDatos[i].Origen,
						Destino: servDatos[i].Destino,
						PuntoLlegada: servDatos[i].PuntoLlegada,
						TerminalOrigen: servDatos[i].TerminalOrigen,
						TerminalDestino: servDatos[i].TerminalDestino,
						IdModalidad: servDatos[i].IdModalidad,
						IdTipoCarga: servDatos[i].IdTipoCarga,
						IdTipoAlmacenamiento: servDatos[i].IdTipoAlmacenamiento,
						IdRegimen: servDatos[i].IdRegimen,
						AgenteCarga: servDatos[i].AgenteCarga,
						Embarcador: servDatos[i].Embarcador,
						Consignatario: servDatos[i].Consignatario,
						IdManifiesto: servDatos[i].IdManifiesto,
						IdContenedor: servDatos[i].IdContenedor,
						EsCargaSuelta: servDatos[i].EsCargaSuelta,
						EsExpo: servDatos[i].EsExpo,
						Adjunto: servDatos[i].Adjunto,
						EsTCI: servDatos[i].EsTCI,*/
						EsMaster: servDatos[i].EsMaster,
						EsMasterDescripcion: servDatos[i].EsMasterDescripcion,
						DocumentoTransporteMadreId: servDatos[i].DocumentoTransporteMadreId,
						DocumentoTransporteMasterCodigo: servDatos[i].DocumentoTransporteMasterCodigo,
						DocumentoTransporteCodigo: servDatos[i].DocumentoTransporteCodigo,
						Descripcion: servDatos[i].Descripcion,
						BultosManifestados: servDatos[i].BultosManifestados,
						PesoManifestado: servDatos[i].PesoManifestado,
						BultosRecibidos: servDatos[i].BultosRecibidos,
						PesoRecibido: servDatos[i].PesoRecibido,
						BultosFaltantes: servDatos[i].BultosFaltantes,
						PesoFaltante: servDatos[i].PesoFaltante,
						BultosSobrantes: servDatos[i].BultosSobrantes,
						PesoSobrante: servDatos[i].PesoSobrante,
						BultosMalEstado: servDatos[i].BultosMalEstado,
						PesoMalEstado: servDatos[i].PesoMalEstado,
						Origen: servDatos[i].Origen,
						Destino: servDatos[i].Destino,
						OrigenDescripcion: servDatos[i].OrigenDescripcion,
						DestinoDescripcion: servDatos[i].DestinoDescripcion,
						PuntoLlegada: servDatos[i].PuntoLlegada,
						PuntoLlegadaDescripcion: servDatos[i].PuntoLlegadaDescripcion,
						TerminalOrigen: servDatos[i].TerminalOrigen,
						TerminalOrigenDescripcion: servDatos[i].TerminalOrigenDescripcion,
						TerminalDestino: servDatos[i].TerminalDestino,
						TerminalDestinoDescripcion: servDatos[i].TerminalDestinoDescripcion,
						IdModalidad: servDatos[i].IdModalidad,
						ModalidadDescripcion: servDatos[i].ModalidadDescripcion,
						IdTipoCarga: servDatos[i].IdTipoCarga,
						TipoCargaDescripcion: servDatos[i].TipoCargaDescripcion,
						IdTipoAlmacenamiento: servDatos[i].IdTipoAlmacenamiento,
						TipoAlmacenamientoDescripcion: servDatos[i].TipoAlmacenamientoDescripcion,
						IdRegimen: servDatos[i].IdRegimen,
						RegimenAduanaCodigo: servDatos[i].RegimenAduanaCodigo,
						RegimenAduanaCodigoSAP: servDatos[i].RegimenAduanaCodigoSAP,
						RegimenAduanaDescripcion: servDatos[i].RegimenAduanaDescripcion,
						AgenteCarga: servDatos[i].AgenteCarga,
						Embarcador: servDatos[i].Embarcador,
						Consignatario: servDatos[i].Consignatario,
						IdManifiesto: servDatos[i].IdManifiesto,
						IdContenedor: servDatos[i].IdContenedor,
						EsCargaSuelta: servDatos[i].EsCargaSuelta,
						EsExpo: servDatos[i].EsExpo,
						Adjunto: servDatos[i].Adjunto,
						CodVolante: servDatos[i].CodVolante,
						enabledInput: false,
						icon: "sap-icon://edit"
					};
					arrayDatos.push(modDatos);
				}
				self.getView().getModel("localModel").setProperty("/Detail/TotalBultoManifestado", totalBultoManifestado);
				self.getView().getModel("localModel").setProperty("/Detail/TotalPesoManifestado", totalPesoManifestado);
				return arrayDatos;
			}
		},
		detailPDF:{
			mapExportPDF:function(detail,obj){
				var datosPdf=  {
					Titulo:"Volante"+obj.CodVolante,
					NumeroVuelo: detail.NroViaje,
					Transportista: "Falta",
					DocumentoTransporteCodigo: obj.DocumentoTransporteCodigo,
					ProcOrigen: obj.OrigenDescripcion,
					Embarcador: obj.Embarcador,
					Consignatario: obj.Consignatario,
					Contenido: "Falta",
					Ubicacion: "Falta",
					NumeroManifiesto: detail.NumeroManifiesto,
					FechaHoraLlegada: detail.FechaHoraLlegada,
					FechaTerminoDescarga: detail.FechaTerminoDescarga,
					aDetalle: [
			            {
			              "Manifestado":controller.leftNumberUndefined(obj.BultosManifestados),
			              "Recibidos":controller.leftNumberUndefined(obj.BultosRecibidos),	
			              "Faltantes": controller.leftNumberUndefined(obj.BultosFaltantes),
			              "Sobrantes": controller.leftNumberUndefined(obj.BultosSobrantes),
			              "Buenos": controller.leftNumberUndefined(0),//obj.BultosRecibidos-obj.BultosMalEstado,
			              "Malos": controller.leftNumberUndefined(obj.BultosMalEstado)
			            },
			            {
			              "Manifestado": controller.leftNumberUndefined(obj.PesoManifestados),
			              "Recibidos": controller.leftNumberUndefined(obj.PesoRecibidos),	
			              "Faltantes": controller.leftNumberUndefined(obj.PesoFaltantes),
			              "Sobrantes": controller.leftNumberUndefined(obj.PesoSobrantes),
			              "Buenos": controller.leftNumberUndefined(0),//obj.PesoRecibidos-obj.BultosMalEstado,
			              "Malos": controller.leftNumberUndefined(obj.PesoMalEstado)
			            }
			          ]
	    		};
	    		return datosPdf;
			},
			mapExportEtiquetaPDF:function(obj){
				var datosPdf=  {
					CodigoUA: controller.valueUndefinedPDF(obj.CodigoUA),
					DocumentoTransporte: controller.valueUndefinedPDF(obj.DocumentoTransporteCodigo),
					DocumentoTransporteMaster: obj.DocumentoTransporteMasterCodigo ? obj.DocumentoTransporteMasterCodigo : obj.DocumentoTransporteCodigo,
					Bultos: controller.leftNumberUndefined(obj.Bultos),
					Peso: controller.leftNumberUndefined(obj.Peso),
					TipoAlmacenamientoDescripcion: controller.valueUndefinedPDF(obj.TipoAlmacenamientoDescripcion),
					Almacen: controller.valueUndefinedPDF(obj.UbicacionDescAlmacen),
					Transportista: controller.valueUndefinedPDF(obj.DescripcionTransportista),
					TipoCargaDescripcion: controller.valueUndefinedPDF(obj.TipoCargaDescripcion),
					FechaIngreso: controller.valueUndefinedPDF(obj.FechaIngreso)
	    		};
	    		return datosPdf;
			}
		},
		detailExcel: {
			mapTbImportExcel: function (datos) {
				var servDatos = datos;
				var arrayDatos = [];
				for (var i in servDatos) {
					var modDatos = {
						masterDirecta: servDatos[i]["Master / Directa"],
						GABL: servDatos[i]["GA / BL"],
						descripcion: servDatos[i]["Descripcion"],
						bultosManifestado: servDatos[i]["Bultos Manif."],
						pesoManifestado: servDatos[i]["Peso Manif."],
						origen: servDatos[i]["Origen"],
						destino: servDatos[i]["Destino"],
						pLlegada: servDatos[i]["P. Llegada"],
						termOrigen: servDatos[i]["Term. Origen"],
						termDestino: servDatos[i]["Term. Destino"],
						modalidad: servDatos[i]["Modalidad"],
						clasificacionCarga: servDatos[i]["Clasf. Carga"],
						regimen: servDatos[i]["Régimen"],
						agenteCarga: servDatos[i]["Agente de Carga"],
						embarcador: servDatos[i]["Embarcador"],
						consignatario: servDatos[i]["Consignatario"],
						enabledInput: false,
						icon: "sap-icon://edit"
					};
					arrayDatos.push(modDatos);
				}
				return arrayDatos;
			},
			mapTbExportExcel: function (datos) {
				var servDatos = datos;
				var arrayDatos = [];
				for (var i in servDatos) {
					var modDatos = {
						["Matrícula"]: servDatos[i]["matricula"],
						["Tipo Avión"]: servDatos[i]["tipoAvion"],
						["Cap. Pasajeros"]: servDatos[i]["capPasajeros"],
						["Cant. Cabinas"]: servDatos[i]["cantCabina"],
						["Peso Máx. Despegue"]: servDatos[i]["pesoMaxDespegue"],
					};
					arrayDatos.push(modDatos);
				}
				return arrayDatos;
			}
		}
	};
});

