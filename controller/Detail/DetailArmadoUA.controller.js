sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/UIComponent",
	"jquery.sap.global",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"sap/m/UploadCollectionParameter",
	"../../estructuras/Estructura",
	"../../model/mockdata",
	'sap/ui/model/Filter',
	"sap/ui/model/FilterOperator",
	"../../util/util",
	"../../viewPass",
	"../../services/Services",
	"../BaseController"
], function (Controller, UIComponent, jQuery, MessageBox, MessageToast, UploadCollectionParameter, Estructura, mockdata, Filter,
	FilterOperator, util,
	viewPass,
	Services, BaseController) {
	"use strict";
	return BaseController.extend("saasa.armadoUA.controller.Detail.DetailArmadoUA", {
		onInit: function () {
			var oRouter = UIComponent.getRouterFor(this);
			oRouter.attachRoutePatternMatched(this.onRouteMatched, this);
			//util.controller.navTo(this, "appDetail", null);
		},
		onRouteMatched: function (oEvent) {
			if (oEvent.getParameter("name") === "appDetailArmadoUA") {
				this.getView().getModel("modelOdata").setSizeLimit(90000);
				this.getView().getModel("modelOdata").refresh(true);
				this.getView().getModel("localModel").setSizeLimit(50000);
				this.getView().getModel("localModel").refresh(true);
				var self = this;
				var objList = self.getView().getModel("detailManifiesto").getData();
				self.getDocumentosTransporteManifiestos(objList);
				self.getView().byId("idUA").setVisible(true);
				self.getView().byId("idListaUA").setVisible(true);
				self.getView().byId("idResumen").setVisible(true);
				//self.getView().byId("idActaInv").setVisible(false);
				this.getView().byId("idBulto").setEnabled(false);
				self.getView().byId("fPesoParihuela").setValue(0);
				self.filterDocumentoTransporte(self, "idListDocumentoTransporte", objList);
				//self.filterDocumentoTransporte(self, "tb_tablaResumen", objList);
				self.filterCombo(self, "idListUAArmada", "IdManifiesto", objList.Id);
				self.getView().getModel("localModel").setProperty("/DocumentoTransporte", []);
				self.getView().getModel("miData").setData({
					EsEdicion: false,
					habilitarFotos: false
				});
				self.getView().byId("idListDocumentoTransporte").setEnabled(true);
				self.getView().getModel("miData").refresh(true);
				self.onUA();
				self.validationComboBox();
				self.getView().byId("inputFoto1").setFieldGroupIds("");
				self.getView().byId("inputFoto2").setFieldGroupIds("");
				self.getView().byId("inputFoto3").setFieldGroupIds("");
				self.getView().byId("inputFoto4").setFieldGroupIds("");
				//self.getUsuario(self);
			}
		},
		onAfterRendering: function () {
		},
		validationComboBox: function () {
			this.changeSede();
			this.changeUbicacion();
		},
		onLoadGuiasManifiestos: function (IdManifiesto) {

			//Services.getOdataDocTransporteManifiestos
		},
		onRefresh: function () {
			this.getView().getModel("modelOdata").refresh(true);
		},
		onBuscarManifiesto: function () {
			//var selectKey = this.getView().getModel("localModel").getProperty("/BuscarManifiesto/selectCombo");
			var filter = [{
				key: "NumeroManifiesto",
				query: "Contains"
			}, {
				key: "NroViaje",
				query: "Contains"
			}];
			util.ui.onFiltrarTablaLive(this, "searchBuscar", "idListMaster", filter);
		},
		onUA: function () {
			this.getView().byId("idSegmentButton").setSelectedKey("UA");
			this.getView().byId("idToggleEditMaster").setVisible(false);
			var navCon = this.getView().byId("navDocumentoTransporte");
			navCon.to(this.getView().byId("pgDocTransporte"), "slide");
		},
		backMaster: function () {
			this.onRefresh();
			util.controller.navTo(this, "appDetail", null);
			this.onUA();
			/*if (this.getView().byId("idActaInv").getVisible()) {
				this.getView().byId("idUA").setVisible(true);
				this.getView().byId("idListaUA").setVisible(true);
				this.getView().byId("idResumen").setVisible(true);
				this.getView().byId("idActaInv").setVisible(false);
				var navCon = this.getView().byId("navDocumentoTransporte");
				navCon.to(this.getView().byId("pgResumen"), "slide");
			} else {
				this.onRefresh();
				util.controller.navTo(this, "appDetail", null);
				this.onUA();
			}*/
		},
		onListaUAsArmada: function () {
			var aFilters = [];
			var iIdManifiesto = this.getView().getModel("detailManifiesto").getProperty("/Id");
			if (iIdManifiesto) {
				var filtroiIdManifiesto = new Filter("IdManifiesto", FilterOperator.EQ, iIdManifiesto);
				var FilteriIdManifiesto = new Filter([filtroiIdManifiesto], false);
				aFilters.push(FilteriIdManifiesto);
			}
			// update list binding
			var list = this.getView().byId("idListUAArmada");
			var binding = list.getBinding("items");
			binding.filter(aFilters, "Application");
			this.getView().byId("idToggleEditMaster").setVisible(true);
			var navCon = this.getView().byId("navDocumentoTransporte");
			navCon.to(this.getView().byId("pgListaUAsArmada"), "slide");
		},
		onResumen: function () {
			this.getView().byId("idToggleEditMaster").setVisible(false);
			var navCon = this.getView().byId("navDocumentoTransporte");
			navCon.to(this.getView().byId("pgResumen"), "slide");
			//this.getNumeroIncidencia();
		},
		getNumeroIncidencia: function () {
			var self = this;
			var numeroIncidencia = 0;
			var documentosTransportes = this.getView().getModel("localModel").getProperty("/DocumentoTransporteResumen");
			for (var i = 0; documentosTransportes.length > i; i++) {
				if (documentosTransportes[i].BultosMalEstado > 0) {
					self.getView().getModel("localModel").setProperty("/DocumentoTransporteResumen/" + i + "/Visible", true);
				} else {
					self.getView().getModel("localModel").setProperty("/DocumentoTransporteResumen/" + i + "/Visible", false);
				}
			}
		},
		/*onActaInventario: function () {
			this.getView().byId("idToggleEditMaster").setVisible(false);
			var navCon = this.getView().byId("navDocumentoTransporte");
			navCon.to(this.getView().byId("pgActInventario"), "slide");
		},*/
		selectionDocTransporte: function (oEvent, obj) {
			/*var items = oEvent.getSource().getSelectedItems();
			var itemsArray = [];
			for (var i in items) {
				var obj = items[i].getBindingContext("modelOdata").getObject();
				itemsArray.push(obj);
			}
			this.getView().getModel("localModel").setProperty("/DocumentoTransporte", itemsArray);*/
			var itemsArray = [];
			var item = this.getView().byId("idListDocumentoTransporte").getSelectedItem().getBindingContext("modelOdata").getObject();
			if (obj) {
				item.BultoArmado = obj.Bultos;
				this.getView().byId("fPesoParihuela").setValue(obj.PesoBruto - obj.PesoNeto);
			}

			itemsArray.push(item);
			this.getView().getModel("localModel").setProperty("/DocumentoTransporte", itemsArray);
			this.getView().byId("idBulto").setEnabled(true);
			this.getSector(this);
		},
		onChangeSede: function (oEvent) {
			var key = oEvent.getSource().getSelectedKey();
			"IdPadre", key
			this.filter2Combo(this, "idComboAlmacen", [{
				campoGet: "IdPadre",
				campoSet: key
			}, {
				campoGet: "CodigoSap",
				campoSet: 'IMPO'
			}]);
			var oFilter = [];
			oFilter.push(new Filter("IdSede", "EQ", key));
			this.getUbicacionInicial(this, oFilter);
			//this.filterCombo(this, "idComboAlmacen", "CodigoSap", 'IMPO');
			this.getView().byId("idComboAlmacen").setEnabled(true);
			this.filterCombo(this, "idComboBalanza", "IdPadre", key);
			this.getView().byId("idComboBalanza").setEnabled(true);
		},
		onCalcularPeso: function () {
			var self = this;
			var ipBalanza = this.getView().getModel("miData").getProperty("/Balanza");
			var oResults = util.envioDatos.calcularPeso(ipBalanza);
			sap.ui.core.BusyIndicator.show(0);
			Services.calcularPeso(self, oResults, function (iResults) {
				sap.ui.core.BusyIndicator.hide();
				iResults.oAuditResponse.oResults = util.controller.unlink(iResults.oAuditResponse);
				util.response.validate(iResults.oAuditResponse, {
					success: function (data, message) {
						self.getView().getModel("miData").setProperty("/Peso", iResults.oResults);
						//util.ui.onMensajeGeneral(self, "Se actualizó correctamente la Guía.");
					},
					/*error: function (message) {
						util.ui.onMessageErrorDialogPress(message);
					}*/
				});
			});
		},
		onChangeUbicacion: function () {
			this.onGenerateCodigoUA();
		},
		onGenerateCodigoUA: function () {
			var codigo = this.getView().byId("cbUbicacionArmado").getValue();
			var codigoTipoAlm = this.getView().byId("idTipoAlmArmado").getSelectedKey();
			codigoTipoAlm = util.controller.formatterComboBox(this, "Codigo", codigoTipoAlm, "Campo", "maestroModel", "/T_TIPO_ALMACEN");
			//var codigo = this.getView().byId("cbTipoCarga").getValue();
			this.getView().getModel("miData").setProperty("/CodigoUA", codigo + "-" + codigoTipoAlm);
		},
		onArmarUA: function () {
			var self = this;
			var guias = self.getView().getModel("localModel").getProperty("/DocumentoTransporte");
			var valid = util.validation.validatorSAPUI5(self, "fieldValidViewgArmadoUA");
			if (!valid) {
				return sap.m.MessageToast.show("LLene correctamente los campos.", {
					duration: 3000
				});
			}
			var validPeso = self.onValidarPeso();
			if (validPeso === true) {
				return sap.m.MessageToast.show("El peso parihuela debe ser menor al peso bruto.", {
					duration: 3000
				});
			}
			var miData = self.getView().getModel("miData");
			var Sap = self.getView().getModel("Sap");

			miData.sSede = Sap.getProperty("/iIdCentro");
			miData.sSedeDesc = Sap.getProperty("/ZdescSede");
			miData.sOrganizacionVenta = Sap.getProperty("/iIdOrganizacionVenta");
			miData.sOrganizacionVentaDesc = Sap.getProperty("/ZdescOvta");
			miData.sCanal = Sap.getProperty("/iIdCanal");
			miData.sCanalDesc = Sap.getProperty("/ZdescCanal");
			miData.sSector = Sap.getProperty("/iIdSector");
			miData.sSectorDesc = Sap.getProperty("/ZdescSector");
			miData.sConceptoFacturable = miData.getProperty("/IidConcepto");
			miData.sConceptoFacturableDesc = self.getView().byId("idConceptoFaacturable").getSelectedItem().getProperty("text");
			miData.iIdUbicacionInicial = miData.getProperty("/iIdUbicacionInicial");
			miData.UbicacionId = miData.getProperty("/UbicacionId");
			miData.EsMalEstado = miData.getProperty("/EsMalEstado");
			miData.TieneEmbalaje = miData.getProperty("/TieneEmbalaje");
			miData.CodigoUA = miData.getProperty("/CodigoUA");
			miData.Peso = miData.getProperty("/Peso");;
			miData.TipoCargaId = 0;
			miData.TipoAlmacenamientoId = miData.getProperty("/TipoAlmacenamientoId");
			miData.PesoParhiuela = miData.getProperty("/PesoParhiuela");
			miData.PesoNeto = miData.getProperty("/PesoNeto");
			miData.sPrdha = Sap.getProperty("/sPrdha");
			miData.sPrdhaDesc = Sap.getProperty("/sPrdhaDesc");
			
			miData.IdFoto1 = null;
			miData.IdFoto2 = null;
			miData.IdFoto3 = null;
			miData.IdFoto4 = null;
			
			var foto1 = miData.getProperty("/foto1");
			var foto2 = miData.getProperty("/foto2");
			var foto3 = miData.getProperty("/foto3");
			var foto4 = miData.getProperty("/foto4");
			
			if (foto1) {
				miData.IdFoto1 = foto1.Id;
			}
			if (foto2) {
				miData.IdFoto2 = foto2.Id;
			}
			if (foto3) {
				miData.IdFoto3 = foto3.Id;
			}
			if (foto4) {
				miData.IdFoto4 = foto4.Id;
			}

			var oResults = util.envioDatos.armarUA(miData, guias);
			sap.ui.core.BusyIndicator.show(0);
			Services.armarUA(self, oResults, function (iResults) {
				sap.ui.core.BusyIndicator.hide();
				iResults.oAuditResponse.oResults = util.controller.unlink(iResults.oAuditResponse);
				util.response.validate(iResults.oAuditResponse, {
					success: function (data, message) {
						if (oResults.EsEdicion) {
							util.ui.onMensajeGeneral(self, "La Unidad de Almacenamiento se modificó correctamente.");
						} else {
							util.ui.onMensajeGeneral(self, "La Unidad de Almacenamiento se registró correctamente.");
						}

						self.getView().getModel("miData").setData({
							EsEdicion: false,
							habilitarFotos: false
						});
						self.getView().getModel("modelOdata").refresh(true);
						self.getView().getModel("localModel").setProperty("/DocumentoTransporte", []);
						self.getView().byId("idBulto").setEnabled(false)
						self.getView().byId("fPesoParihuela").setValue(0);
						self.getView().byId("idListDocumentoTransporte").setEnabled(true);
						self.getView().byId("cbUbicacionArmado").setEnabled(false);
						//self.getView().byId("btnArmarUA").setText("Armar UA");
						self.getView().byId("inputFoto1").setFieldGroupIds("");
						self.getView().byId("inputFoto2").setFieldGroupIds("");
						self.getView().byId("inputFoto3").setFieldGroupIds("");
						self.getView().byId("inputFoto4").setFieldGroupIds("");
						self.validBotonEditarUA(false);
						var objList = self.getView().getModel("detailManifiesto").getData();
						self.getDocumentosTransporteManifiestos(objList);
					},
				});
			});

		},
		onCerraronCerrarDlg_dialogMotivoMovilizacion: function () {
			var miData = this.getView().getModel("miData");
			miData.setProperty("/iIdCentro", "");
			miData.setProperty("/iIdOrganizacionVenta", "");
			miData.setProperty("/iIdCanal", "");
			miData.setProperty("/iIdSector", "");
			miData.setProperty("/iIdConcepto", "");
			miData.setProperty("/Canales", []);
			miData.setProperty("/Sector", []);
			miData.setProperty("/ConceptoFacturable", []);
			miData.setProperty("/Centros", []);
			miData.setProperty("/OrganizacionVenta", []);
		},
		onValidarPeso: function () {
			//var balanza = comboBalanza.getSelectedItem().getProperty("text");
			var pesoParhiuela = this.getView().getModel("miData").getProperty("/PesoParhiuela");
			var pesoBruto = this.getView().getModel("miData").getProperty("/Peso");
			var validacion = false
			if (pesoParhiuela > pesoBruto) {
				validacion = true;
			}

			return validacion;
		},
		onRegistrarResumen: function (oEvent) {
			var self = this;
			var manifiesto = self.getView().getModel("detailManifiesto").getData();
			var guias = self.getView().getModel("localModel").getProperty("/DocumentoTransporteResumen");
			var oResults = util.envioDatos.registrarResumen(manifiesto, guias);
			var mensaje = "¿Desea registrar el resumen?";
			//var validacion = self.validarCamposGuias(oResults.oResults.aGuias);
			var valid = util.validation.validatorSAPUI5(self, "fieldValidComentario");
			if (!valid) {
				return sap.m.MessageToast.show("LLene correctamente los campos.", {
					duration: 3000
				});
			};
			/*var nBultosMalEstado = oResults.oResults.aGuias.filter(d => d.iBultosMalEstado > 0)
			if (nBultosMalEstado.length > 0) {
				self.getView().byId("idUA").setVisible(false);
				self.getView().byId("idListaUA").setVisible(false);
				self.getView().byId("idResumen").setVisible(false);
				self.getView().byId("idActaInv").setVisible(true);
				Services.getoDataVNumeroIncidencia(self, [], function (result) {
					util.response.validate(result, {
						success: function (data, message) {
							var anioManifiesto = self.getView().getModel("detailManifiesto").getProperty("/AnioManifiesto");
							var correlativo = util.ui.pad(data[0].NumeroIncidencia + 1, 6);
							self.getView().getModel("localModel").setProperty("/Correlativo", data[0].NumeroIncidencia + 1);
							self.getView().getModel("localModel").setProperty("/sNumeroIncidencia", anioManifiesto + correlativo);
						},
						error: function (message) {}
					});
				});
				self.onActaInventario();*/
			//} else {
			util.ui.messageBox(mensaje, 'c', function (bConfirmacion) {
				if (bConfirmacion) {
					sap.ui.core.BusyIndicator.show(0);
					Services.registrarResumen(self, oResults, function (iResults) {
						sap.ui.core.BusyIndicator.hide();
						iResults.oAuditResponse.oResults = util.controller.unlink(iResults.oAuditResponse);
						util.response.validate(iResults.oAuditResponse, {
							success: function (data, message) {
								util.ui.onMensajeGeneralPress(self, "Se registró correctamente.");
								self.getView().getModel("modelOdata").refresh(true);
							},
						});
					});
				}

			});
			//}

		},
		validarCamposGuias: function (guias) {
			var nGuias = guias.length;
			var camposllenos = guias.filter(d => (d.fPesoBuenEstado) && (d.fPesoFaltante) && (d.fPesoMalEstado) && (d.fPesoRecibido) && (d.fPesoSobrante) &&
				(d.iBultosBuenEstado) && (d.iBultosFaltantes) && (d.iBultosMalEstado) &&
				(d.iBultosRecibidos) && (d.iBultosSobrantes));
			var validacion = false;
			if (camposllenos.length === nGuias) {} else {
				var validacion = true;
			}
			return validacion;
		},
		onFinalizar: function () {
			var self = this;
			var manifiesto = self.getView().getModel("detailManifiesto").getData();
			manifiesto.sNumeroIncidencia = self.getView().getModel("localModel").getProperty("/Correlativo");
			manifiesto.sComentario = self.getView().getModel("localModel").getProperty("/sComentario");
			var guias = self.getView().getModel("localModel").getProperty("/DocumentoTransporteResumen");
			var oResults = util.envioDatos.finalizar(manifiesto, guias);
			var mensaje = "¿Desea finalizar la UA?";
			var validacion = self.validarCamposFinalizar();
			var mensajeError = 'Deben llenarse todos los campos';
			if (validacion === true) {
				util.ui.onMensajeAdvertenciaPress(self, mensajeError);
			} else {

				util.ui.messageBox(mensaje, 'c', function (bConfirmacion) {
					if (bConfirmacion) {
						sap.ui.core.BusyIndicator.show(0);
						Services.registrarResumen(self, oResults, function (iResults) {
							sap.ui.core.BusyIndicator.hide();
							iResults.oAuditResponse.oResults = util.controller.unlink(iResults.oAuditResponse);
							util.response.validate(iResults.oAuditResponse, {
								success: function (data, message) {
									util.ui.onMensajeGeneralPress(self, "Se registró correctamente.");
									self.getView().getModel("localModel").setProperty("/sNumeroIncidencia", "");
									self.getView().getModel("localModel").setProperty("/sComentario", "");
									self.getView().getModel("modelOdata").refresh(true);
								},
							});
						});
					}

				});
			}

		},
		validarCamposFinalizar: function () {
			var sNumeroIncidencia = this.getView().byId("sNumeroIncidencia");
			var sComentario = this.getView().byId("sComentario");
			var numeroIncidencia = sNumeroIncidencia.getValue();
			var comentario = sComentario.getValue();
			var validacion = false;
			if (!numeroIncidencia) {
				sNumeroIncidencia.setValueState("Error");
				sNumeroIncidencia.setValueStateText("Campo obligatorio");
				validacion = true;
			} else {
				sNumeroIncidencia.setValueState("None");
				sNumeroIncidencia.setValueStateText("");
			}

			if (!comentario) {
				sComentario.setValueState("Error");
				sComentario.setValueStateText("Campo obligatorio");
				validacion = true;
			} else {
				sComentario.setValueState("None");
				sComentario.setValueStateText("");
			}
			return validacion;
		},
		onCancelarFinalizar: function () {
			this.getView().byId("idUA").setVisible(true);
			this.getView().byId("idListaUA").setVisible(true);
			this.getView().byId("idResumen").setVisible(true);
			this.getView().byId("idActaInv").setVisible(false);
			var navCon = this.getView().byId("navDocumentoTransporte");
			navCon.to(this.getView().byId("pgResumen"), "slide");
		},
		changeSede: function () {
			//this.getView().byId("idComboAlmacen").setEnabled(false);
			//this.getView().byId("idComboBalanza").setEnabled(false);
		},
		changeUbicacion: function (addItemUbicacion) {
			var combTipoAlmacen = this.getView().byId("idTipoAlmArmado").getSelectedKey();
			var combAlmacen = this.getView().byId("idComboAlmacen").getSelectedKey();
			var comboUbicacionView = this.getView().byId("cbUbicacionArmado");
			if (combAlmacen !== "" && combTipoAlmacen !== "") {
				combTipoAlmacen = util.controller.formatterComboBox(this, "Codigo", combTipoAlmacen, "Campo", "maestroModel", "/T_TIPO_ALMACEN");
				combAlmacen = util.controller.formatterComboBox(this, "Id", combAlmacen, "Campo", "localModel", "/T_ALMACEN");
				this.filterComboUbicaciones(this, "cbUbicacionArmado", "CodAlmacen", combAlmacen, "CodTipoCarga", combTipoAlmacen,
					addItemUbicacion);
				comboUbicacionView.setEnabled(true);
			} else {
				comboUbicacionView.setEnabled(false);
			}
		},
		onChangeTipoAlmacen: function (addItemUbicacion) {
			this.changeUbicacion(addItemUbicacion);
			var objTipoAlmacen = this.getView().byId("idTipoAlmArmado").getSelectedItem().getBindingContext('maestroModel').getObject();
			this.getView().getModel("Sap").setProperty("/sPrdha", objTipoAlmacen.CodigoSap);
			this.getView().getModel("Sap").setProperty("/sPrdhaDesc", objTipoAlmacen.Descripcion);
			this.onConsultarConceptosFacturables();
		},
		onChangeAlmacen: function () {
			this.changeUbicacion();
		},
		editMaster: function (oEvent, pressed) {
			var idList = "idListUAArmada";
			var Pressed;
			if (pressed !== false) {
				Pressed = oEvent.getSource().getPressed();
			} else {
				this.getView().byId("idToggleEditMaster").setPressed(false);
				Pressed = pressed;
			}
			var mode = "";
			var type = "";
			if (Pressed) {
				mode = "Delete";
				//type = "DetailAndActive";
			} else {
				mode = "SingleSelectMaster";
				//type = "Navigation";
			}
			/*this.getView().byId(idList).getItems().forEach(function (item) {
				item.setType(type);
			});*/
			this.getView().byId(idList).setMode(mode);
		},
		deleteList: function (oEvent) {
			var self = this;
			var obj = util.controller.unlink(oEvent.getParameter("listItem").getBindingContext("modelOdata").getObject());;
			var oResults = util.envioDatos.eliminarUA(obj);
			sap.ui.core.BusyIndicator.show(0);
			Services.eliminarUA(self, oResults, function (iResults) {
				sap.ui.core.BusyIndicator.hide();
				iResults.oAuditResponse.oResults = util.controller.unlink(iResults.oAuditResponse);
				util.response.validate(iResults.oAuditResponse, {
					success: function (data, message) {
						self.getView().getModel("modelOdata").refresh(true);
						var objList = self.getView().getModel("detailManifiesto").getData();
						self.getDocumentosTransporteManifiestos(objList);
						//util.ui.onMensajeGeneral(self, "Se actualizó correctamente la Guía.");
					},
					error: function (message) {
						util.ui.onMessageErrorDialogPress(message);
					}
				});
			});
		},
		onListDetailArmadoUA: function (oEvent) {
			var self = this;
			var obj = util.controller.unlink(oEvent.getParameter("listItem").getBindingContext("modelOdata").getObject());
			self.getView().getModel("localModel").setProperty("/UADetalle", obj);
			util.controller.createDialog(self, "dlg_dialogDetalleUA", "view.Frag.Dialogs.dlg_dialogDetalleUA");
			if (obj.EstadoCodigo === "TE_UA_04"){
				self.getView().getModel("localModel").setProperty("/UADetalle/Despachado",false);
			}else{
				self.getView().getModel("localModel").setProperty("/UADetalle/Despachado",true);
			}
			////////////////////////////////////////////////////////////////////
			util.ui.crearQr(obj.CodigoUA, function (urlQR) {
				self.getView().getModel("localModel").setProperty("/QrCode", urlQR);
			});
			util.ui.crearCodigoBarras(obj.CodigoUA, function (urlCodBarras) {
				self.getView().getModel("localModel").setProperty("/BarCode", urlCodBarras);
			});
			////////////////////////////////////////////////////////////////////
		},
		onEditarUA: function () {
			var self = this;
			var obj = self.getView().getModel("localModel").getProperty("/UADetalle");
			var obj2 = JSON.parse(JSON.stringify(obj));
			
			//obj2.UbicacionId = null;
			obj2.Peso = obj2.PesoBruto;
			obj2.EsEdicion = true;
			obj2.HabilitadoNoEdicion = false;
			obj2.EsMalEstado = obj2.EsMalEstado ? true : false;
			if (obj2.EsMalEstado === true) {
				obj2.habilitarFotos = true;
			} else {
				obj2.habilitarFotos = false;
			}
			obj2.TieneEmbalaje = obj2.TieneEmbalaje ? true : false;
			self.getView().getModel("miData").setProperty("/", obj2);
			self.selectionDocTransporte(null, obj2);
			self.onChangeTipoAlmacen({
				Id: obj2.UbicacionId,
				Codigo: obj2.UbicacionCodigo
			});
			
			if (obj2.habilitarFotos === true) {
				this.getView().byId("inputFoto1").setFieldGroupIds("fieldValidViewgArmadoUA");
				this.getView().byId("inputFoto2").setFieldGroupIds("fieldValidViewgArmadoUA");
				this.getView().byId("inputFoto3").setFieldGroupIds("fieldValidViewgArmadoUA");
				this.getView().byId("inputFoto4").setFieldGroupIds("fieldValidViewgArmadoUA");
			} else {
				this.getView().byId("inputFoto1").setFieldGroupIds("");
				this.getView().byId("inputFoto2").setFieldGroupIds("");
				this.getView().byId("inputFoto3").setFieldGroupIds("");
				this.getView().byId("inputFoto4").setFieldGroupIds("");
			}
			
			// Obtener las 4 Fotografías anexadas a la Unidad de Almacenamiento
			var oFiltro = [];
			if (obj2.IdFoto1 && obj2.IdFoto1 != null) {
				oFiltro.push(new Filter("Id", "EQ", obj2.IdFoto1));
			}
			if (obj2.IdFoto2 && obj2.IdFoto2 != null) {
				oFiltro.push(new Filter("Id", "EQ", obj2.IdFoto2));
			}
			if (obj2.IdFoto3 && obj2.IdFoto3 != null) {
				oFiltro.push(new Filter("Id", "EQ", obj2.IdFoto3));
			}
			if (obj2.IdFoto4 && obj2.IdFoto4 != null) {
				oFiltro.push(new Filter("Id", "EQ", obj2.IdFoto4));
			}
			
			if (oFiltro[0]){
				self.obtenerAdjuntos(oFiltro,function (data){
					var data_ua = self.getView().getModel("miData").getProperty("/");
					
					data.forEach(function(e, index){
						if (data_ua.IdFoto1 === e.Id) {
							data_ua.foto1 = {};
							data_ua.foto1.Id = e.Id;
							data_ua.foto1.IdAdjunto = e.IdAdjunto;
							data_ua.foto1.NombreAdjunto = e.NombreAdjunto;
							data_ua.foto1.RutaAdjunto = e.RutaAdjunto;
						}
						if (data_ua.IdFoto2 === e.Id) {
							data_ua.foto2 = {};
							data_ua.foto2.Id = e.Id;
							data_ua.foto2.IdAdjunto = e.IdAdjunto;
							data_ua.foto2.NombreAdjunto = e.NombreAdjunto;
							data_ua.foto2.RutaAdjunto = e.RutaAdjunto;
						}
						if (data_ua.IdFoto3 === e.Id) {
							data_ua.foto3 = {};
							data_ua.foto3.Id = e.Id;
							data_ua.foto3.IdAdjunto = e.IdAdjunto;
							data_ua.foto3.NombreAdjunto = e.NombreAdjunto;
							data_ua.foto3.RutaAdjunto = e.RutaAdjunto;
						}
						if (data_ua.IdFoto4 === e.Id) {
							data_ua.foto4 = {};
							data_ua.foto4.Id = e.Id;
							data_ua.foto4.IdAdjunto = e.IdAdjunto;
							data_ua.foto4.NombreAdjunto = e.NombreAdjunto;
							data_ua.foto4.RutaAdjunto = e.RutaAdjunto;
						}
					});
					
					self.getView().getModel("miData").setProperty("/", data_ua);
				});
			}

			self.getView().byId("idListDocumentoTransporte").setEnabled(false);
			self.getView().byId("cbUbicacionArmado").setEnabled(false);
			self.onCerrarDlg_dialogDetalleUA();
			self.getView().byId("idSegmentButton").setSelectedKey("UA");
			var navCon = self.getView().byId("navDocumentoTransporte");
			navCon.to(self.getView().byId("pgDocTransporte"), "slide");
			//self.getView().byId("btnArmarUA").setText("Editar UA");
			self.validBotonEditarUA(true);
			//var array = self.getView().getModel("modelOdata").oData.VUbicacionDisponibles;
			//console.log(array);
			//array.push(obj2);
			//console.log(array);
			//self.getView().getModel("modelOdata").setProperty("/VUbicacionDisponibles", array);
		},
		onCerrarDlg_dialogDetalleUA: function () {
			sap.ui.getCore().byId("dlg_dialogDetalleUA").destroy();
		},
		onChangeOrganizacionVenta: function () {
			var self = this;
			var iOrgvta = self.getView().getModel("miData").getProperty("/iIdOrganizacionVenta");
			var iSede = self.getView().getModel("miData").getProperty("/iIdCentro");
			if (!iOrgvta || !iSede) {
				return MessageToast.show("Ingreser Centro y Org. de ventas", {
					duration: 5000,
					width: "25em"
				});
			}
			var oData = {
				sIOrgvta: iOrgvta,
				sISede: iSede
			}
			var oResults = util.envioDatos.consultarCanalesSectores(oData);
			sap.ui.core.BusyIndicator.show(0);
			Services.consultarCanalesSectores(self, oResults, function (iResults) {
				sap.ui.core.BusyIndicator.hide();
				iResults.oAuditResponse.oResults = util.controller.unlink(iResults.oAuditResponse);
				util.response.validate(iResults.oAuditResponse, {
					success: function (data, message) {
						self.getView().getModel("miData").setProperty("/Canales", iResults.oResults.TCanal);
						self.getView().getModel("miData").setProperty("/Sector", iResults.oResults.TSector);
					},
				});
			});
		},
		onConsultarConceptosFacturables: function () {
			var self = this;
			var sIOrgvta = self.getView().getModel("Sap").getProperty("/iIdOrganizacionVenta");
			var sISede = self.getView().getModel("Sap").getProperty("/iIdCentro");
			var sICanal = self.getView().getModel("Sap").getProperty("/iIdCanal");
			var sISector = self.getView().getModel("Sap").getProperty("/iIdSector");
			var sIPrdha = self.getView().getModel("Sap").getProperty("/sPrdha");
			var oData = {
				sICanal: sICanal,
				sIOrgvta: sIOrgvta,
				sIPrdha: sIPrdha,
				sISector: sISector, //
				sISede: sISede
			}
			var oResults = util.envioDatos.consultarConceptosFacturables(oData);
			sap.ui.core.BusyIndicator.show(0);
			Services.consultarConceptosFacturables(self, oResults, function (iResults) {
				sap.ui.core.BusyIndicator.hide();
				iResults.oAuditResponse.oResults = util.controller.unlink(iResults.oAuditResponse);
				util.response.validate(iResults.oAuditResponse, {
					success: function (data, message) {
						self.getView().getModel("localModel").setProperty("/ConceptoFacturable", iResults.oResults.TConfac);
					},
					error: function (message) {
						self.getView().getModel("localModel").setProperty("/ConceptoFacturable", []);
					}
				});
			});
		},
		onGenerarEtiquetaDlg_dialogDetalleUA: function () {
			var qr = this.getView().getModel("localModel").getProperty("/QrCode");
			var bar = this.getView().getModel("localModel").getProperty("/BarCode");
			var obj = this.getView().getModel("localModel").getProperty("/UADetalle");
			var documentoTransporteResumen = this.getView().getModel("localModel").getProperty("/DocumentoTransporteResumen");

			var manifiesto = this.getView().getModel("detailManifiesto").getData();
			obj.DescripcionTransportista = manifiesto.DescripcionTransportista;
			/////Inicio Nuevo Roy 03-12-2019
			var objResumen = documentoTransporteResumen.find(function (el) {
				return el.Id == obj.DocumentoTransporteId;
			});
			obj.FechaIngreso = objResumen.FechaIngreso;
			/////End Nuevo Roy 03-12-2019
			var mapPDF = Estructura.detailPDF.mapExportEtiquetaPDF(obj);
			mapPDF.qr = qr;
			mapPDF.bar = bar;
			util.ui.onGenerarEtiquetaPDF(mapPDF, [170, 120]);
		},
		onCalcularPesoNeto: function (oEvent) {
			var pesoParhiuela = parseFloat(oEvent.getParameter("value"));
			if (!pesoParhiuela && pesoParhiuela !== 0) {
				this.getView().getModel("miData").setProperty("/PesoNeto", null);
			} else {
				this.getView().getModel("miData").setProperty("/PesoParhiuela", pesoParhiuela);
				var pesoBruto = this.getView().getModel("miData").getProperty("/Peso");
				var pesoNeto = pesoBruto - pesoParhiuela;
				this.getView().getModel("miData").setProperty("/PesoNeto", pesoNeto);
			}
		},
		validBotonEditarUA: function (Editar) {
			var self = this;
			self.getView().getModel("localModel").setProperty("/ValidBotones", {});
			if (Editar) {
				self.getView().getModel("localModel").setProperty("/ValidBotones/CancelarEditar", true);
				self.getView().getModel("localModel").setProperty("/ValidBotones/EtiquetaUA", "Editar UA");
				self.getView().byId("idListDocumentoTransporte").setEnabled(false);
				self.getView().byId("cbUbicacionArmado").setEnabled(false);
			} else {
				self.getView().getModel("localModel").setProperty("/ValidBotones/CancelarEditar", false);
				self.getView().getModel("localModel").setProperty("/ValidBotones/EtiquetaUA", "Armar UA");
				self.getView().byId("idListDocumentoTransporte").setEnabled(true);
				self.getView().byId("cbUbicacionArmado").setEnabled(true);
				self.getView().getModel("localModel").setProperty("/DocumentoTransporte/0", {});
				self.getView().byId("fPesoParihuela").setValue("");
			}
		},
		onCancelarEditarUA: function () {
			this.limpiarDataArmarUA();
			this.validBotonEditarUA(false);
		},
		limpiarDataArmarUA: function () {
			this.getView().getModel("miData").setProperty("/", {});
		},
		
		selectMalEstado: function (oEvent) {
			var self = this;
			var habilitado = oEvent.getSource().getProperty("selected");
			
			if (habilitado === true) {
				this.getView().getModel("miData").setProperty("/habilitarFotos", habilitado);
				
				this.getView().byId("inputFoto1").setFieldGroupIds("fieldValidViewgArmadoUA");
				this.getView().byId("inputFoto2").setFieldGroupIds("fieldValidViewgArmadoUA");
				this.getView().byId("inputFoto3").setFieldGroupIds("fieldValidViewgArmadoUA");
				this.getView().byId("inputFoto4").setFieldGroupIds("fieldValidViewgArmadoUA");
				
			} else if (habilitado === false) {	
				var mensaje = "¿Seguro de desmarcar? Se borrarán todas las fotos cargadas";
				util.ui.messageBox(mensaje, 'c', function (bConfirmacion) {
					if (bConfirmacion) {
						
						var linea, items;
						var foto1 = self.getView().getModel("miData").getProperty("/foto1");
						var foto2 = self.getView().getModel("miData").getProperty("/foto2");
						var foto3 = self.getView().getModel("miData").getProperty("/foto3");
						var foto4 = self.getView().getModel("miData").getProperty("/foto4");
						
						items = [];
						if (foto1) {
							linea = {};
							linea.Id = foto1.Id;
							linea.IdAdjunto = foto1.IdAdjunto;
							items.push(linea);
						}
						if (foto2) {
							linea = {};
							linea.Id = foto2.Id;
							linea.IdAdjunto = foto2.IdAdjunto;
							items.push(linea);
						}
						if (foto3) {
							linea = {};
							linea.Id = foto3.Id;
							linea.IdAdjunto = foto3.IdAdjunto;
							items.push(linea);
						}
						if (foto4) {
							linea = {};
							linea.Id = foto4.Id;
							linea.IdAdjunto = foto4.IdAdjunto;
							items.push(linea);
						}
						
						if (items[0]) {
							self.eliminarFotos(items, {
								success: function () {
									self.getView().getModel("miData").setProperty("/habilitarFotos", habilitado);
									
									self.getView().getModel("miData").setProperty("/foto1");
									self.getView().getModel("miData").setProperty("/foto2");
									self.getView().getModel("miData").setProperty("/foto3");
									self.getView().getModel("miData").setProperty("/foto4");
									
									self.getView().byId("inputFoto1").setFieldGroupIds("");
									self.getView().byId("inputFoto2").setFieldGroupIds("");
									self.getView().byId("inputFoto3").setFieldGroupIds("");
									self.getView().byId("inputFoto4").setFieldGroupIds("");
								},
								
								error: function () {
									self.getView().byId("cbMalEstado").setSelected(true);
								}
							});
						} else {
							self.getView().getModel("miData").setProperty("/habilitarFotos", habilitado);
							self.getView().getModel("miData").setProperty("/foto1");
							self.getView().getModel("miData").setProperty("/foto2");
							self.getView().getModel("miData").setProperty("/foto3");
							self.getView().getModel("miData").setProperty("/foto4");
							
							self.getView().byId("inputFoto1").setFieldGroupIds("");
							self.getView().byId("inputFoto2").setFieldGroupIds("");
							self.getView().byId("inputFoto3").setFieldGroupIds("");
							self.getView().byId("inputFoto4").setFieldGroupIds("");
						}
				
					} else {
						self.getView().byId("cbMalEstado").setSelected(true);
						return;
					}
				});
			}
			
		},
		
		pressFoto1: function () {
			this.getView().getModel("miData").setProperty("/fotoSeleccionada", "/foto1");
			this.dlg_dialogCamera = sap.ui.xmlfragment("saasa.armadoUA.view.Frag.Dialogs.dlg_dialogCamara", this);
			this.getView().addDependent(this.dlg_dialogCamera);
			this.dlg_dialogCamera.open();
		},
		pressFoto2: function () {
			this.getView().getModel("miData").setProperty("/fotoSeleccionada", "/foto2");
			this.dlg_dialogCamera = sap.ui.xmlfragment("saasa.armadoUA.view.Frag.Dialogs.dlg_dialogCamara", this);
			this.getView().addDependent(this.dlg_dialogCamera);
			this.dlg_dialogCamera.open();
		},
		pressFoto3: function () {
			this.getView().getModel("miData").setProperty("/fotoSeleccionada", "/foto3");
			this.dlg_dialogCamera = sap.ui.xmlfragment("saasa.armadoUA.view.Frag.Dialogs.dlg_dialogCamara", this);
			this.getView().addDependent(this.dlg_dialogCamera);
			this.dlg_dialogCamera.open();
		},
		pressFoto4: function () {
			this.getView().getModel("miData").setProperty("/fotoSeleccionada", "/foto4");
			this.dlg_dialogCamera = sap.ui.xmlfragment("saasa.armadoUA.view.Frag.Dialogs.dlg_dialogCamara", this);
			this.getView().addDependent(this.dlg_dialogCamera);
			this.dlg_dialogCamera.open();
		},
		
		pressBorrarFoto1: function () {
			var self = this;
			var datos_foto, items, linea;
			this.getView().getModel("miData").setProperty("/fotoSeleccionada", "/foto1");
			
			datos_foto = this.getView().getModel("miData").getProperty("/foto1");
			if (datos_foto) {
				items = [];
				linea = {};
				linea.Id = datos_foto.Id;
				linea.IdAdjunto = datos_foto.IdAdjunto;
				items.push(linea);
				
				this.eliminarFotos(items, {
					success: function () {
						self.getView().getModel("miData").setProperty("/foto1");
					},
					error: function () {}
				});
			}
		},
		pressBorrarFoto2: function () {
			var self = this;
			var datos_foto, items, linea;
			this.getView().getModel("miData").setProperty("/fotoSeleccionada", "/foto2");
			
			datos_foto = this.getView().getModel("miData").getProperty("/foto2");
			if (datos_foto) {
				items = [];
				linea = {};
				linea.Id = datos_foto.Id;
				linea.IdAdjunto = datos_foto.IdAdjunto;
				items.push(linea);
				
				this.eliminarFotos(items, {
					success: function () {
						self.getView().getModel("miData").setProperty("/foto2");
					},
					error: function () {}
				});
			}
		},
		pressBorrarFoto3: function () {
			var self = this;
			var datos_foto, items, linea;
			this.getView().getModel("miData").setProperty("/fotoSeleccionada", "/foto3");
			
			datos_foto = this.getView().getModel("miData").getProperty("/foto3");
			if (datos_foto) {
				items = [];
				linea = {};
				linea.Id = datos_foto.Id;
				linea.IdAdjunto = datos_foto.IdAdjunto;
				items.push(linea);
				
				this.eliminarFotos(items, {
					success: function () {
						self.getView().getModel("miData").setProperty("/foto3");
					},
					error: function () {}
				});
			}
		},
		pressBorrarFoto4: function () {
			var self = this;
			var datos_foto, items, linea;
			this.getView().getModel("miData").setProperty("/fotoSeleccionada", "/foto4");
			
			datos_foto = this.getView().getModel("miData").getProperty("/foto4");
			if (datos_foto) {
				items = [];
				linea = {};
				linea.Id = datos_foto.Id;
				linea.IdAdjunto = datos_foto.IdAdjunto;
				items.push(linea);
				
				this.eliminarFotos(items, {
					success: function () {
						self.getView().getModel("miData").setProperty("/foto4");
					},
					error: function () {}
				});
			}
		},
		
		afterCloseCamara: function () {
			this.dlg_dialogCamera.destroy();
		},
		
		onSnapshot: function (oEvent) {
		    var imagen = oEvent.getParameter("image");
		    var fotoSeleccionada = this.getView().getModel("miData").getProperty("/fotoSeleccionada");
		    this.subirFoto(fotoSeleccionada, imagen);
		    this.dlg_dialogCamera.destroy();
		},
		
		subirFoto: function (pathFoto, imagen) {
			var accion;
			var self = this;
			var obj = {};
			
			var manifiesto = this.getView().getModel("detailManifiesto").getProperty("/");
			if (!manifiesto.Id){
				MessageToast.show("Seleccione manifiesto");
				return;
			}
			
			var codigoUA = this.getView().getModel("miData").getProperty("/CodigoUA");
			if (!codigoUA){
				MessageToast.show("Debe generar Código UA");
				return;
			}
			
			var otros_folder = this.getView().getModel("maestroModel").getProperty("/T_TIPO_DOC_ADJUNTO");
			otros_folder = otros_folder.find(function (e) {
				return e.Campo === "10"; // Id de Folder: Otros
			});
			
			var objFoto = this.getView().getModel("miData").getProperty(pathFoto);
			if (!objFoto) {
				accion = "R"; // Registrar
				var objFoto = {};
			} else {
				obj.Id = objFoto.Id;
				obj.IdAdjunto = objFoto.IdAdjunto;
				accion = "A"; // Actualizar
			}
			
			obj.IdManifiesto = manifiesto.Id;
			obj.Base64File = imagen.slice(22);
			obj.Extension = "image/png";
			obj.iIdTipoAdjunto = otros_folder.Codigo; // Id de Tipo de Adjunto: Otros
			obj.sIdFolder = otros_folder.CodigoSap; // Id de Folder: Otros
			obj.FileName = manifiesto.NumeroManifiesto + "_" + codigoUA + "_Foto" + pathFoto.slice(5);
			if (accion === "R") {
				var oResults = util.envioDatos.registrarAdjunto(obj);
			} else {
				var oResults = util.envioDatos.actualizarAdjunto(obj);
			}
			
			this.dlg_dialogCamera.destroy();
			sap.ui.core.BusyIndicator.show(0);
			Services.Adjunto(self, oResults, function (result) {
				sap.ui.core.BusyIndicator.hide();
				util.response.validateAdjunto(result, {
					success: function (data, message) {
						if (accion === "R") {
							util.ui.onMensajeGeneral(self, "Se registró correctamente la foto");
						} else {
							util.ui.onMensajeGeneral(self, "Se actualizó correctamente foto");
						}
						// Acciones después de consumir servicio
						
						var oFiltro = [];
						oFiltro.push(new Filter("Id", "EQ", result.oResults.Id));
						self.obtenerAdjuntos(oFiltro,function (data){
							var ruta_foto = self.getView().getModel("miData").getProperty("/fotoSeleccionada");
							objFoto.Id = data[0].Id;
							objFoto.IdAdjunto = data[0].IdAdjunto;
							objFoto.NombreAdjunto = data[0].NombreAdjunto;
							objFoto.RutaAdjunto = data[0].RutaAdjunto;
							
							self.getView().getModel("miData").setProperty(ruta_foto, objFoto);
						});
					},
					error: function (message) {
						util.ui.onMessageErrorDialogPress(message);
					}
				});
			});
		},
		
		eliminarFotos: function (aItems, callback) {
			var self = this;
			var oResults = util.envioDatos.eliminarAdjunto(aItems);
			sap.ui.core.BusyIndicator.show(0);
			Services.eliminarAdjunto(self, oResults, function (result) {
				sap.ui.core.BusyIndicator.hide();
				util.response.validateAdjunto(result, {
					success: function (data, message) {
						if (aItems.length == 1) {
							util.ui.onMensajeGeneral(self, "Se ha eliminado la foto.");
						} else {
							util.ui.onMensajeGeneral(self, "Se han elmiminado las Fotos seleccionados.");
						}
						callback.success();
					},
					error: function (message) {
						callback.error();
					}
				});
			});
		},
		
		obtenerAdjuntos: function (filtro, callback) {
			var self = this;
			sap.ui.core.BusyIndicator.show(0);
			Services.getOdataAdjuntos(self, filtro, function (result) {
				sap.ui.core.BusyIndicator.hide();
				util.response.validate(result, {
					success: function (data, message) {
						callback(data);
					},
					error: function (message) {
					}
				});
			});
		}

	});
});