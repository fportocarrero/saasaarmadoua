sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"./model/models",
	"sap/ui/model/json/JSONModel"
], function (UIComponent, Device, models, JSONModel) {
	"use strict";

	return UIComponent.extend("saasa.armadoUA.Component", {

		metadata: {
			manifest: "json",
			// activate automatic message generation
			config: {
				fullWidth: true //Set your fullscreen parameter here!
			}
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			/*
					// set local data model (schedule.json)
					var oConfig = this.getMetadata().getConfig();
					var sNamespace = this.getMetadata().getManifestEntry("sap.app").id;
					// mapping to the property "modelLocal" in the "config" property of the app descriptor
					var oLocalModel = new JSONModel(jQuery.sap.getModulePath(sNamespace, oConfig.modelLocal));
					this.setModel(oLocalModel);
			*/

			// enable routing
			this.getRouter().initialize();

			// set the device model
			this.setModel(models.createDeviceModel(), "device");
		}
	});
});