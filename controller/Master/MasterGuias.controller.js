sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/UIComponent",
	"../../util/util",
	'sap/ui/model/Filter',
	"../../model/mockdata",
	"../../model/models",
	"../../viewPass",
	"../../services/Services",
	"../../estructuras/Estructura",
	"../BaseController"
], function (Controller, UIComponent, util, Filter, mockdata, models, viewPass, Services, Estructura, BaseController) {
	"use strict";
	return BaseController.extend("saasa.armadoUA.controller.MasterGuias", {
		onInit: function () {
			var oRouter = UIComponent.getRouterFor(this);
			oRouter.attachRoutePatternMatched(this.onRouteMatched, this);
		},
		onRouteMatched: function (oEvent) {

		},
		onAfterRendering: function () {
			viewPass.MasterGuias = this;
		},
		onRefresh: function () {
			this.getView().getModel("modelOdata").refresh(true);
		},
		backMaster: function () {
			util.controller.navToBackMaster(viewPass.Home, "SplitApp");
		},
		onListMaster: function (oEvent) {
			var self = this;
			util.ui.loadingPage(self, true);
			var objList = util.controller.unlink(oEvent.getSource().getSelectedItem().getBindingContext("modelOdata").getObject());

			

			var mensaje = "Desea Armar UA's del Documento de Transporte: " + objList.DocumentoTransporteCodigo;
			util.ui.messageBox(mensaje, 'c', function (bConfirmacion) {
				if (bConfirmacion) {
					self.cargarComboTablas();
					util.controller.createDialog(self, "dlg_dialogDocumentoTransporte", "view.Frag.Dialogs.dlg_dialogDocumentoTransporte");
				}
			});
			return;
			util.ui.loadingPage(self, false);
		},
		onUA: function () {
			var navCon = sap.ui.getCore().byId("navDocumentoTransporte");
			navCon.to(sap.ui.getCore().byId("pgDocTransporte"), "slide");
		},
		onListaUAsArmada: function () {
			var navCon = sap.ui.getCore().byId("navDocumentoTransporte");
			navCon.to(sap.ui.getCore().byId("pgListaUAsArmada"), "slide");
			var listUAS = mockdata.modelListUAS();
			this.getView().getModel("localModel").setProperty("/VUAS", listUAS);
		},
		onCerrarDlg_dialogDocumentoTransporte: function () {
			sap.ui.getCore().byId("dlg_dialogDocumentoTransporte").destroy();
			util.controller.createDialog(this, "dlg_dialogCerrarDocumentoTransporte", "view.Frag.Dialogs.dlg_dialogCerrarDocumentoTransporte");
		},
		onArmarUADlg_dialogDocumentoTransporte: function () {
			sap.ui.getCore().byId("dlg_dialogDocumentoTransporte").destroy();
		},
		onCancelarDlg_dialogCerrarDocumentoTransporte: function () {
			sap.ui.getCore().byId("dlg_dialogCerrarDocumentoTransporte").destroy();
		},
		onAceptarDlg_dialogCerrarDocumentoTransporte: function () {
			sap.ui.getCore().byId("dlg_dialogCerrarDocumentoTransporte").destroy();
		},
		onBuscarDocumentoTransporte: function () {
			//var selectKey = this.getView().getModel("localModel").getProperty("/BuscarManifiesto/selectCombo");
			var filter = [{
				key: "NumeroManifiesto",
				query: "Contains"
			}, {
				key: "NroViaje",
				query: "Contains"
			}];
			util.ui.onFiltrarTablaLive(this, "searchBuscar", "idListMasterGuias", filter);
		},

	});
});