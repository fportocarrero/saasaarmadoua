sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/UIComponent",
	"../../util/util",
	'sap/ui/model/Filter',
	"../../model/models",
	"../../viewPass",
	"../../services/Services",
	"../../estructuras/Estructura",
	"../BaseController"
], function (Controller, UIComponent, util, Filter, models, viewPass, Services, Estructura, BaseController) {
	"use strict";
	return BaseController.extend("saasa.armadoUA.controller.MasterGuias", {
		onInit: function () {
			var oRouter = UIComponent.getRouterFor(this);
			oRouter.attachRoutePatternMatched(this.onRouteMatched, this);
		},
		onRouteMatched: function (oEvent) {

		},
		
	});
});