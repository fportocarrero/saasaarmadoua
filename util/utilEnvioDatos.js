jQuery.sap.require("sap.ui.core.format.DateFormat");
sap.ui.define([
	"./util"
], function (util) {
	"use strict";
	return {
		calcularPeso: function (ipBalanza) {
			var modDatos = {
				oResults: {
					sIP: ipBalanza
				}
			};
			return modDatos;
		},
		armarUA: function (datos, guias) {
			var servDatos = datos;
			servDatos.oData = servDatos.oData ? servDatos.oData : {};
			var modDatos = {
				oResults: {
					iId: servDatos.oData.Id,
					sCodigoUA: servDatos.CodigoUA,
					iUbicacionId: servDatos.UbicacionId,
					iEsMalEstado: servDatos.EsMalEstado ? 1 : 0,
					fPeso: servDatos.Peso,
					iTieneEmbalaje: servDatos.TieneEmbalaje ? 1 : 0,
					iTipoCargaId: servDatos.TipoCargaId,
					iTipoAlmacenamientoId: servDatos.TipoAlmacenamientoId,
					fPesoParihuela: servDatos.PesoParhiuela,
					fPesoNeto: servDatos.PesoNeto,
					sConcFacturableCodigo: servDatos.sConceptoFacturable,
					sConcFacturableDescripcion: servDatos.sConceptoFacturableDesc,
					sBalanza: servDatos.oData.Balanza,
					iUbicacionZRId: servDatos.oData.iIdUbicacionInicial,
					EsEdicion: servDatos.oData.EsEdicion ? true : false,
					iIdFoto1: servDatos.IdFoto1,
					iIdFoto2: servDatos.IdFoto2,
					iIdFoto3: servDatos.IdFoto3,
					iIdFoto4: servDatos.IdFoto4,
					aGuias: [],
					oMovimiento: {
						iIdUbicacionInicial: servDatos.iIdUbicacionInicial,
						iIdUbicacionFinal: servDatos.UbicacionId
					},
					oConceptoFacturable: {
						sSede: servDatos.sSede,
						sSedeDesc: servDatos.sSedeDesc,
						sOrganizacionVenta: servDatos.sOrganizacionVenta,
						sOrganizacionVentaDesc: servDatos.sOrganizacionVentaDesc,
						sSector: servDatos.sSector,
						sSectorDesc: servDatos.sSectorDesc,
						sConceptoFacturable: servDatos.sConceptoFacturable,
						sConceptoFacturableDesc: servDatos.sConceptoFacturableDesc,
						sCanal: servDatos.sCanal,
						sCanalDesc: servDatos.sCanalDesc,
						sPrdha: servDatos.sPrdha,
						sPrdhaDesc: servDatos.sPrdhaDesc
					}
				}
			};
			for (var i in guias) {
				modDatos.oResults.aGuias.push({
					iId: guias[i].Id,
					iBultos: guias[i].BultoArmado
				});
			}
			return modDatos;
		},

		registrarResumen: function (manifiesto, guias) {
			var servDatos = manifiesto;
			var modDatos = {
				oResults: {
					iIdManifiesto: servDatos.Id,
					aGuias: []
				}
			};
			for (var i in guias) {
				modDatos.oResults.aGuias.push({
					iId: guias[i].Id,
					iBultosRecibidos: guias[i].BultosRecibidos,
					fPesoRecibido: guias[i].PesoRecibido,
					iBultosFaltantes: guias[i].BultosFaltantes,
					fPesoFaltante: guias[i].PesoFaltante,
					iBultosSobrantes: guias[i].BultosSobrantes,
					fPesoSobrante: guias[i].PesoSobrante,
					iBultosMalEstado: guias[i].BultosMalEstado,
					fPesoMalEstado: guias[i].PesoMalEstado,
					iBultosBuenEstado: guias[i].BultosBuenEstado,
					fPesoBuenEstado: guias[i].PesoBuenEstado,
					sNumeroIncidencia: (guias[i].Correlativo) ? guias[i].Correlativo : null,
					sComentario: (guias[i].ActaInventarioObs) ? guias[i].ActaInventarioObs : null,
					dFechaIngreso: guias[i].FechaIngresoDT ? guias[i].FechaIngresoDT : null,
				});
			}
			return modDatos;
		},

		finalizar: function (manifiesto, guias) {
			var servDatos = manifiesto;
			var modDatos = {
				oResults: {
					iIdManifiesto: servDatos.Id,
					sNumeroIncidencia: servDatos.sNumeroIncidencia,
					sComentario: servDatos.sComentario,
					aGuias: []
				}
			};
			for (var i in guias) {
				modDatos.oResults.aGuias.push({
					iId: guias[i].Id,
					iBultosRecibidos: guias[i].BultosRecibidos,
					fPesoRecibido: guias[i].PesoRecibido,
					iBultosFaltantes: guias[i].BultosFaltantes,
					fPesoFaltante: guias[i].PesoFaltante,
					iBultosSobrantes: guias[i].BultosSobrantes,
					fPesoSobrante: guias[i].PesoSobrante,
					iBultosMalEstado: guias[i].BultosMalEstado,
					fPesoMalEstado: guias[i].PesoMalEstado,
					iBultosBuenEstado: guias[i].BultosBuenEstado,
					fPesoBuenEstado: guias[i].PesoBuenEstado
				});
			}
			return modDatos;
		},

		registrarVolante: function (IdItinerario, datos) {
			var servDatos = datos;
			var modDatos = {
				oResults: {
					aItems: []
				}
			};
			for (var i in servDatos) {
				modDatos.oResults.aItems.push({
					Id: servDatos[i].Id,

					iIdManifiesto: servDatos[i].IdManifiesto,
					iIdDocumentoTransporte: servDatos[i].Id,
					iIdItinerario: IdItinerario,
					sDocumentoTransporteCodigo: servDatos[i].DocumentoTransporteCodigo,

					EsMaster: servDatos[i].EsMaster,
					EsMasterDescripcion: servDatos[i].EsMasterDescripcion,
					DocumentoTransporteMadreId: servDatos[i].DocumentoTransporteMadreId,
					DocumentoTransporteMasterCodigo: servDatos[i].DocumentoTransporteMasterCodigo,
					DocumentoTransporteCodigo: servDatos[i].DocumentoTransporteCodigo,
					Descripcion: servDatos[i].Descripcion,
					BultosManifestados: servDatos[i].BultosManifestados,
					PesoManifestado: servDatos[i].PesoManifestado,
					BultosRecibidos: servDatos[i].BultosRecibidos,
					PesoRecibido: servDatos[i].PesoRecibido,
					BultosFaltantes: servDatos[i].BultosFaltantes,
					PesoFaltante: servDatos[i].PesoFaltante,
					BultosSobrantes: servDatos[i].BultosSobrantes,
					PesoSobrante: servDatos[i].PesoSobrante,
					BultosMalEstado: servDatos[i].BultosMalEstado,
					PesoMalEstado: servDatos[i].PesoMalEstado,
					Origen: servDatos[i].Origen,
					Destino: servDatos[i].Destino,
					OrigenDescripcion: servDatos[i].OrigenDescripcion,
					DestinoDescripcion: servDatos[i].DestinoDescripcion,
					PuntoLlegada: servDatos[i].PuntoLlegada,
					PuntoLlegadaDescripcion: servDatos[i].PuntoLlegadaDescripcion,
					TerminalOrigen: servDatos[i].TerminalOrigen,
					TerminalOrigenDescripcion: servDatos[i].TerminalOrigenDescripcion,
					TerminalDestino: servDatos[i].TerminalDestino,
					TerminalDestinoDescripcion: servDatos[i].TerminalDestinoDescripcion,
					IdModalidad: servDatos[i].IdModalidad,
					ModalidadDescripcion: servDatos[i].ModalidadDescripcion,
					IdTipoCarga: servDatos[i].IdTipoCarga,
					TipoCargaDescripcion: servDatos[i].TipoCargaDescripcion,
					IdTipoAlmacenamiento: servDatos[i].IdTipoAlmacenamiento,
					TipoAlmacenamientoDescripcion: servDatos[i].TipoAlmacenamientoDescripcion,
					IdRegimen: servDatos[i].IdRegimen,
					RegimenAduanaCodigo: servDatos[i].RegimenAduanaCodigo,
					RegimenAduanaCodigoSAP: servDatos[i].RegimenAduanaCodigoSAP,
					RegimenAduanaDescripcion: servDatos[i].RegimenAduanaDescripcion,
					AgenteCarga: servDatos[i].AgenteCarga,
					Embarcador: servDatos[i].Embarcador,
					Consignatario: servDatos[i].Consignatario,
					IdManifiesto: servDatos[i].IdManifiesto,
					IdContenedor: servDatos[i].IdContenedor,
					EsCargaSuelta: servDatos[i].EsCargaSuelta,
					EsExpo: servDatos[i].EsExpo,
					Adjunto: servDatos[i].Adjunto,
				});
			}
			return modDatos;
		},
		eliminarUA: function (datos, guias) {
			var servDatos = datos;
			var modDatos = {
				oResults: {
					iId: servDatos.Id
				}
			};
			return modDatos;
		},
		inicioTarja: function (datos, guias) {
			var servDatos = datos;
			var modDatos = {
				oResults: {
					iIdManifiesto: servDatos.Id
				}
			};
			return modDatos;
		},
		consultarSedesOrgVentas: function () {
			var modDatos = {
				oResults: {}
			};
			return modDatos;
		},
		consultarCanalesSectores: function (datos) {
			var servDatos = datos;
			var modDatos = {
				oResults: servDatos
			};
			return modDatos;
		},
		consultarConceptosFacturables: function (datos) {
			var servDatos = datos;
			var modDatos = {
				oResults: servDatos
			};
			return modDatos;
		},
		registrarAdjunto: function (datos) {
			var servDatos = datos;
			var modDatos = {
				oResults: {
					flagAdjunto: servDatos.FileName && servDatos.FileName != "" ? true : false,
					sIdFolder: servDatos.sIdFolder,
					sBase64File: servDatos.Base64File,
					sExtension: servDatos.Extension,
					sFileName: servDatos.FileName,
					iIdManifiesto: servDatos.IdManifiesto,
					iIdTipoAdjunto: servDatos.iIdTipoAdjunto
				}
			};
			return modDatos;
		},
		actualizarAdjunto: function (datos) {
			var servDatos = datos;
			var modDatos = {
				oResults: {
					iId: servDatos.Id.toString(),
					iIdAdjunto: servDatos.IdAdjunto.toString(),
					
					flagAdjunto: servDatos.FileName && servDatos.FileName != "" ? true : false,
					sIdFolder: servDatos.sIdFolder,
					sBase64File: servDatos.Base64File,
					sExtension: servDatos.Extension,
					sFileName: servDatos.FileName,
					iIdManifiesto: servDatos.IdManifiesto,
					iIdTipoAdjunto: servDatos.iIdTipoAdjunto
				}
			};
			return modDatos;
		},
		eliminarAdjunto: function (datos) {
			var servDatos = datos;
			var modDatos = {
				oResults: {
					aItems: []
				}
			};
			for (var i in servDatos) {
				modDatos.oResults.aItems.push({
					iId: servDatos[i].Id,
					iIdAdjunto: servDatos[i].IdAdjunto,
				});
			}
			return modDatos;
		}
	};
});