sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/UIComponent",
	"jquery.sap.global",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"sap/m/UploadCollectionParameter",
	"../../estructuras/Estructura",
	'sap/ui/model/Filter',
	"../../util/util",
	"../../viewPass",
	"../../services/Services",
	"../BaseController"
], function (Controller, UIComponent, jQuery, MessageBox, MessageToast, UploadCollectionParameter, Estructura, Filter, util, viewPass,
	Services, BaseController) {
	"use strict";
	return BaseController.extend("saasa.armadoUA.controller.Detail.Detail", {
		onInit: function () {
			/*var oRouter = UIComponent.getRouterFor(this);
			oRouter.attachRoutePatternMatched(this.onRouteMatched, this);
			var self = this;
			self.getUsuario(self);
			self.getManifiestoArmado(self); */
		},
		onRouteMatched: function (oEvent) {
			if (oEvent.getParameter("name") === "appDetail") {

				this.getView().getModel("localModel").setProperty("/Sap", {});
			}
		},
		onAfterRendering: function () {
			var self = this;
			self.getView().getModel("localModel").setProperty("/Sap", {});
			self.getUsuario(self);
			self.getManifiestoArmado(self);
		},
		onRefresh: function () {
			var self = this;
			self.getView().byId("searchBuscar").setValue(null);
			sap.ui.core.BusyIndicator.show(0);
			Services.getoDataVManifiestoArmado(self, [], function (result) {
				util.response.validate(result, {
					success: function (data, message) {
						self.getView().getModel("localModel").setProperty("/VManifiestoArmado", data);
						self.onBuscarManifiesto();
						sap.ui.core.BusyIndicator.hide();
					},
					error: function (message) {
						self.getView().getModel("localModel").setProperty("/VManifiestoArmado", {});
					}
				});
			});

		},
		onArmarUA: function () {
			var idList = this.getView().byId("idListGuia");
			var items = idList.getSelectedItems();
			var itemsArray = [];
			for (var i in items) {
				var obj = items[i].getBindingContext("modelOdata").getObject();
				itemsArray.push(obj);
			}
			this.getView().getModel("maestroModel").setProperty("/DocumentoTransporte", itemsArray);
			util.controller.navTo(this, "appDetailArmadoUA", null);
		},
		onBuscarManifiesto: function () {
			//var selectKey = this.getView().getModel("localModel").getProperty("/BuscarManifiesto/selectCombo");
			var filter = [{
				key: "NumeroManifiesto",
				query: "Contains"
			}, {
				key: "NroViaje",
				query: "Contains"
			}];
			util.ui.onFiltrarTablaLive(this, "searchBuscar", "idListMaster", filter);
		},
		selectItemMaster: function (objList) {
			var self = this;
			if (!objList.DocumentosTransporte) {
				util.ui.messageBox("Este manifiesto no cuenta con documentos de transporte", 'e', function () {});
				return;
			}
			util.ui.loadingPage(self, true);
			var mensaje = "Desea Armar UA's del siguiente Manifiesto: " + objList.NumeroManifiesto;

			var iIdSede = self.getView().getModel("localModel").getProperty("/iIdSede");
			var oFilter = [];
			oFilter.push(new Filter("CodigoTabla", "EQ", 'T_CENTROS_CARGA'));
			oFilter.push(new Filter("IdPadre", "EQ", iIdSede));
			self.getCentroCarga(self, oFilter);
			self.getOrgVenta(self);
			self.getCanal(self, objList.CodVia ? objList.CodVia : objList.Via);
			//self.getSector(self);
			if (objList.EstadoCodigo === "TE_MANIF_01") {
				util.ui.messageBox(mensaje, 'c', function (bConfirmacion) {
					if (bConfirmacion) {
						/////

						var oResults = util.envioDatos.inicioTarja(objList);
						sap.ui.core.BusyIndicator.show(0);
						Services.inicioTarja(self, oResults, function (iResults) {
							sap.ui.core.BusyIndicator.hide();
							iResults.oAuditResponse.oResults = util.controller.unlink(iResults.oAuditResponse);
							util.response.validate(iResults.oAuditResponse, {
								success: function (data, message) {},
								/*error: function (message) {
									util.ui.onMessageErrorDialogPress(message);
								}*/
							});
						});
						//
						self.getView().byId("searchBuscar").setValue("");
						util.controller.navTo(self, "appDetailArmadoUA", null);
						self.getView().getModel("detailManifiesto").setData(objList);
					}
				});
			} else {
				//self.getView().getModel("miData").setProperty("/editable",false);
				self.getView().byId("searchBuscar").setValue("");
				util.controller.navTo(self, "appDetailArmadoUA", null);
				self.getView().getModel("detailManifiesto").setData(objList);
			}
			self.validBotonEditarUA(false);
			util.ui.loadingPage(self, false);
		},
		onListDetail: function (oEvent) {
			var self = this;

			var objList = util.controller.unlink(oEvent.getSource().getSelectedItem().getBindingContext("localModel").getObject());
			///////Inicio Validacion Nueva Roy 10-12-2019
			if (objList.CantidadDireccionamiento) {
				if (parseInt(objList.CantidadDireccionamiento) > 0) {
					var oFiltro = [];
					oFiltro.push(new Filter("IdManifiesto", "EQ", objList.Id));
					//oFiltro.push(new Filter("EstadoCodigo", "EQ", "CER_REC_TER"));
					sap.ui.core.BusyIndicator.show(0);
					Services.getoDataVRecepcionTraslado(self, oFiltro, function (result) {
						sap.ui.core.BusyIndicator.hide();
						util.response.validate(result, {
							success: function (data, message) {
								var modData = data;
								if (modData.length > 0) {
									for (var i in modData) {
										if (modData[i].EstadoCodigo !== "CER_REC_TER") {
											util.ui.messageBox("Pendiente de direccionamiento", 'e', function () {});
											return;
										}
									}
								} else {
									util.ui.messageBox("Pendiente de direccionamiento", 'e', function () {});
									return;
								}
								self.selectItemMaster(objList);
							},
							error: function (message) {

							}
						});
					});
				} else {
					self.selectItemMaster(objList);
				}
			}
			//////End Validacion Nueva Roy 10-12-2019
		},
		validBotonEditarUA: function (Editar) {
			var self = this;
			self.getView().getModel("localModel").setProperty("/ValidBotones", {});
			if (Editar) {
				self.getView().getModel("localModel").setProperty("/ValidBotones/CancelarEditar", true);
				self.getView().getModel("localModel").setProperty("/ValidBotones/EtiquetaUA", "Editar UA");
			} else {
				self.getView().getModel("localModel").setProperty("/ValidBotones/CancelarEditar", false);
				self.getView().getModel("localModel").setProperty("/ValidBotones/EtiquetaUA", "Armar UA");
			}
		}
	});
});