sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/UIComponent",
	"../util/util",
	'sap/ui/model/Filter',
	"../model/models",
	"../viewPass",
	"../services/Services",
	"../estructuras/Estructura"
], function (Controller, UIComponent, util, Filter, models, viewPass, Services, Estructura) {
	"use strict";
	return Controller.extend("saasa.armadoUA.controller.BaseController", {
		getUsuario: function (self) {
			var oResults = util.envioDatos.consultarSedesOrgVentas();
			sap.ui.core.BusyIndicator.show(0);
			Services.usuarioSesion(self, oResults, function (iResults) {
				sap.ui.core.BusyIndicator.hide();
				iResults.oAuditResponse.oResults = util.controller.unlink(iResults.oAuditResponse);
				util.response.validateUsuario(iResults.oAuditResponse, {
					success: function (data, message) {
						var oFilter = [];
						oFilter.push(new Filter("Id", "EQ", iResults.oResults[0].Sede));
						self.getCodigoSede(self, oFilter);
					},
					error: function (message) {
						util.ui.onMessageErrorDialogPress(message);
					}
				});
			});
		},
		getManifiestoArmado: function (self) {
			sap.ui.core.BusyIndicator.show(0);
			Services.getoDataVManifiestoArmado(self, [], function (result) {
				util.response.validate(result, {
					success: function (data, message) {
						self.getView().getModel("localModel").setSizeLimit(50000);
						self.getView().getModel("localModel").refresh(true);
						self.getView().getModel("localModel").setProperty("/VManifiestoArmado", data);
						sap.ui.core.BusyIndicator.hide();
					},
					error: function (message) {
						self.getView().getModel("localModel").setProperty("/VManifiestoArmado", {});
					}
				});
			});
		},
		getCodigoSede: function (self, oFilter) {
			Services.getoDataVGenericaCampo2(self, oFilter, function (result) {
				util.response.validate(result, {
					success: function (data, message) {
						self.getView().getModel("localModel").setProperty("/iIdSede", data[0].Id);
						var oFilter = [];
						oFilter.push(new Filter("CodigoTabla", "EQ", 'T_ALMACEN'));
						oFilter.push(new Filter("PadreCodigoSap", "EQ", data[0].CodigoSap));
						oFilter.push(new Filter("CodigoSap", "EQ", 'IMPO'));
						self.getAlmacenes(self, oFilter);
						var oFilter2 = [];
						oFilter2.push(new Filter("IdSede", "EQ", data[0].Id));
						self.getUbicacionInicial(self, oFilter2);
					},
					error: function (message) {
						//self.getView().getModel("miData").setProperty("/Sede", {});
					}
				});
			});
		},
		getAlmacenes: function (self, oFilter) {
			Services.getoDataVGenericaCampo(self, oFilter, function (result) {
				util.response.validate(result, {
					success: function (data, message) {
						self.getView().getModel("localModel").setProperty("/T_ALMACEN", data);
					},
					error: function (message) {
						self.getView().getModel("localModel").setProperty("/T_ALMACEN", {});
					}
				});
			});
		},
		getUbicacionInicial: function (self, oFilter) {
			Services.getoDataVUbicacionZonasRecepcionImportacion(self, oFilter, function (result) {
				util.response.validate(result, {
					success: function (data, message) {
						self.getView().getModel("localModel").setProperty("/T_UBICACION", data);
						//self.getView().getModel("miData").setProperty("/iIdUbicacionInicial", data[0].Id);
					},
					error: function (message) {
						//self.getView().getModel("miData").setProperty("/iIdUbicacionInicial", 0);
					}
				});
			});
		},
		getCentroCarga: function (self, oFilter) {
			Services.getoDataVGenericaCampo(self, oFilter, function (result) {
				util.response.validate(result, {
					success: function (data, message) {
						self.getView().getModel("Sap").setProperty("/iIdCentro", data[0].Campo);
						self.getView().getModel("Sap").setProperty("/ZdescSede", data[0].DescripcionCampo);
					},
					error: function (message) {
						self.getView().getModel("Sap").setProperty("/iIdCentro", "");
						self.getView().getModel("Sap").setProperty("/ZdescSede", "");
					}
				});
			});
		},
		getOrgVenta: function (self) {
			var oFilter = [];
			oFilter.push(new Filter("CodigoTabla", "EQ", 'T_ORGVENTAS'));
			Services.getoDataVGenericaCampo(self, oFilter, function (result) {
				util.response.validate(result, {
					success: function (data, message) {
						self.getView().getModel("Sap").setProperty("/iIdOrganizacionVenta", data[0].Campo);
						self.getView().getModel("Sap").setProperty("/ZdescOvta", data[0].DescripcionCampo);
					},
					error: function (message) {
						self.getView().getModel("Sap").setProperty("/iIdOrganizacionVenta", "");
						self.getView().getModel("Sap").setProperty("/ZdescOvta", "");
					}
				});
			});
		},
		getCanal: function (self, CodVia) {
			var oFilter = [];
			oFilter.push(new Filter("CodigoTabla", "EQ", 'T_CANAL_CARGA'));
			oFilter.push(new Filter("CodigoSap", "EQ", CodVia));
			Services.getoDataVGenericaCampo(self, oFilter, function (result) {
				util.response.validate(result, {
					success: function (data, message) {
						self.getView().getModel("Sap").setProperty("/iIdCanal", data[0].Campo);
						self.getView().getModel("Sap").setProperty("/ZdescCanal", data[0].DescripcionCampo);
					},
					error: function (message) {
						self.getView().getModel("Sap").setProperty("/iIdCanal", "");
						self.getView().getModel("Sap").setProperty("/ZdescCanal", "");
					}
				});
			});
		},
		getSector: function (self) {
			var oFilter = [];
			var DocTransporte = self.getView().byId("idListDocumentoTransporte").getSelectedItem().getBindingContext("modelOdata").getObject();
			self.getView().getModel("Sap").setProperty("/iIdSector", DocTransporte.SectorSAP);
			self.getView().getModel("Sap").setProperty("/ZdescSector", DocTransporte.SectorSAPDescripcion);
			/*oFilter.push(new Filter("CodigoTabla", "EQ", 'T_SECTOR_CARGA'));
			oFilter.push(new Filter("Campo", "EQ", sISector));
			Services.getoDataVGenericaCampo(self, oFilter, function (result) {
				util.response.validate(result, {
					success: function (data, message) {
						self.getView().getModel("Sap").setProperty("/iIdSector", data[0].CodigoSap);
						self.getView().getModel("Sap").setProperty("/ZdescSector", data[0].DescripcionCampo);
					},
					error: function (message) {
						self.getView().getModel("Sap").setProperty("/iIdSector", "");
						self.getView().getModel("Sap").setProperty("/ZdescSector", "");
					}
				});
			});*/
		},
		filterDocumentoTransporte: function (viewHome, idList, objList) {
			var documentosTransporte = objList.DocumentosTransporte;
			var modDocumentosTransporte = documentosTransporte && documentosTransporte != "" ? documentosTransporte.split(",") : [];
			modDocumentosTransporte = util.controller.removeDuplicates(modDocumentosTransporte);
			modDocumentosTransporte = modDocumentosTransporte.length === 0 ? ["undefined"] : modDocumentosTransporte;
			var objArray = [];
			for (var i in modDocumentosTransporte) {
				objArray.push(new Filter("Id", "EQ", modDocumentosTransporte[i]));
			}
			var viewList = viewHome.getView().byId(idList);
			if (viewList.getBinding("items")) {
				viewList.getBinding("items").filter(objArray);
			} else {
				viewList.getBinding("rows").filter(objArray);
			}
			this.cargarComboTablas();
		},
		filterCombo: function (view, idList, campoGet, campoSet) {
			var objArray = [];
			objArray.push(new Filter(campoGet, "EQ", campoSet));
			view.getView().byId(idList).getBinding("items").filter(objArray);
		},
		filter2Combo: function (view, idList, params, campoGet, campoSet) {
			var objArray = [];

			params.forEach(function (itemParam) {
				var campoGet = itemParam.campoGet;
				var campoSet = itemParam.campoSet;
				objArray.push(new Filter(campoGet, "EQ", campoSet));
			});

			view.getView().byId(idList).getBinding("items").filter(objArray);
		},
		filterComboUbicaciones: function (view, idList, campoGet1, campoSet1, campoGet2, campoSet2, addItem) {
			var self = this;
			var objArray = [];
			objArray.push(new Filter(campoGet1, "EQ", campoSet1));
			objArray.push(new Filter(campoGet2, "EQ", campoSet2));
			Services.getoDataVUbicacionDisponible(this, objArray, function (result) {
				if (result.iCode === 1) {
					if (addItem) {
						result.oResults.unshift(addItem);
					}
					self.getView().getModel("localModel").setProperty("/VUbicacionDisponibles", result.oResults);
				} else {
					if (addItem) {
						self.getView().getModel("localModel").setProperty("/VUbicacionDisponibles", [addItem]);
					} else {
						self.getView().getModel("localModel").setProperty("/VUbicacionDisponibles", []);
					}
				}

				if (addItem) {
					view.getView().byId(idList).setSelectedKey(addItem.Id);
				}
				self.getView().getModel("localModel").refresh();
			});
			//view.getView().byId(idList).getBinding("items").filter(objArray);
		},
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		defaultTables: function () {
			this.getView().getModel("miData").setProperty("/AddManifiesto", {});
		},
		cargarComboTablas: function () {
			var self = this;
			///End Inicio Combo///
			var oFiltro = [];
			//Combo Documento Transporte
			//oFiltro.push(new Filter("CodigoTabla", "EQ", "T_MOD_RECEP"));
			//oFiltro.push(new Filter("CodigoTabla", "EQ", "T_TERMINALES"));
			//oFiltro.push(new Filter("CodigoTabla", "EQ", "T_AEROPUERTOS"));
			oFiltro.push(new Filter("CodigoTabla", "EQ", "T_TIPO_ALMACEN"));
			oFiltro.push(new Filter("CodigoTabla", "EQ", "T_TIPO_CARGA"));
			//oFiltro.push(new Filter("CodigoTabla", "EQ", "T_REGIMENES"));
			oFiltro.push(new Filter("CodigoTabla", "EQ", "T_SEDES"));
			//oFiltro.push(new Filter("CodigoTabla", "EQ", "T_ALMACEN"));
			oFiltro.push(new Filter("CodigoTabla", "EQ", "T_BALANZA"));
			oFiltro.push(new Filter("CodigoTabla", "EQ", "T_TIPO_DOC_ADJUNTO"));

			Services.getoDataVGenericaCampo(self, oFiltro, function (result) {
				util.response.validate(result, {
					success: function (data, message) {
						Estructura.maestro.mapVGenericaCampoComboBox(self, data);
					},
					error: function (message) {}
				});
			});
			///End Maestros Combo///
		},
		getDocumentosTransporteManifiestos: function (objList) {
			var self = this;
			var oFiltro = [];
			var documentosTransporte = objList.DocumentosTransporte;
			var modDocumentosTransporte = documentosTransporte && documentosTransporte != "" ? documentosTransporte.split(",") : [];
			for (var j in modDocumentosTransporte) {
				oFiltro.push(new Filter("Id", "EQ", modDocumentosTransporte[j]));
			}
			if (modDocumentosTransporte.length > 0) {
				//getOdataDocTransporteVolante
				Services.getOdataDocTransporteBultos(self, oFiltro, function (result) {
					util.response.validate(result, {
						success: function (data, message) {
							//var mapData = util.controller.unlink(Estructura.detail.mapoDataDocumentosTransporte(self,data));
							/*var nBultosMalEstado = mapData.filter(d => d.BultosMalEstado > 0)
							if(nBultosMalEstado.length>0){
								self.getView().getModel("localModel").setProperty("/MostrarTab", true);
							}
							else{
								self.getView().getModel("localModel").setProperty("/MostrarTab", false);
							}*/
							self.getView().getModel("localModel").setProperty("/MostrarTab", false);
							for (var i in data) {
								var pesoManifestado = data[i].PesoManifestado;
								var bultoManifestado = data[i].BultosManifestados;

								var pesoRecibido = data[i].PesoRecibido;
								var bultoRecibido = data[i].BultosRecibidos;
								var bultosMalEstado = data[i].BultosMalEstado;
								if (parseInt(pesoRecibido) > parseInt(pesoManifestado)) {
									data[i].clasePeso = "rojo";
								} else {
									data[i].clasePeso = "negro";
								}
								////////////////////////////////////////////
								////////////////////////////////////////////
								if (bultoRecibido > bultoManifestado) {
									data[i].claseBulto = "rojo";
								} else {
									data[i].claseBulto = "negro";
								}

								if (bultosMalEstado) {
									data[i].visibleIncidencia = true;
								} else {
									data[i].visibleIncidencia = false;
								}
								data[i].FechaIngresoDT = data[i].FechaIngresoDT ? data[i].FechaIngresoDT : new Date();
							}
							self.getView().getModel("localModel").setProperty("/DocumentoTransporteResumen", data);
						},
						error: function (message) {
							self.getView().getModel("localModel").setProperty("/DocumentoTransporteResumen", []);
						}
					});
				});
			}
		},
	});
});