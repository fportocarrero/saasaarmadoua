sap.ui.define([
	"./utilUI"
], function(utilUI) {
    "use strict";
    return {
    	iCodeSuccess	: 1,
    	iCodeWarn		: 2,
    	iCodeError		: -1,
    	iCodeException	: -2,
        success: function(sIdTransaction, sMessage, oResults) {
            return {
                iCode			: this.iCodeSuccess,
                sIdTransaction  : sIdTransaction,
                sMessage		: sMessage,
                oResults		: oResults
            };
        },
        warn: function(sIdTransaction, sMessage,oResults) {
            return {
                iCode		: this.iCodeWarn,
                sIdTransaction  : sIdTransaction,
                sMessage	: sMessage,
                oResults	: oResults
            };
        },
        error: function(sIdTransaction, sMessage,oResults) {
            return {
                iCode		: this.iCodeError,
                sIdTransaction  : sIdTransaction,
                sMessage	: sMessage,
                oResults	: oResults
            };
        },
        exception: function(sIdTransaction, sMessage) {
            return {
                iCode		: this.iCodeException,
                sIdTransaction  : sIdTransaction,
                sMessage	: sMessage
            };
        },
        validate: function(result,events){
        	if(result.iCode === 1){
        		events.success(result.oResults,result.sMessage);
        	}else{
        		utilUI.onMessageErrorDialogPress(result.sIdTransaction, result.iCode,result.sMessage);
        		//events.error(result.sMessage);
        	}
        },
        validateAdjunto: function(result,events){
        	var valid = result.oAuditResponse;
        	if(valid.iCode === 1){
        		events.success(result.oResults,valid.sMessage);
        	}else{
        		utilUI.onMessageErrorDialogPress(valid.sIdTransaction, valid.iCode,valid.sMessage);
        		events.error(result.sMessage);
        	}
        },
        validateUsuario: function(result,events){
        	if(result.iCode === 1){
        		events.success(result.oResults,result.sMessage);
        	}
        	else if(result.iCode === 2){
        		utilUI.onMessageErrorDialogPress(result.sIdTransaction, result.iCode,'Este usuario no tiene sede asignada');
        		//events.error(result.sMessage);
        	}
        	else{
        		utilUI.onMessageErrorDialogPress(result.sIdTransaction, result.iCode,result.sMessage);
        		//events.error(result.sMessage);
        	}
        }
    };
});