sap.ui.define([
	"../util/utilResponse",
	"../util/utilHttp",
	"../constantes",
	"../model/mockdata",
	"../estructuras/Estructura"
], function (utilResponse, utilHttp, constantes, mockdata, Estructura) {
	"use strict";
	return {

		getoDataVManifiesto: function (context, oFilter, callback) {
			utilHttp.getoData(context, '/VManifiesto', oFilter, callback); //callback(modLocal) --> mockData
		},
		getoDataVUnidadAlmacenamiento: function (context, oFilter, callback) {
			utilHttp.getoData(context, '/VUnidadAlmacenamientoArmadoImpo', oFilter, callback); //callback(modLocal) --> mockData
		},
		getoDataVNumeroIncidencia: function (context, oFilter, callback) {
			utilHttp.getoData(context, '/VNumeroIncidencia', oFilter, callback); //callback(modLocal) --> mockData
		},
		getOdataDocTransporteVolante: function (context, oFilter, callback) {
			utilHttp.getoData(context, '/VDocumentoTransporteManifiestoVolante', oFilter, callback); //callback(modLocal) --> mockData
		},
		getOdataDocTransporteBultos: function (context, oFilter, callback) {
			utilHttp.getoData(context, '/VDocumentoTransporteBultos', oFilter, callback); //callback(modLocal) --> mockData
		},
		getOdataDocTransporteManifiestos: function (context, oFilter, callback) {
			utilHttp.getoData(context, '/VDocumentoTransporteManifiestos', oFilter, callback); //callback(modLocal) --> mockData
		},
		getoDataVGenericaCampo: function (context, oFilter, callback) {
			utilHttp.getoData(context, '/VGenericaCampoMaestro', oFilter, callback); //callback(modLocal) --> mockData
		},
		getoDataVGenericaCampo2: function (context, oFilter, callback) {
			utilHttp.getoData(context, '/VGenericaCampo', oFilter, callback); //callback(modLocal) --> mockData
		},
		getoDataVUbicacionZonasRecepcionImportacion: function (context, oFilter, callback) {
			utilHttp.getoData(context, '/VUbicacionZonasRecepcionImportacion', oFilter, callback); //callback(modLocal) --> mockData
		},
		getoDataVUbicacionDisponible: function (context, oFilter, callback) {
			utilHttp.getoData(context, '/VUbicacionDisponibles', oFilter, callback); //callback(modLocal) --> mockData
		},
		getOdataAdjuntos: function (context, oFilter, callback) {
			utilHttp.getoData(context, '/VManifiestoAdjunto', oFilter, callback); //callback(modLocal) --> mockData
		},
		///////////////////////////////////////////////////////////////////////////////////
		usuarioSesion: function (context, oResults, callback) {
			utilHttp.Post(constantes.services.usuarioSesion, oResults, callback, context);
		},
		calcularPeso: function (context, oResults, callback) {
			utilHttp.Post(constantes.services.calcularPeso, oResults, callback, context);
		},
		consultarSedesOrgVentas: function (context, oResults, callback) {
			utilHttp.Post(constantes.services.consultarSedesOrgVentas, oResults, callback, context);
		},
		consultarCanalesSectores: function (context, oResults, callback) {
			utilHttp.Post(constantes.services.consultarCanalesSectores, oResults, callback, context);
		},
		consultarConceptosFacturables: function (context, oResults, callback) {
			utilHttp.Post(constantes.services.consultarConceptosFacturables, oResults, callback, context);
		},
		armarUA: function (context, oResults, callback) {
			utilHttp.Post(constantes.services.armarUA, oResults, callback, context);
		},
		inicioTarja: function (context, oResults, callback) {
			utilHttp.Post(constantes.services.inicioTarja, oResults, callback, context);
		},
		registrarResumen: function (context, oResults, callback) {
			utilHttp.Post(constantes.services.registrarResumen, oResults, callback, context);
		},
		eliminarUA: function (context, oResults, callback) {
			utilHttp.Post(constantes.services.eliminarUA, oResults, callback, context);
		},
		getoDataVManifiestoArmado: function (context, oFilter, callback) {
			utilHttp.getoData(context, '/VManifiestoArmado', oFilter, callback); //callback(modLocal) --> mockData
		},
		getoDataVRecepcionTraslado: function (context, oFilter, callback) {
			utilHttp.getoData(context, '/VRecepcionTraslado', oFilter, callback); //callback(modLocal) --> mockData
		},
		////////////////////////////////////////////////////////////////////////
		Adjunto: function (context, oResults, callback) {
			utilHttp.Post(constantes.services.Adjunto, oResults, callback, context);
		},
		eliminarAdjunto: function (context, oResults, callback) {
			utilHttp.Post(constantes.services.eliminarAdjunto, oResults, callback, context);
		}
		//////////////////////////////////////////////////////////////////////////////
	};
});