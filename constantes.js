/* global moment:true */
sap.ui.define([
    
], function() {
    "use strict";
    return {
    	idProyecto: "saasa.armadoUA.",
        PaginaHome: "appHome",
        IdApp:"ArmadoUAImportacion",
        modelOdata:"modelOdata",
        root:"/SaasaService",
        userApi:"userapi",
        services: {
        	usuarioSesion:"/Usuario/Sesion",
        	calcularPeso: "/Carga/Service/BalanzaService/PesoCarga",
        	armarUA: "/Carga/Service/RegistrarUA",
        	registrarResumen: "/Carga/Service/FinTarjaManifiesto",
        	eliminarUA: "/Carga/Service/EliminarUA",
        	inicioTarja:"/Carga/Service/InicioTarjaManifiesto",
        	consultarSedesOrgVentas:"/Api/Sap/ConsultarSedesOrgVentas/",
        	consultarCanalesSectores:"/Api/Sap/ConsultarCanalesSectores/",
        	consultarConceptosFacturables:"/Api/Sap/ConsultarConceptosFacturables/",
        	
			Adjunto: "/Carga/Service/Adjunto/",
			registrarAdjunto: "/Carga/Service/RegistrarAdjunto/",
			actualizarAdjunto: "/Carga/Service/ActualizarAdjunto/",
			eliminarAdjunto: "/Carga/Service/EliminarAdjunto/",
		}
    };
});