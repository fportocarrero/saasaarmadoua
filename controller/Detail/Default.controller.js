sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/UIComponent",
	"../../util/util",
	"../../viewPass"
], function (Controller,UIComponent,util,viewPass) {
	"use strict";
	return Controller.extend("saasa.armadoUA.controller.Detail.Default", {
		onInit: function () {
			var oRouter = UIComponent.getRouterFor(this);
            oRouter.attachRoutePatternMatched(this.onRouteMatched, this);
		},
		onRouteMatched: function (oEvent) {
			if (oEvent.getParameter("name") === "appDefault") {	
			}
		}
	});
});