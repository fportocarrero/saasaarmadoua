sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"sap/m/MessageToast",
	"../constantes",
	"./util"
], function (JSONModel, History, MessageToast, constantes, util) {
	"use strict";
	return {
		initModelView: function (controller) {
			controller.getView().setModel(new JSONModel({}));
		},
		removeSelectionTableUI:function(self,idTable){
			self.getView().byId(idTable).clearSelection();
		},
		removeSelectionTable:function(self,idTable){
			self.getView().byId(idTable).removeSelections(true);
		},
		leftNumberUndefined:function(valor){
			if(!valor){valor ="0";}else{valor = valor.toString();}
			return valor;
		},
		itemsSelectedTable: function (self, idTable, model, table, event) {
			var tabla = self.getView().byId(idTable);
			var itemsTabla = tabla.getSelectedIndices().length;
			var aItems = [];
			if (itemsTabla > 0) {
				for (var i = itemsTabla - 1; i >= 0; i--) {
					var item = tabla.getSelectedIndices()[i];
					var obj = self.getView().getModel(model).getProperty(table + "/" + item);
					aItems.push(obj);
					//self.removeSelection(idTable);
				}
				event.Seleccionados(aItems);
			} else {
				MessageToast.show('No ha seleccionado ningun item', {
					duration: 3000
				});
				return;
			}
		},
		removeDuplicates: function (originalArray) {
			var newArray = [];
			var lookupObject = {};

			for (var i in originalArray) {
				lookupObject[originalArray[i]] = originalArray[i];
			}

			for (i in lookupObject) {
				newArray.push(lookupObject[i]);
			}
			return newArray;
		},
		formatterComboBox: function (self, campo, dato, newCampo, nameModel, nametable) {
			var modData = "";
			try {
				if (dato) {
					var table = self.getView().getModel(nameModel).getProperty(nametable);
					var findObject = table.find(function (el) {
						return el[campo] == dato;
					});
					modData = findObject[newCampo];
				}
			} catch (e) {}
			return modData;
		},
		pad: function (n, length) { //Añadir ceros a la izquierda, length tamaño final que debe tener
			var n = n.toString();
			while (n.length < length) {
				n = "0" + n;
			}
			return n;
		},
		isObject: function (a) {
			return (!!a) && (a.constructor === Object);
		},
		formatFechaDDMMAAAAHora: function (date) {
			var fecha = this.pad(date.getDate(), 2) + "/" + this.pad((date.getMonth() + 1), 2) + "/" + this.pad(date.getFullYear(), 4) + " " +
				this.pad(date.getHours(), 2) + ":" + this.pad(date.getMinutes(), 2) + ":" + this.pad(date.getSeconds(), 2);
			return fecha;
		},
		formatFechaDDMMAAAA: function (date) {
			var fecha = this.pad(date.getDate(), 2) + "/" + this.pad((date.getMonth() + 1), 2) + "/" + this.pad(date.getFullYear(), 4);
			return fecha;
		},
		formatFechaDate: function (date) {
			var iFecha = date.split("/");
			var fecha = new Date(iFecha[2].split(" ")[0] + "/" + iFecha[1] + "/" + iFecha[0] + " " + iFecha[2].split(" ")[1]);
			return fecha;
		},
		btnMenosTable: function (self, idTable, model, table, event) {
			var tabla = self.getView().byId(idTable);
			var itemsTabla = tabla.getSelectedIndices().length;
			var aItems = [];
			if (itemsTabla > 0) {
				for (var i = itemsTabla - 1; i >= 0; i--) {
					var item = tabla.getSelectedIndices()[i];
					var obj = self.getView().getModel(model).getProperty(table + "/" + item);
					aItems.push(obj);
					//self.removeSelection(idTable);
				}
				event.Eliminar(aItems);
			} else {
				MessageToast.show('No ha seleccionado ningun item', {
					duration: 3000
				});
				return;
			}
		},
		btnMasTable: function (self, model, table) {
			var contenedor = self.getView().getModel(model).getProperty(table);
			contenedor = contenedor ? contenedor : [];
			if (jQuery.isEmptyObject(contenedor)) {
				contenedor = [{
					enabledInput: true,
					icon: "sap-icon://save",
					anterior: {}
				}];
			} else {
				var ultimoObj = contenedor[contenedor.length - 1];
				if (ultimoObj.anterior && jQuery.isEmptyObject(ultimoObj.anterior)) {
					return;
				}
				for (var i in contenedor) {
					contenedor[i].icon = "sap-icon://edit";
					contenedor[i].enabledInput = false;
				}
				contenedor.push({
					enabledInput: true,
					icon: "sap-icon://save",
					anterior: {}
				});

			}
			self.getView().getModel(model).setProperty(table, contenedor);
		},
		changeEditSaveRowTable: function (self, oEvent, model, campoAnterior, campoCondicion, campoIcon, campoEnabled, event) {
			var objDepend = oEvent.getSource().getBindingContext(model).getObject();
			var obj = this.unlink(objDepend);
			var path = oEvent.getSource().getBindingContext(model).getPath();
			if (obj.icon === "sap-icon://edit") {
				var contenedor = this.unlink(self.getView().getModel(model).getProperty(path));
				self.getView().getModel(model).setProperty(path + "/" + campoAnterior, contenedor);
				self.getView().getModel(model).setProperty(path + "/" + campoIcon, "sap-icon://save");
				self.getView().getModel(model).setProperty(path + "/" + campoEnabled, true);
				self.getView().getModel(model).refresh(true);
			} else if (obj.icon === "sap-icon://save") {
				obj.path = path;
				if (obj.Id && obj.Id != "") {
					////Editar
					event.Editar(obj);
				} else {
					////Registrar
					event.Registrar(obj);
				}
				////
			}
		},
		changeCancelRowTable: function (self, oEvent, model, table, campoAnterior, campoIcon, campoEnabled) {
			var objDepend = oEvent.getSource().getBindingContext(model).getObject();
			var obj = this.unlink(objDepend);
			var path = oEvent.getSource().getBindingContext(model).getPath();
			if (obj.icon === "sap-icon://save") {
				if (obj.Id && obj.Id != "") {
					////Editar
					var objAnterior = obj[campoAnterior];
					self.getView().getModel(model).setProperty(path, objAnterior);
					self.getView().getModel(model).setProperty(path + "/" + campoIcon, "sap-icon://edit");
					self.getView().getModel(model).setProperty(path + "/" + campoEnabled, false);
				} else {
					////Registrar
					var contenedor = this.unlink(self.getView().getModel(model).getProperty(table));
					contenedor.pop();
					self.getView().getModel(model).setProperty(table, contenedor);
				}
				self.getView().getModel(model).refresh(true);
			}
		},

		btnMenosTable2: function (self, idTable, model, table) {
			var tabla = self.getView().byId(idTable);
			var listTotalGuia = self.getView().getModel(model).getProperty(table);
			var itemsTabla = tabla.getSelectedIndices().length;
			if (itemsTabla > 0) {
				for (var i = itemsTabla - 1; i >= 0; i--) {
					var item = tabla.getSelectedIndices()[i];
					listTotalGuia.splice(item, 1);
					self.getView().getModel(model).setProperty(table, listTotalGuia);
					self.removeSelection(idTable);
				}
				MessageToast.show('Se ha(n) Eliminado el(los) item(s) seleccionado(s)', {
					duration: 3000
				});
				return;
			} else {
				MessageToast.show('No ha seleccionado ningun item', {
					duration: 3000
				});
				return;
			}
		},
		changeEditSaveRowTable2: function (self, oEvent, model, campoAnterior, campoCondicion, campoIcon, campoEnabled, event) {
			var objDepend = oEvent.getSource().getBindingContext(model).getObject();
			var obj = this.unlink(objDepend);
			var path = oEvent.getSource().getBindingContext(model).getPath();
			if (obj.icon === "sap-icon://edit") {
				////Editar
				var contenedor = this.unlink(self.getView().getModel(model).getProperty(path));
				self.getView().getModel(model).setProperty(path + "/" + campoAnterior, contenedor);
				self.getView().getModel(model).setProperty(path + "/" + campoIcon, "sap-icon://save");
				self.getView().getModel(model).setProperty(path + "/" + campoEnabled, true);
				////
			} else if (obj.icon === "sap-icon://save") {
				////Registrar
				var objAnterior = obj[campoAnterior];
				var objAnteriorIsEmpty = jQuery.isEmptyObject(objAnterior);
				if (obj[campoCondicion] === undefined || obj[campoCondicion] === "") { //objAnteriorIsEmpty && (obj[campoCondicion] === undefined)
					MessageToast.show("Por favor llene correctamente los campos.");
				} else {
					self.getView().getModel(model).setProperty(path + "/" + campoIcon, "sap-icon://edit");
					self.getView().getModel(model).setProperty(path + "/" + campoEnabled, false);
					self.getView().getModel(model).setProperty(path + "/" + campoAnterior, contenedor);
				}
				////
			}
		},
		changeCancelRowTable2: function (self, oEvent, model, table, campoAnterior, campoIcon, campoEnabled) {
			var objDepend = oEvent.getSource().getBindingContext(model).getObject();
			var obj = this.unlink(objDepend);
			var path = oEvent.getSource().getBindingContext(model).getPath();
			if (obj.icon === "sap-icon://edit") {
				////
				self.getView().getModel(model).setProperty(path + "/" + campoIcon, "sap-icon://save");
				self.getView().getModel(model).setProperty(path + "/" + campoEnabled, true);
				////
			} else if (obj.icon === "sap-icon://save") {
				////
				var objAnterior = obj[campoAnterior];
				self.getView().getModel(model).setProperty(path + "/" + campoIcon, "sap-icon://edit");
				self.getView().getModel(model).setProperty(path + "/" + campoEnabled, false);
				self.getView().getModel(model).setProperty(path, objAnterior);
				var objAnteriorIsEmpty = jQuery.isEmptyObject(objAnterior);
				if (objAnteriorIsEmpty) {
					var contenedor = this.unlink(self.getView().getModel(model).getProperty(table));
					contenedor.pop();
					self.getView().getModel(model).setProperty(table, contenedor);
				}
				////
			}
		},
		unlink: function (object) {
			return JSON.parse(JSON.stringify(object));
		},
		createDialog: function (self, idFrag, rutaFrag) {
			try {
				sap.ui.getCore().byId(idFrag).destroy();
			} catch (e) {}
			if (!sap.ui.getCore().byId(idFrag)) {
				self[idFrag] = sap.ui.xmlfragment(
					constantes.idProyecto + rutaFrag,
					self.getView().getController() // associate controller with the fragment            
				);
				self.getView().addDependent(self[idFrag]);
			}
			sap.ui.getCore().byId(idFrag).open();
			sap.ui.core.BusyIndicator.hide();
		},
		createPage: function (splitId, viewId, rutaController) {
			var viewDefault = $.Home.getView().byId(viewId).getId();
			$.Home.getView().byId(splitId).to($.Home.createId(viewDefault));
			//this.callController(rutaController).onRouteMatched();
			/*$.Home.getView().byId(splitId).setMode("ShowMode");
			$.Home.getView().byId(splitId).setMode("HideMode");*/
		},
		callController: function (rutaController) {
			return sap.ui.controller(constantes.idProyecto + ".controller." + rutaController);
		},
		controllerFromView: function (controller, id) {
			var idController = this.byId(controller, id).getId();
			return sap.ui.getCore().byId(idController).getController();
		},
		createPageMaster: function (self, splitId, viewId) {
			var viewDefault = self.getView().byId(viewId).getId();
			self.getView().byId(splitId).toMaster(self.createId(viewDefault));
		},
		getControllerView: function (nameController) {
			if (!this.controllerView) {
				this.controllerView = sap.ui.getCore().byId(sap.ui.controller(constantes.idProyecto + nameController).getId())
					.getController();
			}
			return this.controllerView;
		},
		property: function (controller, root, data, bRefresh) {
			if (data) {
				controller.getView().getModel().setProperty(root, data);
				if (bRefresh === true) {
					this.refreshModel(controller);
				}
				return null;
			} else {
				return controller.getView().getModel().getProperty(root);
			}
		},
		refreshModel: function (controller) {
			controller.getView().getModel().refresh();
		},
		byId: function (controller, id) {
			return controller.getView().byId(id);
		},
		navToPage: function (self, page) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(self);
			oRouter.navTo(page);
			this.getControllerView(page);
		},
		navTo: function (controller, page, data) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(controller);
			//oRouter.navTo(null, data ? data : null, true);
			oRouter.navTo(page, data ? data : null, false);
		},
		navToMaster: function (controller, nameSplit, nameMaster) {
			controller.getView().byId(nameSplit).toMaster(controller.createId(nameMaster));
		},
		navToBackMaster : function(controller, nameSplit) {
			controller.getView().byId(nameSplit).backMaster();
		},
		navBack: function (controller) {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(controller);
				oRouter.navTo(constantes.PaginaHome, true);
			}
		},
		filtroInicialFechasOC: function (self) {
			// Setea los Date Picker de OC a los ultimos 3 meses
			var date = new Date(new Date().getFullYear(), 0, 1);
			var date2 = new Date();
			var dpFin = self.getView().byId("dpFin");
			dpFin.setDateValue(date2);
			var dpIni = self.getView().byId("dpIni");
			dpIni.setDateValue(date);
		},
		filtroInicialFechasComprobantes: function (self) {
			// Setea los Date Picker de Comprobantes a los ultimos 3 meses
			var dateC = new Date(new Date().getFullYear(), 0, 1);
			var dateC2 = new Date();
			var dpFinC = self.getView().byId("dpFinDOA");
			dpFinC.setDateValue(dateC2);
			var dpIniC = self.getView().byId("dpIniDOA");
			dpIniC.setDateValue(dateC);
		},
		getParameter: function (self, oEvent, nameParam, nameView, data) {
			var parametro = oEvent.mParameters ? oEvent.getParameter("arguments")[nameParam] : null;
			if (!parametro) {
				if (nameView) {
					this.navTo(self, nameView, data);
				}
			}
			return parametro;
		},
		valueUndefinedPDF: function (valor) {
			var newValor = valor;
			if (!valor) {
				newValor = "";
			}
			if (newValor.includes) {
				if (newValor.includes("XNA") || newValor.includes("NO APLICA")) {
					newValor = "";
				}
			}
			return newValor;
		},
		leftNumberUndefined: function (valor) {
			if (!valor) {
				valor = "0";
			} else {
				valor = valor.toString();
			}
			return valor;
		},
	};
});