sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	'sap/m/Button',
	'sap/m/Dialog',
	'sap/m/Text',
	"sap/ui/layout/VerticalLayout",
	"sap/ui/core/routing/History",
	'sap/ui/model/Filter',
	"./utilController",
	"../dataPass",
	"../estructuras/Estructura",
	"../lib/QR/qrcode",
	"../lib/JBar/JsBarCode",
	"../lib/Excel/base64",
	"../lib/Excel/xlsx.min",
	"../lib/jsPDF/jspdf",
	"./utilFormatter"
], function (JSONModel, MessageBox, MessageToast, Button, Dialog, Text, VerticalLayout, History, Filter, utilController, dataPass,
	Estructura, qrcode, JsBarCode, base64util, xslsutil, jspdfutil, utilFormatter) {
	"use strict";
	return {
		loadingPage: function (self, valor) {
			self.getView().getModel("ui").setProperty("/loadingPage", valor);
			self.getView().getModel("ui").refresh();
		},
		validarEmail: function (email) {
			var rexMail = /^\w+[\w-+\.]*\@\w+([-\.]\w+)*\.[a-zA-Z]{2,}$/; ///^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
			var indicador = "";
			if (!email.match(rexMail)) {
				indicador = false;
			} else {
				indicador = true;
			}
			return indicador;
		},
		onFiltrarTablaLive: function (self, idInput, idTabla, fields) {
			var oView = self.getView();
			var Input = oView.byId(idInput);
			var oTable = oView.byId(idTabla);
			var valor = Input.getValue();
			this.filterSearchBoxList(oTable, valor, fields);
		},
		filterSearchBoxList: function (tablaLista, valor, params) {
			var oBinding = tablaLista.mBindingInfos.items.binding; //tablaLista.getBinding();
			// apply filters to binding
			var aFilters = [];
			params.forEach(function (item) {
				aFilters.push(new Filter(item.key, item.query, valor));
			});
			var filter = new Filter({
				filters: aFilters,
				and: false
			});
			oBinding.filter(filter);
		},
		filterSearchBoxList2: function (tablaLista, valor, params) {
			var oBinding = tablaLista.getBinding();
			// apply filters to binding
			var aFilters = [];
			params.forEach(function (item) {
				aFilters.push(new Filter(item.key, item.query, valor));
			});
			var filter = new Filter({
				filters: aFilters,
				and: false
			});
			oBinding.filter(filter);
		},
		FiltrarCampos2: function (self, modelo, tabla, arrayFiltros, nuevoModel, resolve, reject) {
			var oDataModel = self.getView().getModel(modelo);
			//new Filter("CodigoTabla", "EQ", "tipo_usuario");
			var onSuccess = function (oDataModel) {
				var results = oDataModel.results;
				var filtrado = new JSONModel(results);
				self.getView().setModel(filtrado, nuevoModel);
				self.getView().getModel(nuevoModel);
				self.getView().getModel(nuevoModel).refresh();
				resolve(results);
				sap.ui.core.BusyIndicator.hide();

			};
			//Handler en caso de error
			var onError = function (error) {
				jQuery.sap.log.info("--cargaTablaClasePedido error--", error);
				reject();
				sap.ui.core.BusyIndicator.hide();
			};
			var reqHeaders = {
				filters: arrayFiltros,
				context: this, // mention the context you want
				success: onSuccess, // success call back method
				error: onError, // error call back method
				async: false // flage for async true
			};

			oDataModel.read("/" + tabla, reqHeaders);
		},
		FiltrarCampos: function (self, modelo, tabla, arrayFiltros, nuevoModel, nuevoTabla) {
			var oDataModel = self.getView().getModel(modelo);
			//new Filter("CodigoTabla", "EQ", "tipo_usuario");
			var onSuccess = function (oDataModel) {
				var results = oDataModel.results;
				self.getView().getModel(nuevoModel).setProperty("/" + nuevoTabla, results);
				self.getView().getModel(nuevoModel).refresh();
				sap.ui.core.BusyIndicator.hide();
			};
			//Handler en caso de error
			var onError = function (error) {
				jQuery.sap.log.info("--cargaTablaClasePedido error--", error);
				sap.ui.core.BusyIndicator.hide();
			};
			var reqHeaders = {
				filters: arrayFiltros,
				context: this, // mention the context you want
				success: onSuccess, // success call back method
				error: onError, // error call back method
				async: false // flage for async true
			};
			oDataModel.read("/" + tabla, reqHeaders);
		},
		onMensajeGeneral: function (self, mensaje) {
			var dialog = new sap.m.Dialog({
				title: 'Mensaje',
				type: 'Message',
				content: new sap.m.Text({
					text: mensaje
				}),
				beginButton: new sap.m.Button({
					text: 'OK',
					press: function () {
						dialog.close();
					}.bind(self)
				}),
				afterClose: function () {
					dialog.destroy();
				}
			});

			dialog.open();
		},
		onMensajeGeneralPress: function (self, mensaje, event) {
			var dialog = new sap.m.Dialog({
				title: 'Mensaje',
				type: 'Message',
				content: new sap.m.Text({
					text: mensaje
				}),
				beginButton: new sap.m.Button({
					text: 'OK',
					press: function () {
						dialog.close();
						utilController.navTo(self, "appDetail", null);
					}.bind(self)
				}),
				afterClose: function () {
					dialog.destroy();
				}
			});

			dialog.open();
		},
		onMensajeAdvertenciaPress: function (self, mensaje, event) {
			var dialog = new sap.m.Dialog({
				title: 'Mensaje',
				type: 'Message',
				content: new sap.m.Text({
					text: mensaje
				}),
				beginButton: new sap.m.Button({
					text: 'OK',
					press: function () {
						dialog.close();
					}.bind(self)
				}),
				afterClose: function () {
					dialog.destroy();
				}
			});

			dialog.open();
		},
		onMessageErrorDialogPress: function (idTransaccion,codigo, sMessage) {
			var mensaje = '';
			if(codigo>0){
				mensaje=sMessage;
			}
			else{
				mensaje = 'Ocurrió un error en el servicio';
			}
			var that = this;
			var dialog = new sap.m.Dialog({
				//id: 'dglExitoso',
				title: 'Error',
				type: 'Message',
				state: 'Error',
				contentWidth: '25rem',
				content: new sap.ui.layout.VerticalLayout({
					//id: 'idVertical',
					content: [new sap.m.Text({
							text: mensaje + '\n' + '\n',
							textAlign: 'Center'
								//id: 'txtMensaje'
						}),
						new sap.m.Input({
							value: idTransaccion,
							enabled: false,
							//id: 'inputTranscaccion',
							class: 'tamaniotexto'
						})
					]
				}),
				beginButton: new sap.m.Button({
					icon: 'sap-icon://copy',
					//id: 'btnidTransaccion',
					tooltip: 'Copiar código de transaccion al portapapeles',
					press: function () {
						that.copyToClipboard(idTransaccion);
					}
				}),
				endButton: new sap.m.Button({
					text: 'OK',
					press: function () {
						dialog.destroy();
					}
				}),
				afterClose: function () {
					dialog.destroy();
				}
			});
			dialog.open();
		},

		onMessageWarningDialogPress: function (idTransaccion, mensaje) {
			var that = this;
			var dialog = new Dialog({
				//id: 'dglExitoso',
				title: 'Advertencia',
				type: 'Message',
				state: 'Warning',
				contentWidth: '25rem',
				content: new sap.ui.layout.VerticalLayout({
					//id: 'idVertical',
					content: [new sap.m.Text({
							text: mensaje + '\n' + '\n',
							textAlign: 'Center'
								//id: 'txtMensaje'
						}),
						new sap.m.Input({
							value: idTransaccion,
							enabled: false,
							//id: 'inputTranscaccion',
							class: 'tamaniotexto'
						})
					]
				}),
				beginButton: new sap.m.Button({
					visible: false,
					icon: 'sap-icon://copy',
					//id: 'btnidTransaccion',
					tooltip: 'Copiar código de transaccion al portapapeles',
					press: function () {
						that.copyToClipboard(idTransaccion);
					}
				}),
				endButton: new sap.m.Button({
					text: 'OK',
					press: function () {
						dialog.destroy();
					}
				}),
				afterClose: function () {
					dialog.destroy();
				}
			});

			dialog.open();
		},

		onMessageWarningDialogPressExit: function (idTransaccion, mensaje) {
			var that = this;
			var dialog = new sap.m.Dialog({
				//id: 'dglExitoso',
				title: 'Advertencia',
				type: 'Message',
				state: 'Warning',
				contentWidth: '25rem',
				content: new sap.ui.layout.VerticalLayout({
					//id: 'idVertical',
					content: [new sap.m.Text({
							text: mensaje + '\n' + '\n',
							textAlign: 'Center'
								//id: 'txtMensaje'
						}),
						new sap.m.Input({
							value: idTransaccion,
							enabled: false,
							//id: 'inputTranscaccion',
							class: 'tamaniotexto'
						})
					]
				}),
				beginButton: new sap.m.Button({
					visible: false,
					icon: 'sap-icon://copy',
					//id: 'btnidTransaccion',
					tooltip: 'Copiar código de transaccion al portapapeles',
					press: function () {
						that.copyToClipboard(idTransaccion);
					}
				}),
				endButton: new sap.m.Button({
					text: 'Salir',
					press: function () {
						var aplicacion = "#";
						var accion = "";
						that.regresarAlLaunchpad(aplicacion, accion);
						dialog.destroy();
					}
				}),
				afterClose: function () {
					dialog.destroy();
				}
			});

			dialog.open();
		},
		onMessageWarningDialogPress2: function (idTransaccion, mensaje) {
			var that = this;
			var dialog = new sap.m.Dialog({
				//id: 'dglExitoso',
				title: 'Advertencia',
				type: 'Message',
				state: 'Warning',
				contentWidth: '25rem',
				content: new sap.ui.layout.VerticalLayout({
					//id: 'idVertical',
					content: [new Text({
						text: mensaje + '\n' + '\n',
						textAlign: 'Center'
							//id: 'txtMensaje'
					})]
				}),
				beginButton: new sap.m.Button({
					visible: false,
					icon: 'sap-icon://copy',
					//id: 'btnidTransaccion',
					tooltip: 'Copiar código de transaccion al portapapeles',
					press: function () {
						that.copyToClipboard(idTransaccion);
					}
				}),
				endButton: new sap.m.Button({
					text: 'OK',
					press: function () {
						dialog.destroy();
					}
				}),
				afterClose: function () {
					dialog.destroy();
				}
			});

			dialog.open();
		},
		onMessageSuccessDialogPress: function (idTransaccion, mensaje) {
			var that = this;
			var dialog = new sap.m.Dialog({
				//id: 'dglExitoso',
				title: 'Éxito',
				type: 'Message',
				state: 'Success',
				contentWidth: '25rem',
				content: new sap.ui.layout.VerticalLayout({
					//id: 'idVertical',
					content: [new sap.m.Text({
							text: mensaje + '\n' + '\n',
							textAlign: 'Center'
								//id: 'txtMensaje'
						}),
						new sap.m.Input({
							value: idTransaccion,
							enabled: false,
							//id: 'inputTranscaccion',
							class: 'tamaniotexto'
						})
					]
				}),
				beginButton: new sap.m.Button({
					icon: 'sap-icon://copy',
					//id: 'btnidTransaccion',
					tooltip: 'Copiar código de transaccion al portapapeles',
					press: function () {
						that.copyToClipboard(idTransaccion);
					}
				}),
				endButton: new sap.m.Button({
					text: 'Cerrar',
					press: function () {
						dialog.destroy();
					}
				}),
				afterClose: function () {
					dialog.destroy();
				}
			});

			dialog.open();
		},

		messageStrip: function (controller, id, message, tipo) {

			var control = controller.getView().byId(id);
			control.setText(message);
			control.setShowIcon(true);
			control.setShowCloseButton(false);

			if (!tipo) {
				control.setType("Information");
			}

			if (tipo.toUpperCase() === "E") {
				control.setType("Error");
			}

			if (tipo.toUpperCase() === "S") {
				control.setType("Success");
			}

			if (tipo.toUpperCase() === "W") {
				control.setType("Warning");
			}

			if (tipo.toUpperCase() === "I") {
				control.setType("Information");
			}
		},
		objectListItemSelectedItem: function (event, model) {
			return event.getSource().getBindingContext(model) == undefined ? false : event.getSource().getBindingContext(model).getObject();
		},
		messageBox: function (mensaje, tipo, callback) {
			if (tipo.toUpperCase() === "C") {
				MessageBox.show(mensaje, {
					icon: MessageBox.Icon.QUESTION,
					title: "Confirmación",
					actions: [MessageBox.Action.YES, MessageBox.Action.NO],
					onClose: function (sAnswer) {
						return callback(sAnswer === MessageBox.Action.YES);
					}
				});
			}

			if (tipo.toUpperCase() === "E") {
				MessageBox.error(mensaje, {
					onClose: function (sAnswer) {
						return callback(sAnswer === MessageBox.Action.YES);
					}
				});
			}

			if (tipo.toUpperCase() === "S") {
				MessageBox.success(mensaje, {
					onClose: function (sAnswer) {
						return callback(sAnswer === MessageBox.Action.YES);
					}
				});
			}

		},
		messageToast: function (data) {
			return MessageToast.show(data, {
				duration: 3000
			});
		},
		imageResize: function (srcData, width, height) {
			var imageObj = new Image(),
				canvas = document.createElement("canvas"),
				ctx = canvas.getContext('2d'),
				xStart = 0,
				yStart = 0,
				aspectRadio,
				newWidth,
				newHeight;

			imageObj.src = srcData;
			canvas.width = width;
			canvas.height = height;

			aspectRadio = imageObj.height / imageObj.width;

			if (imageObj.height < imageObj.width) {
				//horizontal
				aspectRadio = imageObj.width / imageObj.height;
				newHeight = height,
					newWidth = aspectRadio * height;
				xStart = -(newWidth - width) / 2;
			} else {
				//vertical
				newWidth = width,
					newHeight = aspectRadio * width;
				yStart = -(newHeight - height) / 2;
			}

			ctx.drawImage(imageObj, xStart, yStart, newWidth, newHeight);

			return canvas.toDataURL("image/jpeg", 0.75);
		},
		resizeImg: function (img, maxWidth, maxHeight, degrees) {
			var imgWidth = img.width,
				imgHeight = img.height;

			var ratio = 1,
				ratio1 = 1,
				ratio2 = 1;
			ratio1 = maxWidth / imgWidth;
			ratio2 = maxHeight / imgHeight;

			// Use the smallest ratio that the image best fit into the maxWidth x maxHeight box.
			if (ratio1 < ratio2) {
				ratio = ratio1;
			} else {
				ratio = ratio2;
			}
			var canvas = document.createElement("canvas");
			var canvasContext = canvas.getContext("2d");
			var canvasCopy = document.createElement("canvas");
			var copyContext = canvasCopy.getContext("2d");
			var canvasCopy2 = document.createElement("canvas");
			var copyContext2 = canvasCopy2.getContext("2d");
			canvasCopy.width = imgWidth;
			canvasCopy.height = imgHeight;
			copyContext.drawImage(img, 0, 0);

			// init
			canvasCopy2.width = imgWidth;
			canvasCopy2.height = imgHeight;
			copyContext2.drawImage(canvasCopy, 0, 0, canvasCopy.width, canvasCopy.height, 0, 0, canvasCopy2.width, canvasCopy2.height);

			var rounds = 1;
			var roundRatio = ratio * rounds;
			for (var i = 1; i <= rounds; i++) {

				// tmp
				canvasCopy.width = imgWidth * roundRatio / i;
				canvasCopy.height = imgHeight * roundRatio / i;

				copyContext.drawImage(canvasCopy2, 0, 0, canvasCopy2.width, canvasCopy2.height, 0, 0, canvasCopy.width, canvasCopy.height);

				// copy back
				canvasCopy2.width = imgWidth * roundRatio / i;
				canvasCopy2.height = imgHeight * roundRatio / i;
				copyContext2.drawImage(canvasCopy, 0, 0, canvasCopy.width, canvasCopy.height, 0, 0, canvasCopy2.width, canvasCopy2.height);

			} // end for

			canvas.width = imgWidth * roundRatio / rounds;
			canvas.height = imgHeight * roundRatio / rounds;
			canvasContext.drawImage(canvasCopy2, 0, 0, canvasCopy2.width, canvasCopy2.height, 0, 0, canvas.width, canvas.height);

			if (degrees == 90 || degrees == 270) {
				canvas.width = canvasCopy2.height;
				canvas.height = canvasCopy2.width;
			} else {
				canvas.width = canvasCopy2.width;
				canvas.height = canvasCopy2.height;
			}

			canvasContext.clearRect(0, 0, canvas.width, canvas.height);
			if (degrees == 90 || degrees == 270) {
				canvasContext.translate(canvasCopy2.height / 2, canvasCopy2.width / 2);
			} else {
				canvasContext.translate(canvasCopy2.width / 2, canvasCopy2.height / 2);
			}
			canvasContext.rotate(degrees * Math.PI / 180);
			canvasContext.drawImage(canvasCopy2, -canvasCopy2.width / 2, -canvasCopy2.height / 2);

			var dataURL = canvas.toDataURL();
			return dataURL;
		},
		loading: function (bShow) {
			if (bShow === true) {
				sap.ui.core.BusyIndicator.show(10);
			} else {
				sap.ui.core.BusyIndicator.hide();
			}
		},
		getValueIN18: function (context, nameField) {
			return context.getView().getModel("i18n").getResourceBundle().getText(nameField);
			//{i18n>Description_TEXT}
		},
		crearExcel1: function (self, model, tabla, arrayFiltros, newModel, newTabla, arrayColumTabla, tituloExcel, nameFile) {
			var results = self.getView().getModel(model).getProperty("/" + tabla);
			this.onCreateExcel(self, results, newModel, arrayColumTabla, tituloExcel, nameFile);
		},
		crearExcelOdata1: function (self, model, tabla, arrayFiltros, newModel, newTabla, arrayColumTabla, tituloExcel, nameFile) {
			var that = this;
			var odata = "/" + tabla;
			var oDataModel = self.getView().getModel(model);
			//new Filter("CodigoTabla", "EQ", "tipo_usuario");
			var onSuccess = function (oDataModel) {
				var results = oDataModel.results;
				self.getView().getModel(newModel).setProperty("/" + newTabla, results);
				that.onCreateExcel(self, results, newModel, arrayColumTabla, tituloExcel, nameFile);
			};
			//Handler en caso de error
			var onError = function (error) {
				jQuery.sap.log.info("--cargaTablaClasePedido error--", error);
			};
			var reqHeaders = {
				filters: arrayFiltros,
				context: self, // mention the context you want
				success: onSuccess, // success call back method
				error: onError, // error call back method
				async: false // flage for async true
			};
			oDataModel.read(odata, reqHeaders);
		},
		onCreateExcelPasado: function (self, resultados, newModel, arrayColumTabla, tituloExcel, nameFile) {
			/*for (var i = 0; i < resultados.length; i++) {
				resultados[i].FechaCreacion = resultados[i].FechaCreacion.getDate() + "/" + (resultados[i].FechaCreacion.getMonth() + 1) + "/" +
					resultados[i].FechaCreacion.getFullYear();
			}*/
			var rows = resultados;
			//////Inicio Fecha Actual/////////////////////////////////////////////////////////////////////////
			var date = new Date();
			var yyyy = date.getFullYear().toString();
			var mm = (date.getMonth() + 1).toString(); // getMonth() is zero-based
			var dd = date.getDate().toString();
			var fechaActual = (dd[1] ? dd : "0" + dd[0]) + "/" + (mm[1] ? mm : "0" + mm[0]) + "/" + yyyy; // padding 
			//var fechaAct = (dd[1] ? dd : "0" + dd[0]) + (mm[1] ? mm : "0" + mm[0]) + yyyy; // padding 
			///////Fin Fecha Actual///////////////////////////////////////////////////////////////////////////
			var datefull = fechaActual; //this.formatter.date_full(new Date());
			var html = "";
			var tr = "";
			var th = "";
			var td = "";
			html = '<h2> ' + tituloExcel + ' </h2>';
			html = html + '<label>Fecha Reporte: ' + datefull + '</label>';
			var columnas = [];
			columnas = arrayColumTabla;
			/*[{
			        						name: "Id",
			        						model: "Id"
			        					   }];*/
			tr += '<tr>';
			for (var l = 0; l < columnas.length; l++) {
				var text = columnas[l].name;
				th += '<th style="background-color: #404040; color: #fff">' + text + '</th>';
			}
			tr += th + '</tr>';

			for (var j = 0; j < rows.length; j++) {
				tr += '<tr>';
				td = "";
				for (var k = 0; k < columnas.length; k++) {
					var item = rows[j][columnas[k].model];
					td += '<td style="background-color: #e6e6e6">' + item + '</td>';
				}
				tr += td + '</tr>';
			}
			html = html + '<table style="width:100%">' + tr + '</table>';
			var fileName = nameFile + fechaActual + ".xls";
			//var data_type = 'data:application/vnd.ms-excel';
			var ua = window.navigator.userAgent;
			var msie = ua.indexOf("MSIE");
			if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
				if (window.navigator.msSaveBlob) {
					var blob = new Blob([html], {
						type: "application/csv;charset=utf-8;"
					});
					navigator.msSaveBlob(blob, fileName);
				}
			} else {
				var blob2 = new Blob([html], {
					type: "application/csv;charset=utf-8;"
				});
				var filename = fileName;
				var elem = window.document.createElement('a');
				elem.href = window.URL.createObjectURL(blob2);
				elem.download = filename;
				document.body.appendChild(elem);
				elem.click();
				document.body.removeChild(elem);
			}
			self.getView().getModel(newModel).refresh(true);
		},
		///Inicio Export Excel///
		onExportExcel: function (self, model, tabla, arrayFiltros, newModel, newTabla, nameFile, nameHoja, mapEstructura) {
			var results = self.getView().getModel(model).getProperty("/" + tabla);
			this.onCreateExcel(self, results, newModel, nameFile, nameHoja, mapEstructura);
		},
		onExportExcelOdata: function (self, model, tabla, arrayFiltros, newModel, newTabla, nameFile, nameHoja) {
			var that = this;
			var odata = "/" + tabla;
			var oDataModel = self.getView().getModel(model);
			//new Filter("CodigoTabla", "EQ", "tipo_usuario");
			var onSuccess = function (oDataModel) {
				var results = oDataModel.results;
				self.getView().getModel(newModel).setProperty("/" + newTabla, results);
				that.onCreateExcel(self, results, newModel, nameFile, nameHoja);
			};
			//Handler en caso de error
			var onError = function (error) {
				jQuery.sap.log.info("--cargaTablaClasePedido error--", error);
			};
			var reqHeaders = {
				filters: arrayFiltros,
				context: self, // mention the context you want
				success: onSuccess, // success call back method
				error: onError, // error call back method
				async: false // flage for async true
			};
			oDataModel.read(odata, reqHeaders);
		},
		onCreateExcel: function (self, results, newModel, nameFile, nameHoja, mapEstructura) {
			var data = mapEstructura(results);
			/* this line is only needed if you are not adding a script tag reference */
			if (typeof XLSX == 'undefined') XLSX = require('xlsx');
			/* make the worksheet */
			var ws = XLSX.utils.json_to_sheet(data);
			//Inicio Filtrar//
			var AutoFilter = {
				ref: "A1:E1"
			};
			ws['!autofilter'] = AutoFilter;
			//End Filtrar//
			//Inicio Tamaño de la Columna//
			/*var wscols = [{wpx: '100'},{wpx: '100'}];
			ws['!cols'] = wscols ;*/
			//End Tamaño de la Columna//
			//Inicio Agregar Comentario//
			/*ws.A2.c.hidden = true;
			ws.A2.c.push({a:"SheetJS", t:"This comment will be hidden"});*/
			//End Agregar Comentario//
			/* add to workbook */
			var wb = XLSX.utils.book_new();
			//Inicio Agregar Otra Hoja//
			/*wb.SheetNames.push("Test Sheet");
			var ws1 = XLSX.utils.aoa_to_sheet(data);
			wb.Sheets["Test Sheet"] = ws1;*/
			//End Agregar Otra Hoja//
			XLSX.utils.book_append_sheet(wb, ws, nameHoja);
			/* generate an XLSX file */
			//////Inicio Fecha Actual/////////////////////////////////////////////////////////////////////////
			var date = new Date();
			var yyyy = date.getFullYear().toString();
			var mm = (date.getMonth() + 1).toString(); // getMonth() is zero-based
			var dd = date.getDate().toString();
			var fechaActual = (dd[1] ? dd : "0" + dd[0]) + "/" + (mm[1] ? mm : "0" + mm[0]) + "/" + yyyy;
			//////End Fecha Actual////////////////////////////////////////////////////////////////////////////
			XLSX.writeFile(wb, nameFile + fechaActual + ".xlsx");
		},
		///End Export Excel///
		///Inicio Import Excel///
		onImportExcel: function (self, oEvent, model, tabla, idFileUploader, mapEstructura) {
			var file = oEvent.getParameter("files")[0];
			if (file && window.FileReader) {
				var reader = new FileReader();
				reader.onload = this.onReadFile.bind(self, self, model, tabla, idFileUploader, mapEstructura);
				reader.readAsArrayBuffer(file);
			}
		},
		onReadFile: function (self, model, tabla, idFileUploader, mapEstructura, evt) {
			var result = {};
			var data;
			var arr;
			var xlsx;
			data = evt.target.result;
			//var xlsx = XLSX.read(data, {type: 'binary'});
			arr = String.fromCharCode.apply(null, new Uint8Array(data));
			xlsx = XLSX.read(btoa(arr), {
				type: 'base64'
			});
			result = xlsx.Strings;
			result = {};
			xlsx.SheetNames.forEach(function (sheetName) {
				var rObjArr = XLSX.utils.sheet_to_row_object_array(xlsx.Sheets[sheetName]);
				if (rObjArr.length > 0) {
					result[sheetName] = rObjArr;
				}
			});
			var contenido = result["SAP Document Export"]; //this.onRemoverHeader(data);
			//var localModel = this.getView().getModel("modelProductos");
			var formatContenido = mapEstructura(contenido);
			var modelTb = self.getView().getModel(model).getProperty(tabla) === undefined ? [] : self.getView().getModel(model).getProperty(tabla);
			var modelConcat = modelTb.concat(formatContenido);
			self.getView().getModel(model).setProperty(tabla, modelConcat);
			self.getView().byId(idFileUploader).setValue("");
		},
		onRemoverHeader: function (data) {
			var excelKeys;
			var primerLibro = {};
			excelKeys = Object.keys(data);
			primerLibro.nombre = excelKeys[0];
			primerLibro.contenido = data[primerLibro.nombre];
			var shiftedInfo = primerLibro.contenido.shift();
			return primerLibro.contenido;
		},
		///End Import Excel///

		///Inicio Descargar PDF///
		onGenerarPDF: function (value) {

			$.generarOrdenCompraPDF = function (value) {

				var doc = new jsPDF('p', 'mm', 'a5');

				var setBranding = function (value) {
					var cord_X = 10;
					var cord_y = 1;
					/*var pic =
						'data:image/jpeg;base64,/9j/4QlQaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjYtYzEzOCA3OS4xNTk4MjQsIDIwMTYvMDkvMTQtMDE6MDk6MDEgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiLz4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8P3hwYWNrZXQgZW5kPSJ3Ij8+/+0ALFBob3Rvc2hvcCAzLjAAOEJJTQQlAAAAAAAQ1B2M2Y8AsgTpgAmY7PhCfv/bAIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgICAwMDAwMDAwMDAwEBAQEBAQECAQECAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD/90ABAAS/+4ADkFkb2JlAGTAAAAAAf/AABEIAFQAjgMAEQABEQECEQH/xADEAAACAwEBAAMBAAAAAAAAAAAACQcICgYFAgMECwEBAAIDAQEBAQAAAAAAAAAAAAYHBAUIAwIBCRAAAAYCAQIEAgYEDQUAAAAAAQIDBAUGBwgACREKEhMhFDkVFiIxeLcXGHm2IyYyMzdBQlFhd7S1uDhxdXaREQACAQMDAgIDCgoECwkAAAABAgMEBREABhIHIRMxFCJBFTI3OFFhdXaztAgWFyM2QnF0gbUzcpGxGCQmRVJid4KhssMlKFNWY5KTo8H/2gAMAwAAARECEQA/AN/HGmjjTS55Xqb66vsn7e4Lxg6nMk5g0xwdd8x5YimsU9gqWwkKczVWNjcbtJNxI4tzhx6QLiyZvWjNJQROqKpRR5ZlL0r3H7nWa93Tw6WzXu4Q0sDFg8pEx7TeEp/owMkBnVmOPV4kNqC3DftqpWuNNRq89bbqGpqHHZEJpgvKPkctkswXkEKZVwGLIyig0h1ic0SHTi1m3fruG8YxNpzvtPEYPl6DNz9qnYCvVKStl9rwykfMsCV+Re2MiFTQP5lEStQOqf8AgxAChy0qLoTYT1TuvT6srqtqO32Y1izIsau8gSBuLK3NQn50+R5dh38zqs7x1ku1LsKh3hQ0tN41XdZKUo/MqFSOpYMOLqcl4FBJJAVieOcKGjm2zUS3wDSpWhiZN1rmnn1lkdKwFAEhSujqoPKq8rB4sDmE4kTcJPE3ogH2iGR+4/KnGwQ3S38pK1Xdbx6C1OY/P8yJRKJOfzlShT5CG9mp5+ULj1X/ACZNSjvZ1rlnEhz/AErxGMx8MY9UEOJM98cD5iwmMstY7zHCy9gxraGNpi6/brPQp5Zmm7bqw1zpkmrD2iuSTR83au2slDyKQkUKYnlOUSqJmOmchzRK+bevO26mOjvcDQTzU8VRGCQQ8My84pFKkgq69xg5BBUgMCBLbBuWx7opZayxVC1EEFTJTyYDKY5oiBJG6uFZWXIPcYZWV1JVlJkbmm1vdHGmvFsFkr1TinM7ap6FrUIzAovJmwSjGGimgHMBSC5kJFds0QA5hAA8xw7jz2p6aoq5RBSxvLO3kqKWY/sABJ15TTw00Zmq' +
						'HWOIebMQoH7ScAa8qpZBoN/bKvaJd6hdWaAgVd3UrLDWRsiYfuBVeHevEkxH/EQ561dvr7ewSvgmgc+yRGQ/2MBrypa6irV50c0UyD2o6sP7VJ11/MTWVo400caaONNHGmjjTRxpo401/9DfxxprKt1ld88ivdtemHrfgnKTBPXbNmfKbZsoTWPpVUH2SpXGO0NUxm+x5I2WOcFBShwtjjHwSDJqYCST1ACLnUbpemfqno3sOgj2tuTcV9pW/GGioJUhWVe0KzUTTLKEI7SOjrxY90U9gCx1Se/d1yyXC3W+2SJJa56mIMV7hyJ5I3BPt4NCQv6vIs/dljZa568fMz8Rl+GzYP8A0Lfk0vXwadNvpig1CG/SHeH0NdftBqMIv5AnTy/aE1P8zsy8mUHxk9zfVZvsaTVb3n4DbN9ZZ/s7hp/Lr568X+zqcfnQ45z1H8VyT63j7mNWtJ8bBPqaPvkmuE1vydasL6J7+ZYo6kcnbMf7a7f2mE+lmX0hGLPIrITZf4WQaAq3Os0eJFMifynIcpT+YpgMACEi3dYLbunq1s7bl3Dm21u3rRFJwbi4V4GGVbBwVOCMgg4wRgnUE2xue7bL6Kb93XYWjS70G5LrNEXTxE5I9P2ZMryVhlThgQDkEEA6dpDPjycPFSSiZUVJCNYvlEiGE5Eju2qS50yGMAGMUhlBABEAEQDnLVVCKeqkp1OVSRlz8uCRn/hrsWgqGrKGCrcBXlhRyB3ALKGIB+bOvS54ay9YlPEQPTj1H9WojcJbMS3T9dUCAXQiMauTJIvJQJ21oZWc1Zs+71t9lFkY8CZ0VUp5QIDyA08pjgI9pfg+RqenV0l2kKP8fvHcZmH6vFDCHx64hP5zjj1PF5cs4OOY+sE08e86AXw1a7TVEYmHiOJ5EMwLgqP1hKew4mESEAx6Yl0ptDej+jnyqbbdO/Yi8XKz49qdtj5nEMtkZJ0vEhdodGurTF2x1P1iv5IiXES3frIIGdieNM5X8yYGUImcK86pb56t+4Mu09/26CClqJUKzrEQG8NuYWOVXeE5KgnjhwowexI1N9j7b2FPdItw7YrpqipiUsEdl5KGR48lPDSTiRIe55IzKpU+qM9llbq99RFnkzJ9BwT0aNgb5D4vvl0orm92GbsLOAtR6dNPYo0/V1orHCsRJxE02aldMxaSL0y6CxBJ5jD5eYlq6SdP3tlNX3zeFBBPUwRyCJRGWTxFDcX5T8gVzhuSLgg58tetw6h7rS4TUdssFTLTwuymQichihwwXjThfMEArI4YYZSVYE+XWfEFQVx6febNvYTXF+2yvrtlLGuKsoYKm7/8DGIvclTbKHh7RC3xKouHSsMt53fdq5iG75u8YLtVSh5SLqe1R0AnpOoFFtGa4qbVcKaaaGpWLJIhXkyNEZAOQ9X1hIVKsGHfKjHi6wQT7PqdyR0ZFdSPGskDORgScgrcuBIyUcAFPMZyUKsa/WLxGWzkfRIrPbHpQZeY6z+SFPLZcs13sjCsPwlnLZmgpXronh8KX8HJvXabePcruRQdLKJgJyCoBSyCn/B223LXvYW3VSNuX1+NOkcZccQTho/SfEyACWAAKgE9wMnRy9bbvFSJd5LDUJYyU5TEzlByIBPiejKmBnAxyLHsByIGtJ2tmfKRtLgTE2w+OPpAlJy/SIW7QTWYQTbTEajKtwM6hZhuiq4bpS0HIEVZugSUVS9dA/kOcnlMPN+47DW7YvtXt+48fTKSZo2K+9bB7MuQDxYYZcgHBGQDkC8bNdae92qC60oKwzxhsHGVPkynBIyrAqeJK5HYkYJm7ml1s9HGmjjTRxpr/9HYd1NtkpTUTQPa7YivqGRtWO8QWFSkuCiUBa360GbUuhPOximKoDO42NiqJBD7YE8vt37hNenG34t075tliqBypZ6pfEHyxRgyyj+MaMM+zOtBuive22Gpqon8ObgEV/8AQeVliV/n4s4bHtxrC84SFFn4ZcpjmWVPV68u5cKCJlnbxzvBEuXr5wcwmMq6fO1TrKnMImOocxhERERHuShzw6igkkgsO/zWsAf8Nc63vw/SLH4KLHF6WMKoAVR6dWYUAYAA8gBpq2vHzM/EZfhs2D/0LfkCvXwadNvpig16t+kO8Poa6/aDUWx4iHQA6ewgPYQ6gtXEBD7wEMmZm7CH/YeTSm+MruX6rt9jR6ra+duhdnx/5kn+zuGn+Ovnrxf7Opx+dDjnPMfxXJPrePuY1bEnxsE+po++SagOjfLI6mn4j91f35S5O6z4d9h/Qtm+xbVSSfFw6lfTl3/5qfTpmeQ4uuzOF8fPEzfSGR6nOOIdcDgBCL0qBgJJ43MQQ7nFdjJHOHYQ7eiP39/bmKWx1NdS3a+RH8xQVMYcfNPJKqn+DKB/HXWkG6qK1XDb+1qgH0q7UUzRNntmkhhkZce3KSEj5OP9kw8i+p5pE/U76q2hGt2WWmnW7eBrZl3H9txtH5AnZlXGlbyBj+Kk5WVmIeGgloa3PIlR/PpRbNZ4L6IF0tHFXSKPkUU+zeHTTpdvrcVqbd+zK+KjroqholXxnilZQqszcow2FJPHg+A3FvYBmsN57321abiu3twUclVDKgfskbgN3/VdlxwHElwexkUJyIk4ZwdRy665n62erly6SeNM2UDBtXn4qwZXLLmkjsK5W0m1oDKyvqrTVjdU3E1mrTllFIxUxICZ3KqiDVEnmRTL0Zuxr/aOitypOq1TRVN7ljZIOOMs3qeAPeoHmSQGQtGnZACxOGY0nYYaGs6o082xaaspbWrRtMJO4J8QtKw9Z8K0PNA7MSAXi5DxI49eUruZY8u3/YCodWPqhbtauWXHOULTXj6v4RoDyrJWGGYPXXkYtZSlsmMPDKIKlMzZoybBZNy0+HeleLEXOJfr8T6S0UFBV9K9tWW501RTI/plTMrlCfaVky7Aj1iY3BB5JwBAz7RX243OoqId9XS40tRE/HwKeKQgtxBYZST0fKtkcZYSqk45HBxWjAshEl6U3Vx9BV0zao7AaHmQbWBZMthaRrvJ92WhS2UB9MSzriOMQ7kwgBVXAnOTuUQEZXflk/KptIvxLehXPJT3mRDDnj/q58vkGNQ6xwFtg7hjp1lbmaIry7sw8SsyRx7EBlcAr2HEjtxIGirYuRRDwsNPdqvU/hjaratJfEncF9EyIZOxUggn6pj+QUg8hCEDv2DsAB9wdueNtp/3opQo/wA51x/+ick//urt3Ish6GJGQ3i+DSLjvnPpUI448857Y+XTQeh+qiv0otJ1W6iayR8VOxIokcp0zdrragMJTFESj2OAgP8AjysOtgI6p3nPn6Sv2Uep/wBNFKbJokYYYeMCD5gieUEH5wex01flWanWjjTRxpo401//0tI3iJ4aSmOj7tuMaJx+h2uJbBIpp+YRUh4bN+OHcoJgKA90mzQhlz9/YCJCI/dy5OgM8UHVa2eLj1xUKPmY00uP7fIfOdQPqTTzVG05VgzyE0BwM9x4yLjt85B/hrJY/wD5jwzH/qlZ/wCbUNzrWh951F/rP/LBqkLz/TWL96X79V6ahrx8zPxGX4bNg/8AQt+QK9fBp02+mKDXu36Q7w+hrr9oNRfHlEfD/wDT4N2+yXqC1buP9Qd8mZmAO/8AX7jyaUxH+EtuQe38V2+xo9VtfAfyFWcjy/GSf7O4af06+evF/s6nH50OOc8x/Fck+t4+5jVsSfGwT6mj75JqA6N8sjqafiP3V/flLk7rPh32H9C2b7FtVJJ8XDqV9OXf/mp9MsvUY+fbB6GvGqZzNoaEzc+klCgPlSar4iiItIFBD2Aqj+QRAO/9oA5S1nqIYdkbzikI8SWWgVB8rCtdzj/dVtXPuWjqajqn0zqIQTDT093eQjyCtbIoxn9run8dXb5UeuidJy3d6sekWsmytK0+2fxzdbJK3Fhjyd+tTzHtJt+JaxF5Is8nVIyYsruwWRGTZs4Z7EKLSSiMY4Fu0/hCgoICQLg2T0p3tubbc+79sVEMccLSrwEsqTu0SByqBEKksGAQFxluxx56rXdfUDa1jvMW3L/CzmVVbkywtEqnzZhJIGwoBY8UY9sLlu2mwVKhUXH7FeNolLqVJjXKoOHLCo1yHrbFwuUolBddrDM2SCqoFEQ8xiiIAPKpq6+uuDiSvmlnkAwDI7OQPkBYk6sGmo6OiQx0cUUMZOcIqoP7FAGk863dTHp49Q7bq2a90jBsjeclUOJvcs0y1kbEGNX9RnoLGFpjKs5kqla3c1O2paOkn0um4ijKsm3qtB9QQSEQKNv7j6a9Qenu0YtwVtcsFtneNTTxTzrIrTIz8ZE4JHkBSr4Zu/YZHfVbWTe+zd57jktNNSCauhUsJnjgdWVccWRg7yAMCCMquM4bi3YOCdY8oD4J0HtGpzwLQ6YPbMDqswrgLE9iilJFu50FWRwl3UaQgA3UceodEAACCXtyoUuFfH4ZjnmHhAhMOw4BvfBe/qhvaBjPt1ZDUVFJz5wxHxMcsqp5Y8uXbvj2ZzjXoLVKqOa8WpOazXnFUI3btCVleGjVa8Ro0UTVatSwp2xo0rdsqkQyZAS8pDFAQABAO3wtXVLUelrLIKvJPMMeeT5nl' +
						'nOTk5Oe+dfbU1M0PozRoafAHAqOOB5erjHb2dtcjkC64915xBd8jWBsjV8ZYho9lvFgQrcEZVOHqtTins/NKRNehG3qOFEWTRU5G7ZITqn9ilExvfLt9FcNw3eC3U5MtzrJ0iTm3vnkYIvJ2PbuQMk4Gsauq6KyWyavnAjt9LC8jcV7KiAs2FHzAnA1F+om4GD94sPNs7a+TU5P46dWSwVNGQsFXm6hIGmaw4Say6IxE+1ZvwQSWWKBFfJ5FP7Ij2Htst2bSveyrsbJf0SO4CNXwrq44vnj6ykjPby1jWK/W7cVF7oWxi1Nz45Ix34q3zg9mHkfPse4IEcq9RHWpzt+poxUpa6ZJ2HiIyPmb5BY2oVjt9XxNFSKAuk3uWL5HNRqVEKi2UbGUSeOirgZ81TKQyy5Ex2C7A3ENp/jrVLDTWFiRG00qRvOQcYgjJ5yEnOOIwQrH3qk6xpNz21bwtjiEstcWweCFlTtkl27YVewdhlVZlQkOwU3l5CtSLRxpr//09x2x+D6psxgHMuvl4KH1VzNjW444mVwQI4Vj29rg3sQnLtUTmIUz+FcuSO2/cQ7LokHuHbvzcbevVTty+0d+pO9RSVEcoHkG4MCVPzMAVPzE6wLrQLc7bPb2Yr4sbKGHmrEeq4+dGww+cDWC/NeFcoa+5T8OxgTL9ad1fKeKnTbH1sg3ZkzGPLQG98bFoSrJYg+k5h7K1RSkWC4D5VmTpJQPY3O79u3e23mz7+vVsmWa21ERkVx5EG1KSCPYynKsvmGBHs1zZuCCpW42SCaB4Kg1qjwmxlc19XxGfIjBGGHYggg4OrT0/KBqHvP4kPIFeZM7AZhhHMFdI1fqumLRVaQv0FjqcE6iSRnPmiXblyJAAAKsdAAAwEMBwxlsXuns3pnaaxmh5XCllyuGOFpnqI/bj1wq59qhvLIxqN3e/x0lbu65UHGZfRK2mOcgAy3CGlkPlkmPxGZfYxUdwDkQ1Zb9Za/0rekDh6PWZfUjIew+bsg2dJdkCsqvacVbFsa/UFmEj6xfo9ijG3uRBygCZviDnTN5i+TsacWW00dV1g3vfpQ3ujSWuip4yDhRHUURkl5L7WLQJxOewBHtyKr3VfKyn6bbSs0QUUdbXXKpk9p8SGZolwT3A4VDjAI74Jz2w2Ks7lWZl1aeofnHI1ci5iE0X1L2CgoOvUtuaImbZjXEE/SL+kyeu5eQdsV7q+XtLxP4owt2hiESJ6aYFEw05W7IpD+D1tuwWyR0n3BuGid3kPJY56mGWIkBQCIh4anj3bzOTnGrMo901LfhN3i616xmK1bXq1CqCuYqeryAT62WIcktj5guANSxr7kCDyn0iN6sjV5F6xi8rZH2typW4SX+FLYmlYtMtA2ZIko0ZruURdwrKdbpvjoHUbpKm/l+UxRH93Bbp7R+ETs22VJVpaS32mB2XPAuiSJ6pIBwxUlQcEgeWc4h1BcYbt+DD1Dr4AypUXe7yKrEFuJNMe/HI7ZGSMgZHfWhCtRke4hqbKrs26slG1lo2YPVEymcM0JKPjBfpt1B90yu/gkvP2+8CBzj64VE6VdVTIzCCSoYsuezFWfiT8uOTY/bruO0UdLLQUFbJGrVcVGqo5HdRIkfMKfZy4Ln5eI113NZreawMeItoNnyn1U61jqlQTuz2u3ayY2joSux/YX0wu2lcxy7tkzATFE7o0ZGrmTIA+Y5iAUvcwgA97fg63CjtXSyavr5BFSR3KUs58lBWBQT82SO/s1xv10orhX9QKeG2I0lYtIjKBgnKsSMA4z3x29ur/q9YR6bw9hcofWdb9ZNUv6iAy/mH6U/SGFa/hMlAPr/Gi8HX3+NPxPl9pcwJ/f78rxekUf+EB7m+GPxcH/AGnxwOPhc/6LHlx9K/Ncf/C76sA9QpPyQ+ket7scvc/zbPLjjPiYzz8D1fF976V6ufbpdfh8KJYsVdTPIuO7ym6xpZqbqfluNsASCjJF7R1VXuJZRF49UfeqwbuYiMfouVAcAKaRiiVUPsmDllfhB19LdOmVLX2/FTSy3WApjJEgAmGBjuQxBAx3Ps9mq56JUVfQb6q4rpygqzRSsxbiCpJXLHzUEHJwewx3+TXcZ+vd/wAWV67S2VvE2SluzNExMzI0vGGuLO+WqBtdtZEdKwNYdy2JrWyrVZTmXaaTc7szIrdkZUyhiHTT8ptTt6hoLnUQU9t6bpFaXdRJNWGGN0jOAzhZ4zJIVGWChstjAIJyJNuCeup4J6+TdrtVRoxEdL6XNGZFBPAyQypFGCRxy6EJnvnGD6uU+qJvFaOiXgzPIZ8vdNzfR95ZzAVpydTHcVCW3I1JrOIrNeoZG5KtY0IiSkCEfNGz1VNukV+tHkcKgZRRUT/Nr6X7JpOtFfYmoYZrLNY1q0hkBZIZGqEibw8nIHqsy9/V5lVwoAHjXdQ9z1vTakutLUsly91/RTKC681am8UMQpVsBnwAxY8VUuXfLlhKOnnUnqOtWy+0O23UasOwOL7T0/tlJZbAkXHyMRVFLNesMPXFKePWKrePrx4+rtHzldT4RgwXF4RHymOiByDXw3d06qt0WzbG1NvR0Fyi3BRqKluJk4RVK+IAwJfLlQo5Mw4lvI4OpfVWHdkW0LjfL9c5KmCSy1D+EHcoS9NJgFCFjxlw5KqpUoqjKk6q30ItONxssa0IZmxnvpd8M4RXb7LUOu4Ar0XJqxMTmKUqctT61lOSeEm2bJ+1gbhMN7EdiRAp3C7JInrEETiEo66bx2fatz+5FysMFZdx6HK9S5Xk0CyLI8K+qSC8atFyJwA5PE4GdH0q2tuSv249ZSXeangZp1RFL8Vd4GVGK8gh8N3icYGT4WMjlkV7092s2vYdOXrX5EfbD315mXElj1xjqzmBgEBEXlrNLZUstXtdiSsDCBayz55bGrIwLHkFHa5EVRKUxTCcxpBu7am1ZOoWzLclvgFnq460vAeTRlRTo6LwLFQEJyAoAyO+QABp9v7jvqbV3HcDUN6dA1AyupYN+cqpOeXJMhLoQjnkCQMjixJPap0vq+3HpsG6nD3qqZOYVCuYwd5DgsSwq8vDWF9XKtZHVWetLBYIcsVXJGyqGjVFyqPI6STdm8pFlPtnOGC1Z0jouo/5NU2vTNUy1AiadgjKGeMSAorcmVfWA9Vk4+wdgDnRUvUWs2Wm8jenDRqSsYknCtxm8I8wrLnyzhuRb2uCcjTz0kNl8j7ddPnXnOeXnbWTydYIe01+6zjJg0i29lmqDebLRT2cYxgRJiwdWFrXknjlJBNJuRysoCRCJ+QheZ+rO2bdtDqBcLFaQVtsbRvGpJPBZYkl4ZOSQhcqpJJ4gZJOSbs6cbgq9z7OpLxXd6p/EVj2y3hyvGGOAByZVBbAA5E4AGAP/9TfxxprP11HNIXu5PUv0nnalaIunX7VWm1XY1qM2g+cweQadT9nsar3CgP1mZll4CXGKbHdwz9JusQXwCg6KCCoKodM9Ody0mz+iV8r6qOWWO53N7dhSv5ozW2dklCnHL1wFccgeHrLkrxbm7ddXdL1+EPbdlReji10+2hdiWDCVpqe5xwBA4yoQxyseLIcsMZUsHRBcUVcuyHiP3C7R4zTmMa5xlooz1qs1LIxqu08d2esFFSFSetiGVKU50jHKmcQKYQEQAejo4mit3TSJvfJNRg4+X3Jf+/Bx8oGqReqp6y17uqKd0ZGqK3sGUspF9psq6gkqy5GQceft1xuRfl7dFD/ADT24/5QUDkh258JG/8A91tX8vn1CN5/oXsf+tePvcOrjynvu14gUA9xHT3e3sAe4j/FfFP3AHuPIEvwQ9OfrFaP76jU7X4fN4n2firdPvK6XZsXebhifpZ9NbI1OfOIuVLtNsc2YukHrpGMs9YlMX1Kr2muTARbxseaqUu6YOGUnHqHBFydqZNQv2AEJ/DJS1XW27qpjlansltJGA3hzK8zocEECRA6up815A6iu2NvNcOhFfZ71FPBRXK73YBu8cjwM9ErPE/ZlV2jaPkMZAYd1Pf+hBi7KFGvi16p9VmGbyyYStLDF+TIFumu3XqVxCk1K7NopVB0Uqp2b2p3GNetFy+dFdu5KJTmEpwL/My7W2uojDW1anwK6Np4XOD4iGWSMt27Bg8bqy9iCPLBGf6IWqrpZoWo6YFGpCsLLggKVRSOPL3ycSArgkHBGcqwEr81GtrrNvuZ08dusu9bPUjczHdCgJTXnFSeEE71cHWQqxDTEYSnWLJD60FbVF67Tn5QGrCxtfKCKRgceqJS/wAk3bozZ3ULalp6L3bZtwndb/VGpMcYidgfESMJ64XgMlT5kYxn5M0vuXZl8uPUmh3NSR5t0AgDNyjAASZHfsXD9lDdgjZ7DPc8V6JeHqz8TqalMNUrQ9OVPY/9NhX43+HEpqSUgX4mNRxr8cFh+P8ArAAU4Xnw3b6FD1PV9L25YR/CCsR6aY8WT8ofuf6Pjw3z4n9F43jceGMf4xx5e/8AVxnUIHRe4jfHiY/yP9I58cx+H4eeXhcPE58fD/M58PPifnfL1tW5w10ptt1uqD1IMyZNq0JTNcttsV7bYtpmTY29VWyWJs0zepUY2sy4Ulo/JOsnCMYwcr' +
						'ek4KkCQoAmcxfOURid36q7TXpltyz2yV5tw2mroJ5ITHIik0wYuviFeBHIgZBOc5A88SSh6d3uTeN5r61TFbLlS1sCyAxtxFRJJwYASF88GU4KKAcjPYE0rwn00OrXrFi64ao4+0G6ed1Usn1tpiG8NkUo8pkVlU7Wd5GLT8ZITk62trNtERUgr9GN1K6Lpih6aRm7oyJDGmN46k9Kdx3OHdNffr/AIvDkNtTxVhLx4IUqsZjPIgcyJuLHPrqCdR6n2Pvu3U1RZae1WyYzeIvprpFJKFlzyxK9QJOxY4b0fJ994ecKvzsnRb6hg9IeM0/jMU0qRzZCdQC35oJCJ5gphYV7idxg11QYu0o2iQdNGXxEjYfKUrFb035ETgoqmT7RS/sHWfp/+Vh92vVTLZpNvpTcjBJyE4qvFKFApPZP1hlc5AJ7E/n5MN1/iTFY5YlNel9FUeDRYMfoqRlgDIigeIGAXlywAeIBwusSz4Hkcm6UzWs9leErEzedY3eFJqRQMSTSrsxPYvNSnr5IzdRNKSSh5BwZQPIcpVyp+wgBu/OVKa/R2zeqbmpl8WCC5ipUH1eapP4gHf3vIDHzZ1fFTZJLjs99u1B8Oaa3GnYjvwZofDJ7efEnPz41nq6XWsfXG0gs+KdV5LG2vkXp3BZukbjlnJRbVVLNZJqnTp1lbSlQ1CWRvZiJzb5qku0ReV5u9aFVMmdYoAUAv7qfubonvamqd0RVNe273ohHDDwkRFkUHgZPUKernDFZipA7Anvqq9iWTqVtioFjmhhXb4nV/EzCxKlog65EhcjwlcKfCVmkAY8AeIhvAfSO38oujPV6wpZsSVRjkja+34elsEwqOWaK+Z2phU8uWi1WFWSmW0geOq5mcDJIKplfnSMuc4plADFHm6vvVrYdbvfaV5pquVrfaoqpalvAlBQyU6ImFK8ny4IPEHA799aa2dOd00u2r5bZYAKmuFB4Y8SLuYZ2ebBEhXCoQVLshc9sLpnFa0e2ajugS+0YeUeFT2dW17udBToxbtW1IU1omMgzs7HsBu5XX1ZKktEvUlDLet6JDmEgm7gI8rKq3ttuXrsu90nf8W/To5PE8N+XFYFQnw8c/fAjHHOO+NWBRbYu8PTL8WZI8XTi448kzg1TSDvz4cvDIOOeM+ry9urZ9H3XDMGpnT9wrgfPFdj6plCnSGT3FhgouwxFqZM0rLlO5WeHMlOQS7mMdi5hZhuqYEzmFMxxIbsYogEX6v7ktG7d/wBbfrFI0tsmSAKxVkJKQRo3qsAwwykdx38x21temdguG2dnU1mua8KyJ5iRlT2eZ3XujOvkw8mPz4OQP//V3V5eyEXGFIWtfw6Ltc1kolYYtFzmTTcSN6vNdpLEgiUSmESObAU/YB/s/wB3JBtiyHcN3Ft5FUFPUTMR7Fp6eWdv+EZH8dQ/fW6l2bt1r2UWSQ1dHTopOAz1lZBSJ8/Zpwf4aqu/+Z7V/wACV0/P2kcsWH4vlR9cYP5dPqoJ/jc03+zqo/nFPpS16QQP0pers9UQRUdt9n9t2DdyqkRRdFg5yJRXDlikscBUTZOF0inUSKIEOcoGEBEAELyiZj106fQknwm27ayR7CfRqgAkfKASAfPGqisEafkX6gy4Hi/jtXjl7cel0B7Hz8wD+0DUMbJaz4Qb5N6COt0bR0YTCtnTvVilaVDTE6ySNN3SGx5k2yPm0uMktONBlbyUHyhE3BSB3MkmBEzeUMnae7L/AB2bqbunx+V+glhjSVkQ4SFqiCNeHEIeMXqAlSf1jk9ztdx7as1fdenFhqoma1z295HXm+Weo9CaVufIv67OxwGAXPq8QABCKDdutsV4mKSVbonkYbAmTWMQ/MmX4uMY2WCny2NoyX7edFvPFgWIOi9+yvwaXf8Ak++c8867S6VUgdvRHuVO7Jn1WdCnBiPaU5NxPs5H5dbD0GiO5N+3Xwk91Ft9XEJceuI2mflGD7FbAyMd8aUBtqAF6IfSkAoAAfrNbbewAAB72qwiPsHt7iPLB238Oe6vo63/AGa68aok9J7aT55rftotaM9RtgZOi+JX6mGubl+qFUz/AIlw5d2cX37olyHhbCuGitZAhTfzar6j3ORSWMXsKhWSAG7gQvbmXdthirPweNuX9QPSqGonQn/0p6ioDD/5Ejx8mT8pze9iuUsfUK4UB/oZMKTk5yIo5IlA8gqgVJ9neT2+zT/zmrVraONNHGmjjTRxpo400caaONNHGmjjTRxpr//W2q7gQruZxFEqM/MYYLNWu1mdJkAxjHjoLO2PX0kPYoCIlQZEOqP9xUx5YvS6ripNzyiXH560XOJf68luqVT+1sD+OqY682+ev2LA0Hc024bFUMPljhvNC8n/ALUDN/u6i1/8z2r/AIErp+ftI5IIfi+VH1xg/l0+ovP8bmm/2dVH84p9KbvHynOr5+KrbH9/qTy8Yfh46e/Vy1/d6jVS2D4FOoP13rvvVDr9WyoD+t74fA3b2GCkS9/8f0VY8Ht/85p9r/oN1T+X0kfeKnUvu36W9MvotP7qDVIWn9PvidP8jrn/ALHdeSuX9GulH7/D/wBPXz/nXf8A+6VX2z6T3tv8kTpS/ia22/eqw8sbbfw57q+jrf8AZrrW1PwT239tb9tFpzmFqrL2Dxc2xU1HILKsKJhd/Y7AskA+RrHyetOBqezM5EPYEl5ixoFL3+84B2+7lDXyoig/BZt0chHOaoCL+0VtRIcfPxRv4Z1ctoUt1RrAM5Eitj/VFIUJPygNIg7eRYa2K85J1dOjjTRxpo400caaONNHGmjjTRxpo400caa//9ffi5atnqB2zxug6bqeQVEHKRF0TimcqhBOkoUxDCRQgGDuHsYAEPcOfcckkTiSJirjyIOD8nmPm7a8poYaiMwzqrxHGQwBBwcjse3YgEfOM6og/wDmfVj8CN0/P2kcuKH4vlR9cYP5dPrnWf43NN/s6qP5xT6U5ePlOdXz8VW2P7/Unl4w/Dx09+rlr+71GqlsHwKdQfrvXfeqHX7tlf8Aq38Pd/4eT/KfHfNLtf8AQjqp+8D7xU6l93/S3pj9Fr/db9Udaf0++J0/yOuf+x3Xktl/RrpR+/w/9PXz/nXf/wC6VX2z6T3tv8kTpS/ia22/eqw8sbbfw57q+jrf9mutbU/BPbf21v20Wtrmo+lj6hdQXqN733BkijJbKz2FscYbKKiKzlLDWMMNY9b2CwmM3XWKgS+ZLScFKioBFitYFsoYoAqAc4h3bvBK3Ym39k0jZS3xzy1Hnjx5aiYohyB3iiOSRkZlI/VOunbDaDHe6+9SIVErhY849ZSkXORSCTwfhEoDYIaJjjDDTVeVdqY6ONNHGmjjTRxpo400caaONNHGmjjTRxpr/9DfxxprgHOMaW5ydFZjPEFJkSIpMxjlvYUl3Caq1Lm5qJsbyCeNyqfCu2yc5CoOUTHJ6iKnn8hgKocDblNwXaPb8m1hLmxy1aVRjIBAnjjeISKcZBMbsjAHDDGQSoIjcu0rDLu2LfJhxueG3yUQmDMC1LLLHO0TrniwEsauhI5IS3EgOwKG8+Y3veJOlV1ZI3JdYf011Yc+7F32vDLKMvhp2lXK30WSrFni3jV04buIuXbnEpBExVCLJnTOQpyCHOqLBfLRuHrlsKeyTpUxw2Sggk45zHNDDULLE4IBDoe58wQQQSDnXKqbbvu2uj2+qO+UstNPU7uqaiENj89DPU0BiljKkhkc5VfI8lZcAjGuU2WOA7a+HrOQxTFPEPxAwCBimKfFOOvcBD2EBAfYeee1h/kT1UB8/SB94qdbS7EHdnTEjy9yl/ut2qRxCS0jsX4myKjW7qUlXmFbW2ZRMW1cycs9dO4q5NmjVjFsEnEg9dO3KhU0kkkznUUMBSgIiADK6h0i2t0pmlIWFa6IlmICgDwySScAADuST5a+4oJprzvuKJSZJaepVB/pN47jAJ7eZGe+BkZxqe9Kujw52u0A6bVG3Jgrni2q655Xz/me9YInoVzX7rk1rfLbKuce1exPgkW8nRas/Zrkfy7cUAlnTMxWYCyOqosnBt+9YI9r9Q9yV2zpIaqruFLSU8VSjB4oTFGBK6jBWVwfVjIbgGHI8wOLWDsvYc1w2ha6O+J4dPC1Q0kbK6yMssiOnHPEpkIQxZeXFsx8XCSLqrRRSbopN0EyJIoJkRRSTKBSJJJFAiaZCh7FIQhQAA/qAOcoszMxZjlicn9ur1VVRQqjCgYA+bX2c/NfujjTRxpo400caaONNHGmjjTRxpo400caa//R38caaONNeRPV+BtUNJVy0QkTY69MtFo+YgZ6OZzENLMHBfIuyk4uQRcMX7RYvsdNVM5DB94cyKWrqqGpSsopJIauNgyOjFHVh5FWUhlI9hBB1j1VJS11O9HXRRzUkgwyOodGHyMrAqR8xBGontmtmBr1b8PX62Yqp0zb9f1nDjCs4vFkRc40UdM2keuFTTambtYxIWTBBIqZUxIQiRQKAAAc3VFu3ctvoa+20dbUJQ3PHpacsifBY5kJyWOWYk5ySTnWpn2xt+pqaSrmpITPQKFpyBxESgowVFXChVMaFRjC8RjGNdJTcM4jx5Z7zd6' +
						'JjOiVC55OlzT2RbdXarCxNnvMwb3+Ptk+yZoys8uQfcnxKqgEEREoB3Hvh119vVzpKe33GrqJ6CkThBG8jNHEvyRoSVQf1QM6zKOzWm31M1ZQ00EVXUMWldUUPIxOSXYDLZJJ7nGST5k5kvmp1stHGmjjTRxpo400caaONNHGmjjTRxpo400caaONNf/Z';
						
					doc.addImage(pic, 'JPG', cord_y, cord_X, 25, 12);*/
					/* BRANDING */
					doc.setFont('arial', 'bold');
					doc.setFontSize(10);
					doc.text(cord_X, cord_y + 15, 'VOLANTE');
					/*doc.text(cord_y, cord_X + 18, 'GENERALES S.A.');
					doc.setFont('arial', 'normal');
					doc.setFontSize(6);
					doc.text(cord_y, cord_X + 21, "value.SGetentdevCab.Znemp");
					doc.text(cord_y, cord_X + 24, "value.SGetentdevCab.Znemp + ' - ' + value.SGetentdevCab.Znemp");
					doc.text(cord_y, cord_X + 27, "value.SGetentdevCab.Znemp");
					doc.setFontSize(12);
					doc.text(85, 20, "value.SGetentdevCab.Znemp");
					doc.text(90, 24, "'Nº ' + value.SGetentdevCab.Znemp");*/
				};
				var setSubCabecera = function (value) {
					var coordX = 12;
					var coordY = 22;
					doc.rect(10, 17, 130, 35);
					doc.setFontSize(6);
					doc.setFont('helvetica');
					doc.setFontType('bold');
					// Primera Columna.
					doc.text(coordX, coordY, 'Nro. Vuelo(A) : ' + value.NumeroVuelo);
					doc.text(coordX, coordY + 4, 'Transportista : ' + value.Transportista);
					doc.text(coordX, coordY + 8, 'Doc. Transporte(AWB/BL) : ' + value.DocumentoTransporteCodigo);
					doc.text(coordX, coordY + 12, 'Proc. Origen :' + value.ProcOrigen);
					doc.text(coordX, coordY + 16, 'Embarcador :' + value.Embarcador);
					doc.text(coordX, coordY + 20, 'Consignatario :' + value.Consignatario);
					doc.text(coordX, coordY + 24, 'Contenido :' + value.Contenido);
					doc.text(coordX, coordY + 28, 'Ubicación :' + value.Ubicacion);
					// Segunda Columna.	
					doc.text(coordX + 65, coordY, 'Nro. Manifiesto : ' + value.NumeroManifiesto);
					doc.text(coordX + 65, coordY + 4, 'Fec. Llegada :' + value.FechaHoraLlegada);
					doc.text(coordX + 65, coordY + 8, 'Fec. Termino Descarga :' + value.FechaTerminoDescarga);
					doc.setFontType('bold');
				};

				var setHeaderDetalle = function (value) {
					doc.setFontSize(6);
					doc.setFontType('bold');
					doc.rect(10, 56, 130, 7);
					doc.rect(10, 56, 130, 14);
					doc.rect(10, 56, 130, 21);

					doc.line(32, 56, 32, 77);
					doc.line(56, 56, 56, 77);
					doc.line(72, 56, 72, 77);
					doc.line(88, 56, 88, 77);
					doc.line(104, 56, 104, 77);
					doc.line(120, 56, 120, 77);

					doc.text(15, 67, 'Bulto');
					doc.text(15, 74, 'Peso');

					doc.text(34, 60, 'Manifestados');
					doc.text(58, 60, 'Recibidos');
					doc.text(74, 60, 'Faltantes');
					doc.text(90, 60, 'Sobrantes');
					doc.text(106, 60, 'Buenos');
					doc.text(122, 60, 'Malos');
					doc.setFontType('normal');
				};

				var setFooter = function (value, cord_x) {
					doc.setFontSize(6);
					var vSpace = 2.5;
					doc.text(10, cord_x + vSpace * 1, 'Fecha de documento: ');
					doc.text(40, cord_x + vSpace * 1, value.sfecha);
					doc.text(60, cord_x + vSpace * 3, 'Recibido VoBo');
					doc.text(80, cord_x + vSpace * 3, '-----------------------------------------------------');
					doc.text(10, cord_x + vSpace * 3, 'Doc Identidad: ');
					doc.text(40, cord_x + vSpace * 3, value.sDNI);
				};

				var setDetalle = function (Detalle, nroExceso) {
					setSubCabecera(value);
					setHeaderDetalle(value);
					setBranding(value);
					// Impresion del excedente
					for (var z = 0; z < nroExceso; z++) {
						doc.text(36, 68 + 7 * z, Detalle[z].Manifestado);
						doc.text(60, 68 + 7 * z, Detalle[z].Recibidos);
						doc.text(76, 68 + 7 * z, Detalle[z].Faltantes);
						doc.text(92, 68 + 7 * z, Detalle[z].Sobrantes);
						doc.text(108, 68 + 7 * z, Detalle[z].Buenos);
						doc.text(124, 68 + 7 * z, Detalle[z].Malos);
					}
					//setFooter(value, 94);
				};
				// DETALLE
				var xDetalle = value.aDetalle;
				if (!(Array.isArray(xDetalle))) {
					var Detalle = [];
					Detalle.push(xDetalle);
				} else {
					Detalle = xDetalle;
				}

				var nroExceso = Detalle.length;
				setDetalle(Detalle, nroExceso);

				//terminosCondiciones(value);
				doc.save("DOCUMENTO-" + value.Titulo.trim() + '.pdf');
			};
			$.generarOrdenCompraPDF(value);
		},
		onGenerarEtiquetaPDF: function (value, tamanoHoja) {

			$.generarOrdenCompraPDF = function (value) {

				var doc = new jsPDF('landscape', 'mm', tamanoHoja);

				var setBranding = function (value) {
					var cord_X = 10;
					var cord_y = 5;
					var pic =
						'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCACgAfQDASIAAhEBAxEB/8QAHgAAAQUAAwEBAAAAAAAAAAAAAAEGBwgJAgQFAwr/xABYEAABAgUBBgIFBgYNCQYHAAABAgMABAUGEQcIEhMhMUFRYQkUInGBFTJykaGxN0JSdYKyFhcjOENVYnN0kqKz0RgkMzRjk5S0whk1VnbB0jZGU4OV0/D/xAAcAQABBQEBAQAAAAAAAAAAAAAAAwQFBgcCAQj/xABBEQABAwICBggCBwYGAwAAAAABAAIDBBEFIQYSMUFRYRMicYGRobHBMtEUFUKCkuHwBxY0NVJyFyMzwtLxU1Sy/9oADAMBAAIRAxEAPwDVGCCFxAhEJCwQIRBCQQIRCwYggQkzBC/CCBCSFhO3SFgQiEhYMQIRBB2ggQjtBBBAhJBC4ggQiAwQQIRB2ghIEJYMQQQISYhfjCQsCEQQQQISQdIWDHlAhEEEECEghe0EGPKBCIIIIEIhIWEgQlg7QQQIRCQsGIEJIX4QfCCBCIIIIEIggggQiCCEgQj4wveCCBCISFggQk6QsGPKCBCIQQsECEkL1ggx5QIRBCGCBCIIXEBgQkg6QuIIEJMwQsECEmYMxH+s+stJ0ZtxE/PIVNz0wotyci2rdU8oDmSfxUjIyfMeMVEq22jqLPTi3ZN2n01gn2WGpQLCR5leSYi6rEqekdqPNzwCuOD6J4njcRnp2gM2XcbA9mRJ7div3mDMZ8f5Yup38aSn/ANf4Qf5Yup38aSf/ANf4Qy+vKXgfD81Yf8ADnGf6o/xH/itB+kHxjPj/LE1O/jSU/4Fr/COTW2Pqa24lRqMk4AeaFSLeD5HABg+vKXgfD80f4c4z/VH+I/8VoLmF6RAWz/tSS2qs8mg1qVapdwlBU0WSeDNADJCc80qA54ycgHB7RPgiZgnjqWdJEbhUHEcNqsJqDS1jNVw7wRxB3hLmEzCwQ4UWiDPnBB0gQkhcw2r21HtzTqQE3cFVYp6FZ4bajvOufRQMqV8BEC3JtzUiWcW3Q7cm59I5B6ceSwD57oCjj4iJKlw2rrBeCMkcdg8TZQldjWH4adWqmDTw2nwFz42VnswRS+Y25rnWv8AcLdpLaewcU6s/WFCPvI7dFfQseuWzTX09ww642ft3olDo5iNr6g/EFBDTTBibdIfwlXJzBmK4W3tvWtUFobrNIqFIUrkXGimYbHmcbqvqSYni1LtpN8URmr0SdRP094kIeQkp5g4IIIBBHgRETU0FVR5zxlo47vEZKw0OLUGI5UkwceG/wADYr18wZhYQkJBJIAHOI9S6MwZiOrp2hdP7QUtucuSVffTy4EjmYXnwO4CAfeREX1vbktyVUpNKt+o1DHIKmFoYSfq3j9kSkOF1tQLxxG3Zb1soGpx7C6M2mqGg8AbnwF1ZbMGYpzO7dVaWo+p2vItDtx5hbh+wJjpJ25bqCsrt+jqT4Dig/rxJDRzESL6g/EFCnTPBgbdIT91yunCZiptE268uJTWLU3W8+05JTWSP0VJ5/XE7ab63WlqkgootRxPJTvLkJpPDfSO53eih5pJER9VhNbRt15ozbiMx5KYocfwzEXCOnmBcdxuD3A2v3XT8zCwnWFiJVgSZggPIRH11a/WBZxW3P3LKLmEcjLyZMw4D4EIBwffiFooZZ3asTS48gSm09TBSt153ho4kgepUg5gzFbK5tx2zKKUmlUGpVEjop9SGEn7VH7IZ09t1Vhaj6na0iyntx5lbh+wJiajwDEZBfordpA91WZtLcGhNjPfsBPsFcTMGYpanblure9q36OU+A4oP68e9RdutfESmr2oNz8ZySmuY/RUnn9cKv0dxFovqA9hH5JGPTLBnm3SkdrXfmraZ5QsR7ptrtaGqWGaTUCzUcZNPnE8N/HfAzhX6JPnEhdYgJoZKd5jlaWngVbKephq4xLTvDmneDdJmCFghFOUmYMwxtVdb7I0UpCajeVwylGaWDwWFkrffI7NtJBWr4DA74in98elotqQfcZtOyKjWEA4TM1OZRKJPmEJCyR7yIcRwSy5sak3SMZtKvzBmMup30tN+uOEydlW4w32S+t9wj4hafujs0n0tl4Mug1OxKHNt9xKTLzB/tb8OPoE/DzSX0iPitPMwZikFjelb0/rTjbNz23WbaWrkXpconWU+ZI3V49yTFvNPNRrd1WtWVuO1ao3WKNMlSWpptCkZKThQKVAKBB5YIENZIZIvjbZLNe1/wAJTjzBCxxUoIBUo4A5knpCK7SwuYhHUDbR0Z02U6zU75p83ON5BlKTvTrmfyTwgpKT9IiICuv0s1kSC1ot6za3WSOQcnnWpRJ8+RcOPgIcsp5n/C0pIysbtKvVBGZNV9LddDqz8maf0iWT29bnXXj/AGQiPMb9LPqEFguWdbKkeCfWEn6+IYX+gT8PNcfSI+K1JzBmM6bU9LiC+hFy6ebrJOFv0qoZUnzCFoGfdvCLc6H7VenO0C2W7WrYTVkI33aPPp4E4gdyEEkLA7lBUB3MISU0sQu5uS7bKx+QKl6EzCwQ2SqITlBCwISde8ELkiCBCSFzAIOsCEdYQQuYSBCI+czMNScu6+84lplpJWtxZwlKQMkk+AEfX4wz9WrNntQNPaxQKdUjSpqda3Ev7uUqGQShXcJUBukjsT16Rw8lrSWi54JxTsjlmYyV2q0kAnbYXzPcFQLXvVJ3VnUKcqaFK+SpfMtINns0D87Hio5UfeB2iOY9q77Oq9h16Yo9bklyU8weaFdFDspJ6KSexEeNGVzOe+RzpPiJzX2jQQ09PSRRUlujDQG2zBHG+++2/ElEEHaCEE/SQsEECF3KLWJu3axJVSnvKl52UeS8y4nqlSTkRpvpTqFJ6oWPTa/KEJU8jcmGQcll5PJaD8eY8QQe8ZeRPWyLq3+wO9zQag9uUWtrS3lR9lmY6IV5BXzT70ntE9hFZ9Hm6Nx6rvXcfZZvpxgX1rQfSYR/mxXI5t+0P9w5g8VfjpBB1EGPONAXy+khg616py+klkTFWWlL886eBJS6jycdIOM/yQASfdjvD/ilW3BcTs7ftGowWfV5GR45TnlxHFnP9lCfriawejbXVjIn/DtPYN3fkq1pHiL8Lw2Soj+PIDkTlfuFz4KBLoumqXnWpirVmccnp59WVOOHoOyUjoAOwHIR5UEOjTWwJ7U28ZG36etDLswSpb7gylptIypRHfA7dziNmc6OnjLj1WtHcAF81MbNVzBrbue89pJKa8EXgo2xPZUnLoFQn6rUX8e0sOpaST5JCSR9ZhansT2RNIPqk9V5FeORDyHB9RR/6xWv3lw/Wtd3bq/mrr+5GMautqtvw1hf0t5qj0X62P8A8CNP/pUx+uYie4theqMBS6Hc0pN+DU8wpk/1k733CJ42erFqunOmstRK0223PMzDyyGnAtJSpeQQR5REY9iVJW0QbBICdYZbDv3FWHRPBcQwzFC6riLW6jhfIi9xvBKkuOpVf+65z+ZX+qY7eY61TSpdNm0pBUotLAAGSTgxnrdoWwv+E9iyjgiWrX2WdRbnShz5GFJYV/C1N0M496Oa/wCzEo0PYUmlpSqsXU00ru1IypX/AGlKH6sbXNjFBT5PlF+Wfpf1XzHTaOYtVi8dO63E9X/6t6KqkEXYldh6z2kDj1msvq7lK2kD6twx0Lh2G6G7IumiV+flpwJO4J1KHWyewO6Eke/n7oYDSTDi62sfwlSrtC8Za3W1GnkHC/671TaOzTKnN0WoS89ITDkpOS6w40+0opUhQ6EGPtXqJOW1W56kz7XCnZJ5TDyM5AUk4OD3EdCLKNV7b7QfRUoh0T7HJwPeCPcELRnZ/wBVhqxYTM/MbqatKq9WnkJ5AuAZCwOwUCD78jtEme+KW7DtdXLXzXqQVHgzcgJjd7b7biQPscV9UXS++MWxmkbRVr4mDq7R2H9FfTGjeIPxLDIp5Tdwu0niW5X7xZfKY/0Dn0T90ZTT4/z6Y/nFffGrT4yw4BzJScAe6M+6HswaiXXNuuoonyZLLcUQ9U3AyMZ/I5r/ALMWDRiohphM6Z4aOrtNuKqOnFHU1rqZlNGXnrbAT/T4d5CiIQRaihbCk64lK6xdTDCu7cjLFz+0op/Vh3ymw9aDSB6xWqy+vuUraQPq3D98WiTSHDozbXv2An5KjQ6HYzKL9EG9rgPmqUQRcyu7DdvvSbnyRX6jKzePYM2EOtk+YSlJ+2Kj3Rbc7aFw1Ci1JsNzsi8pl0JOQSO4PcEYIPgYfUWJ0uIXEDrkbiLFROJ4HXYQGmrZYO2EEEX4ZLpSU7MU2cZm5R5yWmmVBbbzSilSFDoQR0MaEbOerStV7FS9OqT8t09Ylp0J5b5xlLmO28M/FKozwiw2xNXVyGplRpm8eDP09RKexW2oFJ+or+uI7SGjZUUTpLdZmYPLeP1wU1ofiMlHibIb9SXqkbr52PaD5FXfiHdqnaEktnHSmcuJxtE1V31eqUuSWeT0woEgq77iQCo+Qx1IiYoyw9K9er9U1htm2EuEyVJpImijPLjPuK3jj6DTf1mMrpYhNKGnYvoSV+owkKn9+6gXBqhdM7cVz1N+rVabVvOPvqzgdkpHRKR0CRgCG9BD/wBCtG6tr1qbSbNo7rcq/OFS3Zt4Eol2UDeW4QOZwByHckDlnMWolrG3OQCic3FMCCNYLa9FZpXTJFtNYq9w1qbA/dHUzDcugnvuoSgkDyKj745V70VelFRaUKbV7kpLuOShNNPJB8wpvP2ww+sIL2z8E4+jyLJ2NivRqfvUqH/T53++MVzvT0SdwSqXHLTvun1HqUy9XlVyx92+guA/1RFvti/Sa4dEtCafad0MsMVaWnJpxQl3g6hSFuFSVBQ8Qe+DDWsnilhsx180rBG5j+sFOkebcnO3qp/RXf1DHpR59wNqdoNSQhJWtUs4lKUjJJ3DgCIQbU/K/O/BFjrA9H1rbfqGnjbCbclHMf5xX5hMsR72hvOj+pE/Wn6I+fcQhy5tQ5eXV+MxSqep36nFqT+rFsfVQs2u91Dtie7YFnnBGqlP9E/pmw2PW7nuebc7lDsu2n4DhH748e9PRM2nMUl9VqXjWJGphJLSaqlp9hSuwVuJQpIPiM48DCIr4CbX8l39Hk4LMOO/Qa9UbWrMnV6ROvU6pybqXpeal1lDjax0IIjsXdatSsa6atb1YY9WqlMmnJSZazkJcQopOD3HLke4jyIf5EJvsW3exztCjaK0flKxOcNFxU9z1GrNNjALyQCHAOwWkhXkd4donSMuvRNXc9Jar3hbRcIlqjR0zwSTy4jDyUjHnuvq+qNRoqlVEIpS0bFLxO12AlJBCwQ0SyQwQuYIEJIIXrB3gQiEhYTECF5F33TIWVbNRrlSc4UlIsqecPc46JHmTgAeJEVc2e9qSdreoVRpV1TATKVuaLkgtavZlHTySzn8ggAD+V9Ix8NuXUSZM/S7LllluVDQn5vB/wBIolSW0nyG6T7yPCKnpUUFKkkpUDkEdQYp2I4m+KqDYjkzbzO/yW86LaIU9ZgsktYOtOOqd7QL6pHMnM8RlvWm+rOjtC1foJkaq1wptsEytQaSOLLq8vFJ7pPI+RwYz41P0qr2ktwrplZl/YVky042CWphH5ST4+IPMRc3Za1yGptsfI9VfzclLbCXCo85lnol0eJHIK88HvEqX1YdF1Gt5+jVyTTNSjnNJ6LaV2WhX4qh4/DpEhU0cOKRCeHJx3+x/XkqzhOO1+h1a/Da4F0QOY4X+0y+4jO2w8isrY+jEu7NzDbDDS3nnFBKG20lSlE9AAOpiwdS2K7vbvlVLkH5d2gK/dEVh5QAQjPzVIHMrHgOR8R2s7pJs+WtpIwh2Sl/lCs7uHKpNJBc8wgdEDyHPxJivU+EVEzy141QN59uPotRxTTnCqCBslO/pXuFw1v+4/Z7Pi5b1WHTzYtuu6pZqdr021bEq4ApLDiC7MkeaAQE/E58REhTewXSfVVcC7Zxt8D57sqhSPqCgfti1XKIO2odcJTTi0pmiyEyldy1NotNNtqyqWbUMKdV4cshPiefQGLC/DqGkhL5W3tvJWWU+lWkeN17IKSTVLjkGtFgN5NwTYDMkn1CoPUZVEjPzUu08Jhtl1baXkjAWASArHbPWOuCpKkqSSlQOQR1BhB0gih719KgEAA5rRfZn1ZGqenrJm3QquUzdlZ4E+0vl7Dv6QH1hUS5GbeznqJM6d6p0l5Cz6jUHUSM41nkpC1ABXvSrB+B8Y0kjRcLqzVQDW+JuR9ivlXTLBBg2JHohaOS7m8s8x3HZyI4IiiO2bKOS+sQcWCEP05hxB8QCtP3pMXuiuW2TphMXRbEnc1OZL03RwpMyhAypUurmVfokZ9ylHtF70fqGU9e0vNg4EeOzzCw/S+jkrMJeIhcsIdbkL38jfuVJ4fGjGo/7VeoEhXly6pqVQFMzDSPnKbUMHdz3HIj3YhjwRrssTJ43RSC4cLFfPFPPJSzMniNnNII7QtO7I1PtjUOTS/QavLziinKpfe3Xm/JTZ9ofViHVGT8tNPST6H5d5yXeQcpcaUUqSfEEcxEv2NtXX3Z/DZmZxFwSScDg1IFS8eTg9r68xnlXorI27qV9xwOR8dnotjw/T2F9mV8Raf6m5jw2juJWgOIIiTSTaUtjVNxuRyqjVxQ/wBQmlAhw/7NfRXuwD5RLYil1FPNSvMczS081plJWU9fEJqZ4c07x77weRRBBHwnZ6Xpkm/Nzb7ctLMILjrzqglCEgZJJPQCG4BJsE7JAFyvvBFTNUdtNbU09IWRKNrbSSk1WdQTvHxbb8PNX9WICr2tV9XK4pU/dNSUFfwbL5ZR/VRgRbKXRqsqGh8hDAeO3wGzvKz6v02w6keY4QZSOFgPE7e4LTDrBGVTlw1V1RUupzi1Hup9ZP3xx+Xal/GE1/vlf4xJ/uk7/wAw/D+ahP8AEFn/AKx/EPkpA2l20t65XWEgAcds4HiWUExGMc3n3JhxTjri3XFdVrJJPvMcIv1PF0ELIib6oA8BZZLVziqqZJwLa7ibcLkn3U67GZI1lGO9OfB+tEXviiGxn+GVP5vf/wCmL4Rl2k/8f90e63bQb+U/fd7IhI+c1NMyUu7MTDqGWGklbjjiglKUjmSSegirOqu2giTmnqdZEo3NbhKVVWcSSgnxbb5ZH8pX1d4gqKgqK9+pA29tp3DtKtOJYtR4TH0lW+19g2k9g98hzVq8QRmlcGt9+XM4pU9dNS3Vfwcu8WED9FGBDXcuKqvK3nKnOOKPUqmFk/fFsZonKR15gDyBPyVAl/aBTh1oqdxHNwHsVqrGeu1a2lGu9yBIABEsTjxMu3EZfLtS/jCa/wB8r/GOo/MOTTpcecW84rqtaiSfiYnsJwM4ZOZjJrXFrWtvHPkqnj+lLcbpW04h1LOBve+4jgOK4RM+yGSnXCl47y0wD/ujEMRM+yH+HClf0eZ/ulRMYp/Azf2n0VcwL+a0397fVX+jIj0oNNektpwvuJIbm6LKPNHsUguIP2oMa7xSL0negk5f1h0y/aLKqmajbSVtzzTacrXJLIJXjvw1DPuWo9oxyieGTC+/JfT07S5mSysiWdlzXAbPWslIu52TXP09tDkrOy7RAcUw4MKKM8t4EBQB5HGMjOYiaCLM5oe0tdsKiwSDcLe/SrXuwtaqY3OWhcklU1FIU5J7+5NM+S2VYUn34x4ExIMfnXkKhNUqcam5KZek5po7zb8u4ULQfEKHMGLLaS+kS1c0z4EtP1Nq8qU3gGWraSt0J/kvpwvP0ioeUQcuHOGcZv2p+2pH2gtkoIrjs4bc9g7QbzNISpds3Ysf9z1BwEPHvwHRgOe4hKuvs45xY6Il7HRnVeLFO2uDhcJIWCOpVqtJUGmTVRqM2zIyEq0p5+ZmFhDbSEjKlKUeQAEcLpduCM5tf/SlusT01R9KKay4y2S2bhqrZVvn8plnly8FL6/kiKeXftT6t30+tyr6g11aVnmzKzapZoeQQ1up+yJOOgleLuyTV1QxuQzW7mIQx+el69LgmFlbtdqbqz1UuccJ++Pn+y2ufxzUP+KX/jC/1Yf6/JJ/SuSmTbraQztZaiJQkJBnGVYHiZZok/WTEDR9ZmaenX1PTDzj7y+anHVFSle8nnHyiaY3UaG8ExcbklW59F6op2nsDoqiTYP9ZsxrvGQ/ovv3zyfzLN/e3GvEV7EP9buCkqb4EkLBAYjE6RnyggggQiE6wQsCEkLBBAhUv25rHm5a56RdjaCuQmZcSLqx/BuoKlJz9JJOPomKuRqfqHY8jqNZtTt+oAcCcaKUuYyWljmhY80qAP2RSawdkC8Lor81LVdAoNKk31Muzjyd5T26cHgo/GB7KOB7+kUnE8PldU68Lb6/kd/zX0PofpTRx4SYa6QMMGWe9pvaw2kjMWHJRjplWLhoV80metZl6arTToLMuwgrLo6KQQOqSMg+Uaf0ibmZ6lScxNyipCadaSt2VWsKLSyMlJI5HB5ZENbTXSC2NKad6tQpBKH1JAennsLfe+krw8hgeUPTOO+BE7htC+iYQ9177twWbaXaQ0+P1DHU8WqGXAcficOY2AbwMznt3JY6VXrMjQKe9P1KbYkZJlO84++sIQkeZMdztFO9vGTqLNUtiaE2+qkvsuNeq754SXkKB3t3pvFKwM/yYeVlQaWB0oF7KBwDC24ziEdE+TUDr52vsF7Dmd18l2NYttMr49KsJspHNC6zMo5//abP6yvq7xVKpVKcrVQfnp+ZdnJx9RW6++srWs+JJjrdYIzupq5qt2tKe7cF9VYTgdDgkXR0bLE7XHNx7T7Cw5Igg+EHwhkp5SBoPY83f+qdBkJdBUyxMInJpwdG2W1BSiffgJHmRGmfaIM2TtI/2u7DTVJ9ncrlaSl93eHtMs9W2/Lkd4+Zx2ic40TCaU00F3bXZ/JfLOm2MtxbEyyE3jiu0Hib9Y9l8hyHNEcVoDiChSQpKgQUnmCI5QkTSz1Vd1i2OWqxNzFXsl1qSfcJW5SXzutKPfhK/F+ieXmByirt16f3HY8yWK7RpumqzgLebPDV9FY9lXwMai5j4TckxPy62JlluYYWMKbdQFJUPMHrFuodJKmmaI5hrtHHI+O/v8VnmKaFUNc4y056Jx4Zt8N3ce5ZQ/GCL8ahbJtlXky69TZY25U1AlL0iP3En+U10x9HdMUkvizajp/dVQoFUQlM5Jr3VKQcpWkgFKknwIIPxi+4di1NiVxFcOG0Hb+ayXGNH6zBSHTgFhNg4bL8DvB7fFeKw+5LPNvMuKadbUFIcQcKSRzBB7GL/wCzJq89qjZK2qk4F1yllLEyvu8kj2HPecEHzST3jP2LA7FNVck9VJ2SCiGZynObyexUhaCD9W99cNdIKRlRQveR1mZg+o7wpDRHEJKPFI4mnqSHVI8bHtB9Sryc4qLto6pzBnpayJB4tsJQmZqG4cb6jzQ2fID2iPNPhFuj0jNfXmpLqusd3PuEqUmoOMjPg2dwD6kiKXozTNnrC94vqC/few8Fpem9bJS4aIozYyOsewC5HfkOxMKO1S6XOVupS1PkJdybnZlwNMsNDKlqPIAR1YsFsU0Nio6ozs88gLXT6etxrI+atSkoyP0SofGNLran6HTST2vqi6xLDKL6xrYqS9tcgX4Df5Ar37S2HalOyjb9x3A1TXFDJlJJnjKT5FZIGfcCPOHkzsNWokDi1+srPcoLSf8AoMWSAgjJ5MexGQ36W3YAPYr6Ah0TwaFob0AdzJJPqPRZlavWZJ6e6j1q3pB59+UkVoQ25MkFxWW0qOcADqo9oZ8ShtN/h0uv+ea/uW4i+Nao3ukponvNyWgnwC+fcSjZDXTxxizWvcAOABNlOuxn+GVP5vf+9MXwih2xn+GVP5vf/wCmL4HpGZaT/wAf90e63DQb+U/fd7Kqe2lqnMSYk7Jp7xaS+2JqoqQcFSSTw2/dyKiPo+cVH6RJW0hUnKprZdTi1E8KZDCQewQhKR90RrGgYRTNpaKNrRmQCe05rIdIa2SuxOaR5yBLRyDTb2J7195CQmapOsScmwuZmn1htplpOVLUTgACLK2dsQVaoyjUxcddapK1gKMnKNcdafJSyQkH3bw84a+xtQ2KvrD6w8gLNOkHppsKGcL3kNg/AOGL4RXMexmopJhTU5tkCTa5z4XVz0T0bo8RpjW1g1hcgC9hla5NszmeKrazsM2qlI41frCz4o4SR9qDFZda7EktNdSarbtOfmJiUlAyUOTRSXDvtIWc7oA6qPaNLYz32sPw83H9GV/5ZqG+j2I1dXVuZPIXDVJtlxHJO9MMGw/DsPZLSwhji8C4vsseJKiKJn2Q/wAOFK/o8z/dKiGImfZE/DhSv6PM/wB0qLhin8DN/afRZzgX81pv72+qv9HB1pDzS23EJW2sFKkqGQQeoI8I5wZjDV9TrP8A2mvRky9y1CcuPSqYlqXNvKLr1uzatyXUo8zwHP4PP5CvZ58ikcooDqJo1e+k88qVu62KlQ1g7qXZlg8Ff0HBlCvgTG/0dWo0uTrEm7Jz8qxOyjo3XGJlsONrHgUkYMScNfJGNV2Y801fTtdmMl+dn6oI2G1o9HPpbqdLzMzQ5I2PXVgqRM0pP+bKV235cndx9DdPnGU2q+mVa0c1ArNn3A2hFTpjvDWpo5bdSQFIcQT1SpJSod+fPBiagqY5/h28ExkidHtTXlJt+QmmZmWeXLzLKw4260opWhQOQoEcwQe4jZLYN2k5naB0qcYrjwduy31olZ908jMtkHhPkeKglQV/KST3xGNAi5norbifp20FVqUlREtUqG9vo7Fbbjakn4Ar+uEq2IPhLt4XcDi14HFayRm16UjaBnFViR0opE0pmSaaRPVktqxxVq5ssq8gAFkdypPhGkkYXbXddeuLaa1Jm3lFSkVl+WGeeEsnhJHwCBEVh8YfLc7k7qHEMsN6iECPRt63qndldkaNR5J6o1SeeSxLSrCd5bq1HAAEedFzfRX2pKVvX2rVaZaS67R6M47LbwzuOOOIb3h57ilj9KJ+aTooy/go9jddwanhpv6JuuVOnMTd7XlL0N9aQpVOpct60tvPZTqlJSCO+6FDziTpb0TGniUj1i8LndV3LZl0D7WjF5YIrTq2dxvrWUmIIxuWDW0lpjTdGtbrpsykTM1N06lPNtsvzqkqeWFMtrO8UpSOqz0A5YiNInvbw/faaif0pj/lWYgSLNES6NpPAKMeLOICtx6L7988n8yTf3txrxGQ/ovv3zyfzLN/e3GvEV/EP9buCkKb4EdYISF7xGJ0jEEHSCBCQwsEBgQiEMEECEsJiCBQ3gRnGfCBCaOouq1taXUszlfqKGFKBLUq37b7x8EI6n3nAHcxSnWDarubUkv0+mFdvUFWRwGF/uzyf9osfqpwPHPWGrr9atbtLVGrydcnZmpuuL40vPTKypTzCidw58vmkDkCDEdxQcQxOolc6EdQDK2/vPyX0zoxojhlHDHXOIme4BwcR1RfPqt48zc3GwLRXZj1T/bN02lvWnt+s0vEpOAn2l4HsOH6SR18QqPL2xrW/ZFo1NTiEbz9JmWpxOBz3cltfwwvP6MVR2btUTpbqVJvzDpRR6hiTngTySlR9lz9FWD7t4d40IuyhM3ZatWpLuFM1CUclyeo9tJGftzE9STfWFC6J3xAWPhkfRZvjtCdF9IoqyIWic4PHIX67e657iFlIDnvBH2m5R2nzj8q+nceYcU2tJ7KBwR9Yj5RQti+lgQRcJImTZb0kOpuoDczOs79CpBTMzW8PZcXn9za+JGT5JPjERSUk/UpyXlJVpT8y+4lpppAypaicAD3mNLdENMGNKNP5CjJCVTyx6xPPJ/hH1Ab3wGAkeQiawqj+lT6zh1W5n2Cz/TTHfqfDzFEbSy3DeIH2ndwyHM8k/gMAAch4QQsEaGvlhERfrJrvT9GJ6iN1KmTM/L1IPEuSq07zXD3PxVYCs7/AIjpEoRUjbx/1iyvoTn3sRM4RTR1dayGYXab8txVb0irZ8OwySppzZ7dW2V9rgNnYpbt7ao04r4SDXDTHT/B1BlTWP0sFP2xIFNvi3ay2FyFeps4k88sTbavuMZbQRdZdFKdxvFIR22PyWYwaf1jBaeFruwlvzC1Cr2o1r2zKrmKpX6dJtpGSFzCd4+5IOT8BGfeud/yupeplVrkihSJBe4zL74wpSEJCQojtnBOO2RDBgiUwvBIsMeZQ8ucRbhkoPHdKJ8bibAYwxgN8iSSe09u4IiwOxTTFzWqs7NgHhylNcJPgVLQB/6xX6L2bI2l8xZFjPVeoslipVopdDaxhTbCQeGD4E5KvcRHuP1Laege0nN+Q9/ALjRKifV4tE5o6sfWJ7L28SVPHaM19d6eumax3eysEE1F13n4LO+PsUI0p7RS7bU0+epl2Sd2S7RMlUW0y8wsDkh9AwnP0kAY+iYpejE7Yqwxu+2LDtGfzWmac0j58NbMwX6N1z2EWv3ZKtcTJsqagSNhanp+VH0SshU5dUmqYcOENrKkqQVHsMpxntvZiG4MRptVTtqoXwP2OFlh9DVvoKmOqi+Jhv8Al3i61ibcS6hK0KC0KGQpJyCPGPIua9KFZ0oqZrVWlKYyBnMw6Ek+4dSfICMyZK7a5TZYS8pWahKsDlwmZpxCfqBxHnTM09OOlyYecfdPVbqypR+JijM0S63Xmy5DPzNlqkv7Qep/lU3W5uy8hf0T01uuqn3tqpcFapTi3qfNOoLLi0FBUEtpTnB5jmk9YY/xjkttbe7vpKd4bw3hjI8RHGL7DG2GNsTdjQB4ZLJaiZ1TM+d+1xJPaTf3U6bGf4ZU/m9//pi+Bih+xp+GVP5vf+9MXwjLNJ/4/wC6Pdb1oN/Kfvu9lnJtHU9VN1suttYI4k0HhnuFoSr/ANYjaLRbbWnz0tW6ZeEs0VSsy2JKbUkfMcTkoUfpJyP0POKuxoWEztqKGJ7eAB7RkfRY7j9I+jxSeN42uJHMOzHr5KWNmK/pLT7VSUmak8mXp88wuRefWcJb3ilSVHy3kJyewOY0JZfbmGkOtOJcbWApK0HII8QRGT3ePVkbrrdLl+BJViflGP8A6TE0tCfqBxEVi2BDEpROx+q61jlcGysGj+lZwWA00kWu29xY2Ivt2g5ZLTm4rtotpSapqs1WUpjAGd6ZdCM+4HmT5CM89f7wpl96s1utUd5UxTn+Clp1SCjf3GUIJweeMpPWGHNTkxPPF2ZfcmHD1W6sqUfiY+am1oCSpJSFjeSSOozjI+IMK4VgjMMeZS/WcRbgPmm+P6US43EIBEGMBvtub5jbkN/BcYmfZE/DhSv6PM/3SohiJn2Q/wAOFK/o8z/dKiTxT+Bm/tPooTAv5rTf3t9Vf2ID2m9rujbLtXtSXrlBnqtJ11Myov09xAcY4JaHzF4Cs8X8oY3e+YnyM4PS8/6/pZ/NVP75WMXpY2yyhjti+oZXFrCQrI2X6QbRC8koSbsNBmF9WKzKuMbvvXgt/wBqJkoOq9k3Qyl2kXfQ6mhQzmVqLLn3Kj8/UESzsNYfhcQmYqXbwt+bv1tsCwqe7OV+8aLTWW0lRDs62VnySgEqUfIAmMZtrPWKQ1213uK7KS04zSXS3LSfFTurcaaQEBah23iCrHYEA9Ih+CHNPSNpzrXuUnJMZBayDFzPRW0F2obQlWqQSSxT6E9vK7BTjrSUj6t76opnGuno3tApzSbSWZuStyqpSu3StuYDDicLZlEA8FKh2Kt5S8eCk9xBWyBkJHHJeQN1njkrdxhZtc0Z2g7TWpMq6kpUqtPzAB/JdPFSfiFgxupGW/pTdGZmgajUzUWTl1KpVbYRJzrqRybmmk4TveG+2E4/m1RFYe8NlLTvCd1LbsvwVF4s/wCjw1lpGkGvaRcE23IUiuyS6YuceVutsOlaFtKWeySUbuTyG+CcAGKwQRYJGCRhYd6j2uLSHBfosZeQ+2h1txLjSwFJWg5Ch2IIhtX1qjaWmdNXP3VcdOoUskZzOTCUKV5JT85R8gCYwapmo920SSEnTrorUhJgYEvK1B5tvH0UqAjxJ6oTVTmFPzky9Nvq5qdfcK1H3kxDjDM835dieGqyyClDaq1Bo2qm0FeV028+5M0aoTLapZ51stqWlLLaCrdPMAlBxnniIojm4y4zu8RtTe+kLTvDG8D0I8o4RMtaGtDRuTIm5uVbf0X3759P5lm/vbjXiMh/Rffvnk/mSb+9uNeYruIf63cFJU3wIhIWCIxOkCCD/wDukECEkLCGFgQjEJ2hYSBCWDMEECFE+0BoTK60UBgNPokK7I7xlJtYyhQPVteOe6cDmOYPPnzBptVdmTUukzi5c2vMzYSeT0otDjah4gg/fiNIYTGIh6vC4Kt/SOuDy3q94JpjiOCQfRow18Y2B18r8CDe3LZwss0js66k/wDg+o/1U/4xeHZ7nLmXpxJU+7aXNU2rU3/NQqZHN9oD2F5z1A9k/Rz3iTIMQUeGsonl8byb7skY7pbUY/TinqYWDVNwRrXHHadhG1UN1w2ebxmNVLim6Dbk3UKVNzBmmn2EjdysBSh17KKhDHb2c9SnFpQLQqAKjjKgkD6yqNKu0ENZMEgkeX6xFzfcpqm/aJiNNAyARMOqALnWubAC5z5KsWzlsrTVj1lm6LsUyqpsAmTpzSgtLCiMb61dCodgMgdc5xizsELiJimpo6WPo4hkqFiuLVeM1Jqqt13bABkAOAH6J2lEEJCw6UOiIb2h9B5rWhmjuSVWZp0zTA8EtvtFSHeJudVA5Tjc8D1iZIIc01TLSSiaE2cExraKDEIHU1QLsdtztsz2jms/q/slajUQqLNMl6s2Px5GZSc/oq3VfZDJntIb3pqymYtKsoI/JknFD6wDGm0KItkelVU0WkY13iPmqBNoDQPN4pXt8D6gFZgs6X3jMKCW7VrSj/QHR/0w6rf2ZtR7hcSlFuvSDZ6u1BaWQPMgne+oRoniFxHT9K6kizI2jxPyScWgFE03lmc4djR81XTSPY/pdoTjFWuiZbrtRaIW3KNpxKtKHc55uEeYA8jFigAOULBFVq6yetk6Sd1z5DsCv2H4bSYXF0NIzVG/ie07T+skGPJum1qZelBm6PV5VM3ITSN1xtXI+RB7EHmCI9aDnDRrnMcHNNiFIPY2RpY8XByIO8Kjeo+x1dNuTbr9tYuGmZJQgKSiZQPBSTgK96evgIieb0rvOSdLb1qVpCweY9QdP3JjTzrBiLjBpTVxtDZWh/PMHyyWcVWglBM8vgkdGDuyI7r5+ZWbVA0B1BuN5KJW1p9pKv4Scb9XQPivEWB0u2LZamvs1C9Z1FQWghQpkmSGc/7RZwVe4Ae8iLR4hYb1WklbUNLGWYDw2+J9k8oNC8No3iSW8pH9VreA295PYqw7TWzxWb1rdEqNn0thwNSvqkwwhxthLaUHLeAogdFEcvyREMf5Jmp38Qtf8cx/740FghOm0hrKWFsLQCBxBv6pWu0Ow6vqH1Ly5pdtAIA2AbLclU/Zq0HvTTvUpNWr1LblJD1N1niJmmnDvK3cDCVE9jFsISFiIrq6XEJumlABsBlyVhwrC4MIp/o1OSW3Jz2525DgvNuK3afddFm6TVZZE5ITSC26yvoR4+RBwQR0IimmpexxclAnHpm1VCvUwkqQwpaUTLY8CDgL94OT4Rd2ExC1BilThziYTkdoOz9dib4tgVFjLQKlvWGxwyI+Y5HyWYs5pTelPdLb9qVlCx1AkXFfaBHp0LQbUC4nUolbVqKAr+EmmuAgfFeI0ngxFjdpZUFtmxNB7SqY39n9IHXdO4jhZo8/yVVNMNituUfZn72nkTW6QoUuSUdw+TjnIkeSce+PW2mdnqp3q/bs3Z1KlyqTl1ST0shbbCUNA7ze7vEDkSscvERZXEEQn13WmpbUudci9huz5D/tWf8AdfDBROoWMs11rn7VwbjM/wDXJZ9/5Jmp38RNf8cx/wC+JL2dtAL3sDVKn1mt0puUp7TLyFupmmnCCpsgckqJ6mLdQkPJ9I6yoidC9rbOFth396jqXQzDqOoZURvfrMIIuRa47ksVj21NkKobUkvbT9LuKWok9QkzKUMzkupbUxxuF1Wk5RjhfkqzveXOzkEVmOR0Tg9u1XtzQ4WKxtvD0cet1quLMtQZO4mE9HaTPNqz+g4UK+yIqq2zXqvQ3FInNObnaUOXs0p5Y+tKSDG82IWJJuIyDaAU1NM3cVgZK6AamzywhjTy6VqPQfI8wPvREh2bsHa3Xm+2luypmjsqPOYrDqJVKR4lKjv/AFJMbYYhMR6cSkOxoQKZu8qk+zb6NO3tNKnKXFf08zdtcl1B1inMtkSDCxzCjve08R2yEp/knkYuwAAOQxB0hYjZJXzHWebpy1jWCzUQ3NQtPqDqlZ9Sti5ZBFRo8+3w3mV8iO4Uk9UqScEEcwRDighMEg3C7OeRWTWt/oy9QLIqUzN2KU3pQSoqaaC0NTzSfyVoUQlZHig8/wAkdIrrUNnvVClTBYmtO7obcBwQKQ+r7QkiN7wIWJRmIyNFnAFNHUzScjZYaWdsc6zXvMNtU/T6sS6FkDj1Jn1NtPmVOlP2RcrZ/wDRb0+hzktWdUqm1WnmyHE0GmqUJbPg86QFLH8lISOXUjlGgGIBHElfLILDLsXTadjczmqE7eOxXc+ql2WtW9NKBKPJl6cabOSjT7MohlDSsslIWpIxurUnA6BAirv/AGc2vX/hGX//AC0p/wDsjZiDpHMddLG0MFskOgY43WduwpshapaJ66C47voDVNpHyZMS3HRPsPHiLKN0bqFk9jzxGicEENppnTu13bUqxgjFgiCCCEEojlBB1ggQiEMGIWBCBBBB2gQiCIt1/wBd5TQu36Q/8kTNxVyuVBqlUijyi0oXNTK/mpKzySOnPB5kcucdvSG+r6vD5TavfTxVivS3DMuU1Vqebmgre3sKQBulO6Mgj8YQpqO1dfcudYXspHgxFctU9pu8rX11VplZWmzV61NNHRWFOrrSZLDZWUKGFtkcju/jZO905RLullyXVdVqpnrytRFm1kvLQaWifROgIGN1fESAOfPljliPXRua0OO/mvA4E2CeEEU3om3RfNYtKu3ozoyucsmiTb8tP1KSr7a3mg0Rvr4KmkqICSCe3XnyJiZNUtodFn7Op1Ztujfslp5lJafbk3XzLKLDqkAqKgleCkLyRjsecdOgkaQCNuXevBI0i6mSCIJ1m2n29MNHbPvamUNNwTN0zUjKyFMM1wd5Uw2XB7YQroBj5vXwj3NpHXN/Z+0yZuv5ERWnlz0tJKkzNFkJLpIKgvcVnGPDn5RyInmwttyXuuBfkpagjybjuin2la9RuCrPiUplPlVzky8ee42hJUo+fIRC2kG0TfOrtUpFRltJZyl6f1VSzLXDN1ZnjcIBRQ6qWxvBKiABgn52eY5x4GOcC4bAvS4A2U/fZCxEuquurmmurWltlpoyagi9pmbl1TpmeGZPgpaOQjcO/nidMpxjvmPS171upug1jCvT0k/VpuZmmqfTqXKkB2cmnCdxtJPToSTz5DoTygEbjaw27Eawz5KSITEQDYm0vX3tUqTYOpGn7lgVquSzk1R3kVNuel5vcGVtlaEp3Vgc8c/tGXBr1tA/tPT9rUGk25M3deFzvuMUqjy76WAvhgFxa3FAhCQFDt4+Bj3on6wbbavNcWupfgxDE0lvG77wpE47edkKsepy7/DRK/KLc6h9G6DxEuIA5ZJGCM8ofcJkapsV0DcXRiCIo2kNcl6DWPJViUov7IqtUakxS5GliY4BfdcJPz91WMJST0hx6Maly2sOlts3nKy4lG6xJpmFywc4nAcyUuN72Bvbq0qTnAzjoI61HBuvbJeawvq709IIrZqHtQXvSNdatpnZGmLV5z1OpzNSdfXXESR4awnPsrbI5FQHzvhE06Z3BctzWhLT922yi0a44tYdpSJ1M4G0hRCTxUgA5GD05Zjp0bmgOO/mvA4E2CdXWDtFL7Y29b2rWnDuoz+jDqrClnlNzdTkK8286yEqCVq4Km0qIBI8B5gc4tJO6oW/T9L16gPTZFtJpYrHrG77Rly2HEkJ/KKSMDxOI9fC+M2cOSGva7YnZ8IIqlJ7a1wU6m23dt26WTdtaaXBMtS8pcRqrb7rKXT+5OvS4SChChzzk8umeWZ31o1GVpLpVct4okBVFUeTVNCTL3CD2CBu7+FY69cGB0T2kAjagPaQSnsITtFV7U2zribnbFev7TB60bbvV1hik12Vqzc80XHkgtB1IQkoCgodeY58uRxLmvuuUhoTaUnUnabMV2r1SeaplKo8ooJdnJpz5qAT80cuZwe3IkiAwvDg22ZQHtIupNhMRA2nW0nXKjqtK6dah2I5YVy1GSXP0rcqTc/LzqEZK0BxCU4WkBRxz5JPTln0NpfaAqehMvZzdHtZu66nctWTSJaUcn/VAHFD2PbKFDmSBzwBnrB0T9YM3lGuLXU1QRA2lW0vVbn1Ue03vqxJmwruMganKNGeROy82yDhRQ4gDmOfLH4qumI8LaS2nr/0Afq9Ub0oRXLIkOAP2QqriGN9Tm4nHB4aljDi934Z6R6IXl2pv7QvNdoGsrLQfCIw0N1EvvUOnT83eunybDQkMqkUiqonjNoWFFSvZSNzGE9eu95Q19Jdpt3U7Q+8NQVW6inLt96faFPTOFwP+rNhed/cG7vZx8048456J2fK3mvdYKd4O8MLQnU9Ws+ktt3qunCkrq7CnjJJe4waw4pGN/dTn5ueg6xGOo2141YO0NRtNxbonabMPSMpUK765ueozE3xCy2Wtw72Q2DneHzvLmCJ7nFoGY9kF4ABKsXBHiXvcRs+y6/XhLiaNLp8xPBgr3OJwm1L3d7BxndxnBxmKq0bbtuuWsSi6gXTpC9StOqk8hr5dkK23NqYCnC2FrZ4aVAbwI547eIz6yJ8gu0Ic8N2q4sER7rjqwNINHbgvqXp6a0mmS7cwiUL/BDwU4hI9vdVjkvPQ9Icdm3Wi6bEolyvtJkEVCnM1BbRc3kshbYWRvYGQM9cDpCeqdXW3bF1cXsvfMEVMe23q/NW3Ur9o+lE/VNKJCaWw5cYqbTcw42he4t9uVKclAOee92OcYOLDVjVGgUbS9/UByaL1ttUz5XEw0nJcYLe+kpHiQRgeJhR0T2WuFyHtOxO3EEQHo9r/qDqnUaFPv6RTNDsett8eVrzlZZdcSyUFbbjkuEhQC8Jxgn5wPSJ8jh7Cw2K9Dg4XCISOExMNyjDj7y0ttNpK1rUcBKQMkn4RXzZf2tRtF1qv09+2jbapJhuekCqb4xnZRbrjfFxuJ3MKbHLn87r4gY5zS4DIILgCAd6sP8ACCIR2qdpZvZjtq3a2/QzW5Wp1RMg+lMzwVMNlClqcT7Kt4gJPs8s+IjtXjtEMW7q1pTaNPpjVXp9+NTL7VWRNbgYbbaDiVJRuHfCwod04846ETyA4DI38tq8L2g2UyQRD+1BrzNbPNgU24ZOgIuSZnqsxSm5Jc36sN51LhCt/cV3QBjHfryjwdPdpurz2qUhp5qLYUzp/c1UllzVLPr7c9KzoQCVpS6gDCgATjHbtkZBE8t1wMvkgvANlP0ERLtJa6uaAWjQq23RU1s1OuS1HLCpkscMOpcPEzuKzjc+bgZz1EPXUy8Dp7pxdV0plRPKolKmqmJUucMPFllTm5vYO7ndxnBxnoY51HWB4r3WGfJOWCKySm2tJVXZKqms1PoCJibpbrcvN0BU7u8J5Uy0yUl3cPLddSsHc5g45RPDd7SUtp6i7qoU06nopYqs0SreDLfC4i+eOeBnt2jp0T2bRvt3rwOB2JxYhYqY1tv1+Utqm37WNKJ+l6UVCaQw3cZqbbkw22te4h9yVCcpQTj8buME5GZ91a1bouj+mNXvmrFcxSqewh0JlsFb5WpKG0ozy9pS0jPTnmPXRPaQCNqA9pzT2giGdH9XdRr+rMsm5tJ3rPoE7KGalaoqsszR6JKEONJSFIUoEnyxziZsxw5pYbFdAgi4RB1ggjheoggEECEkBhYIEIggggQoq2idBJHX2z5SmrqkzQK3S5xFSpFZlObknMozuqxkZHiAQeQIIIhj7MesN61W97x0q1JEnOXhabbLwrFPG61UJdwDdWpOAEr5pJwBne6Ag5k3WDSqZ1UpMhLSd5XHZU1JPl9E7bs3wFuZSUlDnI76eecHuBHjaH7OVE0SnK7VWatV7muauqQqo12uzHGmXwj5qcgDCRn39OeAAHIe3oi1xvwy2d6SIOvcKvGpdEvOv+kImJaxblk7WrIshC1z07ICcQWRMYUjcJHMkpOf5J8Yt9YFNuKkWlISl11iXr9fbChM1GVlRLNvEqJThsE7uEkD4ZiJNVNk5Go2rB1Bp1/3JZdcNNRS1LoTiG95lKirBURnmSMjp7IiTdLLEntOrUTR6hdVXvGYDy3flOtuhyYIVjCMjsMcvfHsr2vY2x2Abs/FeMaQ43WZtr3hqBbmzNc9PllUml6aV675qjVmthpyYn5JD5SlxXDylHDKcJzkn2sciQY0Xd0cpJ2e3dNJBwv0k2+qkMPOYJWkslCXDjlknCuXeGdSNj62qZoXdml7lUn5yk3DOuz7s26lHGYdWpCwUYGPZU2kjIiXrEtc2TZlEt8z79UFLk2pNM5NAB11KEhKVKxyzgDMKTzNfmzjf0zXMbC34uCz/wBH6o/rFObLVjzgUpVrOVKo1VlXPcMk4W5cKHvQE/pROvpJP3ubP5/kP1lQ/tKtle3dJtW7pv6nVCdmp6uJfQmTmAjgyaHn+O4lrAzgrA6mHJr3olTtfrFTa9VqE1TZVM6zO8eTCSvebJIHtAjBzHTpmGZrhsHvclAY7UIO0pn7ajc07sl6gCUKuIKYgq3evDDjZX/Z3ofegj8rMaH6fuSSkmVVQJHh7nTHARDrrNBkbhoM5RqnLInabOS65WYl3B7LjaklKkn3gmIR0q2SGNIbipsxR9Rr1etumuuOSlrzNRCpBG8FDcUkJG8kFRIHiAesNw5pj1CbEG/klCCHXCaO1J++u2YPzjVf7uWg24Fer3loBMTJ3aW3e8uHyr5oWd3cz8Av7YmPUXRCm6jak6e3lOVCblZyzH5l+Vl2Aktvl5LYUF5GcDhjGMdTHo6zaO2/rnY8xbFxofEotxEwzMyi+G/LPI+Y62rBwoZPY5BI7wo2VoMd9wN++/zXJYSHc/yUF7WauNtG7NLEsc1D5fmXMJ+cGAGeL8MRI20fs9K1slaDVaNXn7Vve2n1zVFrTKd4NLUBvIWnuhW6nPu6EZB6mm+yrTrL1Blr3r13XFf1ySMqZKnTVwzCXBJNHIVw0pSPaIJBUfE9zmHLrJozOarOUl+Qv26bGmqcHUhduTvBTMJc3Mh1JBC8bg3fDeV4x5rgFga7Zvt7I1SQSRtTR2UtbLm1MYvG1r4lJVi9bKqCabUpiQ/1ebCt7cdSOxPDXkDl0OBnAnuIz0M0CoGg1GqcrSZqoVao1aaM7U6xVn+NNzjx/GWrA6ZOOXck5JJiTIRlLS8lmxdsBDesqZbV+rlqUDaq0eo931hqlW7b7UxcE6462t1JeUlTcsClCVHO82o9Pxo9X0dd8UqrWXfVpUefTUKXblxzPyY+kKSHJB9alsqAUARkpcPMDrEz0fQKj07XG5dTpqcmKnVazIs09MnNNoUxKNICOTfLPMoBOT1J8Y5WjoNSrJ1nu7UOmz8y0/cssxLzlKCECWSppKUpcTgZCsJP9Yw4dJGYujHAeN7+6TDXa+sq03PQb5uD0gV6sWFdMlalURa0qt6bnqeJxDjWWgUBBIwclJz5RciyZCu0u1adK3LVGa1XWmt2bqEtLiXbfXk+0lsZ3RjAxEM6j7I6b41aqGoNK1Duey61PSbUi98hOIbCmkAYGSMkEpBxnqIlnTOypzT+0Zaiz1y1W7Zlla1KqtZcDky5vKJAUR2GcDyEcSva9rbHYBuz8V6xpBN1l3p9c990/ZEoluvv0mhaVXNcTlHqVwIaXMT0oHHAVFSCpKA2d0jIycA9MiLtbXtpN2bsPXTblD4glaRSJKTZ3jlZYZeYSckdTuJOfjHfpuxha8hs4VDR1yrVGao83MmbFRcS36w05xUuApwN3kU46dCYmBixJKY06as+tLXX6eqmClzbk4BvzbfD4alLx+MoZJIxzPLELyzsc8Obudft2Z9u5cMjIBB4KrW1jOU5z0d0gtlTZZfpVETJ46E7zBAT+iFfbEnbT7cwzsZXgib3vW024lL2913wlG9n45jwaLsJ27Iv0GSql6XZcdn0GbE5TbVqc4hck0tJJQFAIBWlOTgHsSOhIM3aq6eyuq+nVetCemnpKUrEsqVdmJcAuNgkHKc8s8u8JmRgLQDcA39F0GuN78LKmlrW1qBtJ2BoXa4seataybZVTKrO3DVJlrM6lhkJSJdpJKsLBOCfygTjHOStsRXA1u2an5g7tOTda0OFXzQ8oscLPnkKxFj7CtFiwLIoFsyr7kzLUeRYkGnngAtxLaAgKVjlkgdob2teilv67Wb+x64PWWENTCJyUnpFzhzMnMIzuutqwcEZI5jofqOnBkBIs0X875o6M6vPLyUIbSKuNtgbN7EmR8oJmak44E/ODHCTvZ8sJc+2Oj6QFdSRUtDVUduXdqwvSWMm3OKUllT3LcCynmE72Mkc8RKGmGy5TrE1AN81u6rgvy625QyMpULgfS56oyfnJaSlIAJyQT5nxOXHrBofTdYqnZM7UKhNSK7VrLVal0ywSQ842QQhe8D7Jx25wNkY17M7hoPv80Fri08/yVfdnFuu6m7V183LqTNSkpftmSiaJK0GmMlMq1KukqEwhxSipwKyrrjHE59QA8vSN/vR7u/n5D/m2okljQamyW0C/qvJ1OclapN0sUqdp6Ep9XmUAgpWrlvbw3Ud/wAQeJj0Nd9Hafr1plU7Kqk9M02Sn1srXMygSXElt1LgxvAjmUAfGPOlb0zH7hbuRqHULd+adVp//CtG/oTP6gil2ykQNibV/Jxibr2c9v8ANxF3abIpplNlZNCitEu0hpKldSEgAE/VFb67sKUCen7ibo183ha1tXDMuTVUtykzyUSb61/6TCSk7oV0I58uXQARzE9oDg42uQfAr1wORCcew6dzZO06KjgCRcOT/PuRRW99Y7Hu+zdeq5OXKzKX5VrrlZ6gyimXVOKYkVgS5SsIKRlC3RzUOkac03T2m25py3ZlBLlGpkvTjTpVbB3ly6dwpCwT1UM5yep6wztO9m619O9Fv2t2EqqFNXLTMs7OzTSPWHeOVlaiQMZG+QPcIVjmY1znnefK91y5jiA0bguVx3gxqBst1m5ZZQLNWtCYnU47b8mpRHwJI+EUJtmYvO7tDtDtM7smKZb+k13TvqyKtT21vTjq25hS0MPFRCW99eMFIPQE9CIv/YWhknYmhR0wbrE9UKYJCapyZ6YCOOlp7f8AAbvshwgcugEM6tbHlu1rQO2dLl1qpMStuzaJ2Rq7QbE0h1K1qCum7/CKHSCKWOO455eBz9EPY51jyXLbelm5LZEv+XZTuNMyDDaEjskPtAD6hHtU5qZe2P2G5Pe9bXYwSzu9d8yHs4884h2ataXSur+ltYsmqT8xLS1UYQw9OS6U8Ubq0r3gCCMko+2PetO2WLTtGkW+04qZlqbJMyKFvAbziG0BAKgOWSBzhDXAjDd4N/RKap1ieSqdpFO00ejGfcUtv1VFsVVtzPQOb8wCD57/AN8Sds4WbLXxsZ2ZbVxtLfkKrbiZV9vO6osOJITg9juFJB8hDcntgm2XmqjRZO87tpdhVCc9embPlJ1KZFSyoKKR7O8EEgcs9hzyAYnyuWPKVSwpm05GZm7ekFyXqDD9Hd4D8mgJ3UllWDuqSAMHtiFZJGH4TtN+xcNaRtG6yq1p/VL62Q9VrI0rr9YbvLTe5nXJG3p91G5PU1aQN1lePnIG8kd+RyN0Ddi4/wAYgGxdkCm27qDSbyuW97r1BrFGCxSv2RzoeakyoYK0pA5q8yfA4yBif4Smc1xBGZ3nZcrtgIFioU2zNQP2t9mq+aoh3hzUxJGnS5BweI+Q1keYC1K/RiqGjGqmn9t7QmhErZlxMVVMxaYtKtpZZdaCJhIDjajvoSDvPEjln7YudrroZTteqPQqTWKlNyVNplVZqjkvKhJE0W84bXvA+yd49OcdfWTZ8omr7Nslcy9QJ236szWJOcpzTYcDjecJOR80nBPuEKxSRtZqO33v4WC4e1xdcclD/pA5GXqkvovIzbSZiUmr7kWH2V/NcbWFJUk+RBI+MQLYTlVtbat0a0vrrrj8/YNWq1PlJh3rMU52X4so5/U3k47BIHaLyazaH03WldnqqNQm5D9jVbYrjHqoSeK41nCFbwPsnPPHOOjeGznb13642hqk4/MSdwW604yEMBPDm0KStKQ5kZ9niLwQe8dxztbGGHgfE7Fy6Ml2sOSin0jv4HrP/wDOVO/Ufg2qVB/ae2a5aTINTTWJx1YT87gBLRWfdgL+2Je2hdB6ftC2VJW5UatPUVuUqLVTamqeElwONpWlI9oEY9sn4CPD0v2WaLYF9G96vcdwX3eCZcyjFVuOaDypVo9UspCQEZyRnn1OMZOeGSMawXOY1su1dOa4uPO3ko99I1+CayP/ADtTf1H4mfaT/e66pc//AJWqn/KOxx130PpuvNtUii1SozdNYptXYq7bkmElS3GgsBB3gfZO+c9+UOu/rRYv+xbiteafdlpWtU6Yprr7IG+2h5pTZUnPLICiRmEw9uqwcCfULrVN3Hj+aykvSUndLdmthCN4Wxqha0g7gfMZqsjMMqJ8uIw2fer3RfTXduZd2HLhTKb3FFoNqO713Awgr/shUejdWyRa947O9I0kqE9OrptJQyJOqBKPWWltqJCxy3clKlpPLooxLcrasi3Z7NtzLYn6amRTTnG30gh5oN8MhQ6c09ffC8tQ1+qRtBv8vJJtjIuOSqbqzO01XoxGHApsyq7XpbbfgXeJLgAee/8AdE2saWSGr2zBQ7JuoPcCo29IMzK2zh1t1LTagsEj5yXEhXPw5xH0nsE2yy1T6LN3ndtSsKQnfXpezpqdSqRSoKKgg+zvFAJPLPc88kmJ31Gsb9n9kz1utVqq2yZnhcOpUKY9Xm5ctuJWOGsDlncCT4pJHeE3vbkGHeTfgumtOZI3WVc9Fb11A0P1souht+1Ri7qVU6c7M23cLaNyZDbKVEsvp74Q2rnzOQOageVs/jEHaV7KdK091A/ZzWLruO/bralVSUrULjmw96o0rO8G0gDBIJHX8Y8uZicYSmc1zrt792a7YCBYpYIIIQSiIIWCBCTEJCwQIRBBB8YEIg7QZggQiDEEGYEIhIWEgQlgg+MECEd4IIT4wISwGCD4wIRCQsHWBCIMQQkCEfXBCwQISQsEJmBCIWCCBCISDML3gQiCD4wAwIRBBBAhIIWCDMCEkLBBAhEEEECEd4SCFgQiCDt1ggQiCCCBCQiFhIXMCEkLBBAhEJCwQIRB2gMECEQQZ5wfGBCIIIIEIhBCwQIRCQuYIEIxBBBAhAggxBAhf//Z';
					doc.addImage(pic, 'JPG', cord_y, cord_X, 40, 12);
					/* BRANDING */
					doc.setFont('arial', 'bold');
					doc.setFontSize(10);
					//doc.text(cord_X, cord_y + 25, 'SERVICIO AEROPORTUARIOS ANDINOS');
					doc.addImage(value.qr, 'JPG', 16, 32, 40, 35);
					doc.addImage(value.bar, "JPEG", 56, 29, 100, 50, 'monkey');
				};
				var setSubCabecera = function (value) {
					var coordX = 24;
					var coordY = 90;
					doc.rect(21, 85, 60, 12);
					doc.setFontSize(6);
					doc.setFont('helvetica');
					doc.setFontType('bold');
					// Primera Columna.
					doc.setFont('arial', 'bold');
					doc.setFontSize(15);
					doc.text(coordX, coordY, 'AWB    : ' + value.DocumentoTransporteMaster);
					doc.text(coordX, coordY + 6, 'HAWB : ' + value.DocumentoTransporte);
					doc.setFontSize(10);
					doc.setFont('helvetica');
					doc.setFontType('bold');
					doc.text(coordX, coordY + 12, 'TIPO DE ALM. :' + value.TipoAlmacenamientoDescripcion);
					doc.text(coordX, coordY + 16, 'LINEA AEREA :' + value.Transportista);
					doc.text(coordX, coordY + 20, 'ALMACEN :' + value.Almacen);
					// Segunda Columna.	
					doc.setFontSize(10);
					doc.setFont('helvetica');
					doc.setFontType('bold');
					doc.text(coordX + 85, coordY, 'BULTOS : ' + value.Bultos);
					doc.text(coordX + 85, coordY + 4, 'Peso (Kg) : ' + value.Peso);
					doc.text(coordX + 85, coordY + 8, 'Fec. Ingreso :' + value.FechaIngreso); //utilFormatter.FechaHoraToString(new Date(value.FechaIngreso))
					doc.setFontType('bold');
				};

				var setFooter = function (value, cord_x) {
					doc.setFontSize(6);
					var vSpace = 2.5;
					/*doc.text(10, cord_x + vSpace * 1, 'Fecha de documento: ');
					doc.text(40, cord_x + vSpace * 1, value.sfecha);
					doc.text(60, cord_x + vSpace * 3, 'Recibido VoBo');
					doc.text(80, cord_x + vSpace * 3, '-----------------------------------------------------');
					doc.text(10, cord_x + vSpace * 3, 'Doc Identidad: ');
					doc.text(40, cord_x + vSpace * 3, value.sDNI);*/
				};

				var setDetalle = function (Detalle, nroExceso) {
					setSubCabecera(value);
					setBranding(value);
					// Impresion del excedente
					for (var z = 0; z < nroExceso; z++) {
						/*doc.text(36, 68 + 7 * z, Detalle[z].Manifestado);
						doc.text(60, 68 + 7 * z, Detalle[z].Recibidos);
						doc.text(76, 68 + 7 * z, Detalle[z].Faltantes);
						doc.text(92, 68 + 7 * z, Detalle[z].Sobrantes);
						doc.text(108, 68 + 7 * z, Detalle[z].Buenos);
						doc.text(124, 68 + 7 * z, Detalle[z].Malos);*/
					}
					//setFooter(value, 94);
				};
				// DETALLE
				var xDetalle = value.aDetalle;
				if (!(Array.isArray(xDetalle))) {
					var Detalle = [];
					Detalle.push(xDetalle);
				} else {
					Detalle = xDetalle;
				}

				var nroExceso = Detalle.length;
				setDetalle(Detalle, nroExceso);

				//terminosCondiciones(value);
				doc.save("UA___" + value.CodigoUA + '.pdf');
			};
			$.generarOrdenCompraPDF(value);
		},
		///End Descargar PDF///
		crearQr: function (codigo, callback) {
			//creamos codigo QR
			var div = document.createElement("canvas");
			div.setAttribute("id", "Div1");
			div.style.height = "100px";
			div.style.background = "black";
			div.style.color = "white";
			var qr = new QRCode(div, codigo, {
				width: 177,
				height: 177,
				colorDark: "#990000",
				colorLight: "#ffffff",
				correctLevel: QRCode.CorrectLevel.H
			});
			var imgQR = qr._el.childNodes[1];
			imgQR.onload = function (oEvent) {
				var imagen = qr._el.childNodes[1].src;
				var base64 = imagen.split(',')[1];
				var formato = "data:image/jpeg;base64," //imagen.split(',')[0];
				var urlQR = formato + base64;
				callback(urlQR);
			};
		},
		crearCodigoBarras: function (codigo, callback) {
			//Creamos codigo de barras
			var div2 = document.createElement("canvas");
			div2.setAttribute("id", "Div2");
			div2.style.height = "100px";
			div2.style.background = "black";
			div2.style.color = "white";
			JsBarcode(div2, codigo);
			console.log(div2.toDataURL("image/jpeg"));

			var elem = document.createElement("img");
			elem.setAttribute("src", "images/hydrangeas.jpg");
			var base64 = div2.toDataURL("image/jpeg").split(',')[1];
			elem.src = "data:image/jpeg;base64," + base64;
			var urlCodigoBarras = elem.src;
			callback(urlCodigoBarras);
		},
		pad: function (n, length) { //Añadir ceros a la izquierda, length tamaño final que debe tener
			var nMod = n.toString();
			while (nMod.length < length) {
				nMod = "0" + nMod;
			}
			return nMod;
		}
	};
});