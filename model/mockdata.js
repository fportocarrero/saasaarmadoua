sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device",
	"jquery.sap.global",
], function (JSONModel, Device, jQuery) {
	"use strict";

	return {
		createDeviceModel: function () {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},
		modelListUAS:function(){
		var modData = [
			{
				TipoCarga:"TC1",
				TipoAlmacenamiento:"TA01",
				Almacen:"ALMC01"
			},
			{
				TipoCarga:"TC2",
				TipoAlmacenamiento:"TA03",
				Almacen:"ALMC03"
			}
			];	
			return modData;
		},
		modelListMaster:function(){
			var modData = [
							{
							nVuelo:"1112002",
							placa:"",
							tipoOrigen:"",
							linea:"",
							tipoVuelo:"",
							tipoCarga:"",
							titulo:"LATAM",
							manifiesto:"123456",
							tiempoLlegada:"Hora Llegada",
							fechaLlegada:"3:40 pm. 25/04/2019",
							tiempoSalida:"Hora Salida",
							horaSalida:"4:50 pm.",
							origen:"Perú",
							destino:"EE.UU.",
							Contenedor:this.modelListContenedor(),
							GABL:this.modelListGABL(),
							Paquetes:this.modelListPaquetes(),
							Precintos:this.modelListPrecintos(),
							Adjuntos:this.modelListAdjuntos()
							},
							{
							nVuelo:"1100023",
							placa:"",
							tipoOrigen:"",
							linea:"",
							tipoVuelo:"",
							tipoCarga:"",
							titulo:"VIVA AIR",
							manifiesto:"123456",
							tiempoLlegada:"Hora Llegada",
							fechaLlegada:"3:40 pm. 25/04/2019",
							tiempoSalida:"Hora Salida",
							horaSalida:"5:50 pm.",
							origen:"Perú",
							destino:"EE.UU.",
							Contenedor:this.modelListContenedor(),
							GABL:this.modelListGABL(),
							Paquetes:this.modelListPaquetes(),
							Precintos:this.modelListPrecintos(),
							Adjuntos:this.modelListAdjuntos()
							},
							{
							nVuelo:"1100333",
							placa:"",
							tipoOrigen:"",
							linea:"",
							tipoVuelo:"",
							tipoCarga:"",
							titulo:"APM INTERNACIONAL",
							manifiesto:"123456",
							tiempoLlegada:"Hora Llegada",
							fechaLlegada:"3:40 pm. 25/04/2019",
							tiempoSalida:"Hora Salida",
							horaSalida:"6:50 pm.",
							origen:"Perú",
							destino:"EE.UU.",
							Contenedor:this.modelListContenedor(),
							GABL:this.modelListGABL(),
							Paquetes:this.modelListPaquetes(),
							Precintos:this.modelListPrecintos(),
							Adjuntos:this.modelListAdjuntos()
							},
							{
							nVuelo:"1100334",
							placa:"",
							tipoOrigen:"",
							linea:"",
							tipoVuelo:"",
							tipoCarga:"",
							titulo:"VIVA AIR",
							tiempoLlegada:"Hora Llegada",
							fechaLlegada:"3:40 pm. 25/04/2019",
							horaLlegada:"7:00 pm.",
							tiempoSalida:"Hora Salida",
							horaSalida:"7:50 pm.",
							origen:"Perú",
							destino:"EE.UU.",
							Contenedor:this.modelListContenedor(),
							GABL:this.modelListGABL(),
							Paquetes:this.modelListPaquetes(),
							Precintos:this.modelListPrecintos(),
							Adjuntos:this.modelListAdjuntos()
							}
							];
			return modData;
		},
		modelListGuias:function(){
			var modData = [
								{
									guiaAerea:"000007DMQ317",
									guiadirecta:"04505601702",
									bultos:"",
									tipoCarga:"",
									importador:"",
									codigoUN:"",
									direccionamiento:"",
									pesoDeclarado:""
								},
								{
									guiaAerea:"000007DMQ316",
									guiadirecta:"04503601701",
									bultos:"",
									tipoCarga:"",
									importador:"",
									codigoUN:"",
									direccionamiento:"",
									pesoDeclarado:""
								},
								{
									guiaAerea:"000007DMQ319",
									guiadirecta:"04503601702",
									bultos:"",
									tipoCarga:"",
									importador:"",
									codigoUN:"",
									direccionamiento:"",
									pesoDeclarado:""
								}
								];
			return modData;
		},
		modelListContenedor:function(){
		var modData = [
								{
									nContenedor:"123",
									tipo:"GA1",
									tamano:"1",
									clasifCont:"2",
									condicion:"3",
									bultosManif:"26",
									pesoManif:"202.33",
									enabledInput:false,
									icon:"sap-icon://edit"
								},
								{
									nContenedor:"1234",
									tipo:"GA1",
									tamano:"1",
									clasifCont:"2",
									condicion:"3",
									bultosManif:"26",
									pesoManif:"202.33",
									enabledInput:false,
									icon:"sap-icon://edit"
								},
								{
									nContenedor:"12345",
									tipo:"GA1",
									tamano:"1",
									clasifCont:"2",
									condicion:"3",
									bultosManif:"26",
									pesoManif:"202.33",
									enabledInput:false,
									icon:"sap-icon://edit"
								}
								];
			return modData;
		},
		modelListGABL:function(){
			var modData = [
								{
									masterDirecta:"DD",
									GABL:"DD",
									descripcion:"DD",
									bultosManifestado:"DD",
									pesoManifestado:"DD",
									origen:"DD",
									destino:"DD",
									pLlegada:"DD",
									termOrigen:"DD",
									termDestino:"DD",
									modalidad:"DD",
									clasificacionCarga:"DD",
									regimen:"DD",
									agenteCarga:"DD",
									embarcador:"DD",
									consignatario:"DD",
									enabledInput:false,
									icon:"sap-icon://edit"
								},
								{
									masterDirecta:"DD",
									GABL:"DD",
									descripcion:"DD",
									bultosManifestado:"DD",
									pesoManifestado:"DD",
									origen:"DD",
									destino:"DD",
									pLlegada:"DD",
									termOrigen:"DD",
									termDestino:"DD",
									modalidad:"DD",
									clasificacionCarga:"DD",
									regimen:"DD",
									agenteCarga:"DD",
									embarcador:"DD",
									consignatario:"DD",
									enabledInput:false,
									icon:"sap-icon://edit"
								},
								{
									masterDirecta:"DD",
									GABL:"DD",
									descripcion:"DD",
									bultosManifestado:"DD",
									pesoManifestado:"DD",
									origen:"DD",
									destino:"DD",
									pLlegada:"DD",
									termOrigen:"DD",
									termDestino:"DD",
									modalidad:"DD",
									clasificacionCarga:"DD",
									regimen:"DD",
									agenteCarga:"DD",
									embarcador:"DD",
									consignatario:"DD",
									enabledInput:false,
									icon:"sap-icon://edit"
								}
								];
			return modData;
			
		},
		modelListPaquetes:function(){
			var modData = [
								{
									guia:"1",
									tipo:"1",
									cantidad:"1",
									enabledInput:false,
									icon:"sap-icon://edit"
								},
								{
									guia:"1",
									tipo:"1",
									cantidad:"1",
									enabledInput:false,
									icon:"sap-icon://edit"
								},
								{
									guia:"1",
									tipo:"1",
									cantidad:"1",
									enabledInput:false,
									icon:"sap-icon://edit"
								}
								];
			return modData;
		},
		modelListPrecintos:function(){
			var modData = [
								{
									contenedor:"1",
									nPrecinto:"1",
									enabledInput:false,
									icon:"sap-icon://edit"
								},
								{
									contenedor:"1",
									nPrecinto:"1",
									enabledInput:false,
									icon:"sap-icon://edit"
								},
								{
									contenedor:"1",
									nPrecinto:"1",
									enabledInput:false,
									icon:"sap-icon://edit"
								}
								];
			return modData;
		},
		modelListAdjuntos:function(){
			var modData = [
								{
									tipoDocumento:"1",
									adjunto:"1",
									enabledInput:false,
									icon:"sap-icon://edit"
								},
								{
									tipoDocumento:"1",
									adjunto:"1",
									enabledInput:false,
									icon:"sap-icon://edit"
								},
								{
									tipoDocumento:"1",
									adjunto:"1",
									enabledInput:false,
									icon:"sap-icon://edit"
								}
								];
			return modData;
		},
		//////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////
		
	};
});